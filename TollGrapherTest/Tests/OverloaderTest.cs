﻿using System.Linq;
using NUnit.Framework;
using Tolltech.Models;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapherTest.Common;
using Tolltech.TollGrapherTestCases.Tests;
using Tolltech.TollGrapherTestCases.Tests.OVerloads;

namespace Tolltech.TollGrapherTest.Tests
{
    public class OverloaderTest : TollGrapherTestCasesTestBase
    {
        [Test]
        [TestCase("Call11", 1)]
        [TestCase("Call12", 1)]
        [TestCase("Call21", 2)]
        [TestCase("Call22", 2)]
        [TestCase("Call31", 3)]
        [TestCase("Call32", 3)]
        [TestCase("Call33", 3)]
        public void TestCalls1(string methodCase, int dummyCalls)
        {
            var method = ExtractMethod(typeof(OverloadCaller).FullName, methodCase);
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(OverloadCaller).FullName}.{methodCase}",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(Overloader).FullName}.Call",
                            NetworkCall = false
                        },
                        Children = Enumerable.Range(0, dummyCalls).Select(x =>
                            new TreeNode<MethodNode>
                            {
                                Node = new MethodNode
                                {
                                    Name = $"{typeof(Dummy).FullName}.DummyCall",
                                    NetworkCall = false
                                },
                                Children = new TreeNode<MethodNode>[0]
                            }).ToArray()
                    },
                },
            });
        }
    }
}