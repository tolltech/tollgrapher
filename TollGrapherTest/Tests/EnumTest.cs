﻿using System;
using NUnit.Framework;
using Tolltech.Models;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapherTest.Common;
using Tolltech.TollGrapherTestCases.Tests;

namespace Tolltech.TollGrapherTest.Tests
{
    public class EnumTest : TollGrapherTestCasesTestBase
    {
        [Test]
        [TestCase(typeof(EnumCases), "Enum")]
        public void TestGetProperty(Type classType, string methodName)
        {
            var method = ExtractMethod(classType.FullName, methodName);
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{classType.FullName}.{methodName}",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new TreeNode<MethodNode>[0]
            });
        }
    }
}