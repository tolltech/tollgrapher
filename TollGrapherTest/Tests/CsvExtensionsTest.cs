﻿using NUnit.Framework;
using Tolltech.Models;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapherTest.Common;

namespace Tolltech.TollGrapherTest.Tests
{
    [Ignore("CSV deprecated")]
    public class CsvExtensionsTest : TestBase
    {
        [Test]
        public void TestGetCsvHeader()
        {
            var header = CsvExtensions.GetCsvHeader<MethodNode>();
            Assert.AreEqual("ClassName,EntryPoint,Error,Id,IsProperty,Name,Namespace,NetworkCall,ServiceName,ShortName", header);
        }

        [Test]
        public void TestGetCsvRow()
        {
            var methodNode = new MethodNode
            {
                EntryPoint = true,
                Name = "name",
                Error = false,
                NetworkCall = true,
                ServiceName = "serviceName",
                ClassName = "class",
                ShortName = "short",
                IsProperty = true,
                Namespace = "namespace"
            };
            var row = methodNode.GetCsvRow(";");

            Assert.AreEqual($"class;1;0;{methodNode.Id};1;name;namespace;1;serviceName;short", row);
        }
    }
}