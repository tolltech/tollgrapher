﻿using NUnit.Framework;
using Tolltech.Models;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapherTest.Common;
using Tolltech.TollGrapherTestCases.Tests;
using Tolltech.TollGrapherTestCases.Tests.Composites;

namespace Tolltech.TollGrapherTest.Tests
{
    public class CompositeTest : TollGrapherTestCasesTestBase
    {
        [Test]
        public void TestCalls()
        {
            var method = ExtractMethod(typeof(Composite).FullName, "Call");
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(Composite).FullName}.Call",
                    NetworkCall = true,
                    EntryPoint = true,
                    ShortName = "Call",
                    ClassName = "Composite"
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(Concrete1).FullName}.Call",
                            NetworkCall = false,
                            ShortName = "Call",
                            ClassName = "Concrete1"
                        },
                        Children = new[]
                        {
                            new TreeNode<MethodNode>
                            {
                                Node = new MethodNode
                                {
                                    Name = $"{typeof(Dummy).FullName}.DummyCall",
                                    NetworkCall = false,
                                    ShortName = "DummyCall",
                                    ClassName = "Dummy"
                                },
                                Children = new TreeNode<MethodNode>[0]
                            }
                        }
                    },
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(Concrete2).FullName}.Call",
                            NetworkCall = false,
                            ShortName = "Call",
                            ClassName = "Concrete2"
                        },
                        Children = new[]
                        {
                            new TreeNode<MethodNode>
                            {
                                Node = new MethodNode
                                {
                                    Name = $"{typeof(Dummy).FullName}.DummyCall",
                                    NetworkCall = false,
                                    ShortName = "DummyCall",
                                    ClassName = "Dummy"
                                },
                                Children = new TreeNode<MethodNode>[0]
                            }
                        }
                    },
                }
            });
        }

        [Test]
        public void TestCallsLambda()
        {
            var method = ExtractMethod(typeof(Composite).FullName, "CallLambda");
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(Composite).FullName}.CallLambda",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(Concrete1).FullName}.Call",
                            NetworkCall = false
                        },
                        Children = new[]
                        {
                            new TreeNode<MethodNode>
                            {
                                Node = new MethodNode
                                {
                                    Name = $"{typeof(Dummy).FullName}.DummyCall",
                                    NetworkCall = false
                                },
                                Children = new TreeNode<MethodNode>[0]
                            }
                        }
                    },
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(Concrete2).FullName}.Call",
                            NetworkCall = false
                        },
                        Children = new[]
                        {
                            new TreeNode<MethodNode>
                            {
                                Node = new MethodNode
                                {
                                    Name = $"{typeof(Dummy).FullName}.DummyCall",
                                    NetworkCall = false
                                },
                                Children = new TreeNode<MethodNode>[0]
                            }
                        }
                    },
                }
            });
        }
    }
}