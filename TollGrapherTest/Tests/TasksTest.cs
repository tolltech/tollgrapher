﻿using System;
using FluentAssertions;
using NUnit.Framework;
using Tolltech.Models;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapherTest.Common;
using Tolltech.TollGrapherTestCases.Infra.Tasks;
using Tolltech.TollGrapherTestCases.Tests;
using Tolltech.TollGrapherTestCases.Tests.Tasks;

namespace Tolltech.TollGrapherTest.Tests
{
    public class TasksTest : TollGrapherTestCasesTestBase
    {
        [Test]
        [TestCase("CallEchelonTask", typeof(EchelonTasktRunner), "DummyCall")]
        [TestCase("CallEchelonTaskExplicitArray", typeof(EchelonTasktRunner), "DummyCall")]
        [TestCase("CallEchelonTaskExplicitArrayFromLinq", typeof(EchelonTasktRunner), "DummyCall")]
        [TestCase("CallEchelonTaskAsyncParams", typeof(EchelonTasktRunner), "DummyCall")]
        [TestCase("CallSqlTask", typeof(SqlTaskRunner), "DummyCall2")]
        public void TestGetBaseMethods(string methodName, Type taskRunnerType, string dummyMethodName)
        {
            var classType = typeof(TaskCases);
            var method = ExtractMethod(classType.FullName, methodName);
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{classType.FullName}.{methodName}",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{taskRunnerType.FullName}.UnsafeRun",
                            NetworkCall = true,
                            EntryPoint = false,
                        },
                        Children = new[]
                        {
                            new TreeNode<MethodNode>
                            {
                                Node = new MethodNode
                                {
                                    Name = $"{typeof(Dummy).FullName}.{dummyMethodName}",
                                    NetworkCall = false,
                                    EntryPoint = false,
                                },
                                Children = new TreeNode<MethodNode>[0]
                            },
                        }
                    },
                }
            });

            walking.Node.Async.Should().BeTrue();

            foreach (var node in walking.Children)
            {
                node.Node.Async.Should().BeTrue();
            }
        }
    }
}