﻿using NUnit.Framework;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapherTest.Common;

namespace Tolltech.TollGrapherTest.Tests
{
    public class ExtensionsTest : TestBase
    {
        [Test]
        [TestCase("Asd.dsa.fff", "fff")]
        [TestCase("fff", "fff")]
        [TestCase("Asd.dsa.fff", "dsa.fff", 1)]
        [TestCase("Asd.dsa.fff", "Asd.dsa.fff", 4)]
        public void TestGetPostfix(string source, string expected, int skipCount = -2, string separator = ".")
        {
            if (skipCount == -2)
            {
                Assert.AreEqual(expected, source.GetPostfix(separator));
            }
            else
            {
                Assert.AreEqual(expected, source.GetPostfix(skipCount, separator));
            }
        }
    }
}