﻿using Microsoft.CodeAnalysis;
using NUnit.Framework;
using Tolltech.TollGrapher.AnalyserHelpers;
using Tolltech.TollGrapherTest.Common;
using Tolltech.TollGrapherTestCases.Tests;
using Tolltech.TollGrapherTestCases.Tests.OVerloads;

namespace Tolltech.TollGrapherTest.Tests
{
    public class OverloadParametersTest : TollGrapherTestCasesTestBase
    {
        //public void CallInt(int i)
        //public void CallInts(int[] i)
        //public void CallNullableInts(int?[] i)
        //public void CallNullableInt(int? i)
        //public void CallLong(long i)

        private ITypeSymbol intTypeSymbol;
        private ITypeSymbol longTypeSymbol;

        // ReSharper disable once NotAccessedField.Local
        private ITypeSymbol iBaseTypeSymbol;
        private INamedTypeSymbol baseTypeSymbol;
        private ITypeSymbol childTypeSymbol;

        protected override void SetUp()
        {
            base.SetUp();

            intTypeSymbol = compiledSolution.CompiledProjects.CompiledProjects[0].Compilation.GetSpecialType(SpecialType.System_Int32);
            longTypeSymbol = compiledSolution.CompiledProjects.CompiledProjects[0].Compilation.GetSpecialType(SpecialType.System_Int64);

            iBaseTypeSymbol = compiledSolution.CompiledProjects.CompiledProjects[0].Compilation.GetTypeByMetadataName(typeof(IBase).FullName);
            baseTypeSymbol = compiledSolution.CompiledProjects.CompiledProjects[0].Compilation.GetTypeByMetadataName(typeof(Base).FullName);
            childTypeSymbol = compiledSolution.CompiledProjects.CompiledProjects[0].Compilation.GetTypeByMetadataName(typeof(Child).FullName);
        }

        [Test]
        [TestCase("CallInt", true, true)]
        [TestCase("CallLong", true, false)]
        [TestCase("CallNullableInt", true, false)]
        [TestCase("CallNullableInts", false, null)]
        [TestCase("CallInts", false, null)]
        public void TestInt(string methodName, bool expectedSuitable, bool? expectedIdentity)
        {
            bool? identity;

            var extractMethod = ExtractMethod(typeof(OverloadCases).FullName, methodName);

            Assert.AreEqual(expectedSuitable, extractMethod.ParametersAreSuitable(new[] { intTypeSymbol }, out identity));
            Assert.AreEqual(expectedIdentity, identity);
        }

        [Test]
        [TestCase("CallInt", false, null)]
        [TestCase("CallLong", true, true)]
        [TestCase("CallNullableInt", false, null)]
        [TestCase("CallNullableInts", false, null)]
        [TestCase("CallInts", false, null)]
        public void TestLong(string methodName, bool expectedSuitable, bool? expectedIdentity)
        {
            bool? identity;

            var extractMethod = ExtractMethod(typeof(OverloadCases).FullName, methodName);

            Assert.AreEqual(expectedSuitable, extractMethod.ParametersAreSuitable(new[] { longTypeSymbol }, out identity));
            Assert.AreEqual(expectedIdentity, identity);
        }


        //public void CallIBase(IBase i)
        //public void CallBase(Base i)
        //public void CallChild(Child i)
        //public void CallIBases(IBase[] bases)
        //public void CallBases(Base[] bases)
        //public void CallChilds(Child[] bases)

        [Test]
        [TestCase("CallIBase", true, false)]
        [TestCase("CallBase", true, false)]
        [TestCase("CallChild", true, true)]
        [TestCase("CallIBases", false, null)]
        [TestCase("CallBases", false, null)]
        [TestCase("CallChilds", false, null)]
        public void TestChild(string methodName, bool expectedSuitable, bool? expectedIdentity)
        {
            bool? identity;

            var extractMethod = ExtractMethod(typeof(OverloadCases).FullName, methodName);

            Assert.AreEqual(expectedSuitable, extractMethod.ParametersAreSuitable(new[] { childTypeSymbol }, out identity));
            Assert.AreEqual(expectedIdentity, identity);
        }

        [Test]
        [TestCase("CallIBase", true, false)]
        [TestCase("CallBase", true, true)]
        [TestCase("CallChild", false, null)]
        [TestCase("CallIBases", false, null)]
        [TestCase("CallBases", false, null)]
        [TestCase("CallChilds", false, null)]
        public void TestBase(string methodName, bool expectedSuitable, bool? expectedIdentity)
        {
            bool? identity;

            var extractMethod = ExtractMethod(typeof(OverloadCases).FullName, methodName);

            Assert.AreEqual(expectedSuitable, extractMethod.ParametersAreSuitable(new[] { baseTypeSymbol }, out identity));
            Assert.AreEqual(expectedIdentity, identity);
        }

        //[Test]
        //[TestCase("CallIBase", false, null)]
        //[TestCase("CallBase", false, null)]
        //[TestCase("CallChild", false, null)]
        //[TestCase("CallIBases", true, false)]
        //[TestCase("CallBases", true, true)]
        //[TestCase("CallChilds", false, null)]
        //public void TestBases(string methodName, bool expectedSuitable, bool? expectedIdentity)
        //{
        //    bool? identity;

        //    var extractMethod = ExtractMethod(typeof(OverloadCases).FullName, methodName);

        //    var basesMethod = ExtractMethod(typeof(OverloadCases).FullName, "CallBases");
        //    var parameterSyntax = basesMethod.MethodDeclarationSyntax.ParameterList.Parameters.Single();
        //    var basesSymbol = (ITypeSymbol) basesMethod.SemanticModel.GetSymbolInfo(parameterSyntax).Symbol;

        //    Assert.AreEqual(expectedSuitable, extractMethod.ParametersAreSuitable(new[] { basesSymbol }, out identity));
        //    Assert.AreEqual(expectedIdentity, identity);
        //}
    }
}