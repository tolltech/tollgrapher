﻿using FluentAssertions;
using NUnit.Framework;
using Tolltech.Models;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapherTest.Common;
using Tolltech.TollGrapherTestCases.Tests.Generic;

namespace Tolltech.TollGrapherTest.Tests
{
    public class GenericTests : TollGrapherTestCasesTestBase
    {
        [Test]
        [TestCase("GenericEntity", "CallGenericProperty", "get_GenericProperty", true)]
        [TestCase("GenericEntity", "CallGenericChildProperty", "get_GenericProperty", true)]
        [TestCase("GenericEntity", "CallNonGenericChildProperty", "get_GenericProperty", true)]
        [TestCase("GenericClass", "CallGenericMethod", "Call", false)]
        [TestCase("GenericChildClass", "CallGenericChildMethod", "ChildCall", false)]
        [TestCase("GenericClass", "CallGenericChildBaseMethod", "Call", false)]
        [TestCase("GenericClass", "CallNonGenericChildMethod", "Call", false)]
        public void TestGetProperty(string callingClassName, string entryMethodName, string callingMethodName, bool isProperty)
        {
            var method = ExtractMethod(typeof(GenericCaller).FullName, entryMethodName);
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(GenericCaller).FullName}.{entryMethodName}",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"Tolltech.TollGrapherTestCases.Tests.Generic.{callingClassName}.{callingMethodName}",
                            NetworkCall = false,
                            EntryPoint = false,
                        },
                        Children = new TreeNode<MethodNode>[0]
                    },
                }
            });

            walking.Children[0].Node.IsProperty.Should().Be(isProperty);
        }
    }
}