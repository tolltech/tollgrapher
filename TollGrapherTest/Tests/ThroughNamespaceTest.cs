﻿using System;
using FluentAssertions;
using NUnit.Framework;
using Tolltech.Models;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapherTest.Common;
using Tolltech.TollGrapherTestCases.Tests;

namespace Tolltech.TollGrapherTest.Tests
{
    public class ThroughNamespaceTest : TollGrapherTestCasesTestBase
    {
        [Test]
        [TestCase(typeof(NamespaceAccessingCases), typeof(ClassWithStaticClass), "AccessToClassWithNamespace", "get_AccessToClassWithNamespace")]
        [TestCase(typeof(NamespaceAccessingCases), typeof(ClassForStaticClass), "AccessToStaticMethodWithNamespace", "AccessToStaticMethodWithNamespace")]
        public void TestGetStaticMethodsWithNamespace(Type classType, Type callingClassType, string methodName, string calledMethodName)
        {
            var method = ExtractMethod(classType.FullName, methodName);
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{classType.FullName}.{methodName}",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{callingClassType.FullName}.{calledMethodName}",
                            NetworkCall = false,
                            EntryPoint = false,
                        },
                        Children = new TreeNode<MethodNode>[0]
                    },
                }
            });
        }

        [Test]
        public void TestGetStaticClass()
        {
            var method = ExtractMethod(typeof(NamespaceAccessingCases).FullName, "AccessToMethodWithNamespace");
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(NamespaceAccessingCases).FullName}.AccessToMethodWithNamespace",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(ClassForStaticClass).FullName}.AccessToMethodWithNamespace",
                            NetworkCall = false,
                            EntryPoint = false,
                        },
                        Children = new TreeNode<MethodNode>[0]
                    },
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(ClassWithStaticClass).FullName}.get_AccessToClassWithNamespace",
                            NetworkCall = false,
                            EntryPoint = false,
                        },
                        Children = new TreeNode<MethodNode>[0]
                    },
                }
            });

            walking.Children[1].Node.IsProperty.Should().BeTrue();
        }
    }
}