﻿using Network;
using NUnit.Framework;
using Tolltech.Models;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapherTest.Common;
using Tolltech.TollGrapherTestCases.Tests;

namespace Tolltech.TollGrapherTest.Tests
{
    public class TollGrapherTestCasesTest : TollGrapherTestCasesTestBase
    {
        [Test]
        public void TestDummy()
        {
            var method = ExtractMethod(typeof(Dummy).FullName, "DummyCall");
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(Dummy).FullName}.DummyCall",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new TreeNode<MethodNode>[0]
            });
        }

        [Test]
        public void TestDummyOneLevel()
        {
            var method = ExtractMethod(typeof(OneLevel).FullName, "Call");
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(OneLevel).FullName}.Call",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(Dummy).FullName}.DummyCall",
                            NetworkCall = false
                        },
                        Children = new TreeNode<MethodNode>[0]
                    },
                }
            });
        }

        [Test]
        public void TestDummyOneLevelWithIfNotNull()
        {
            var method = ExtractMethod(typeof(OneLevel).FullName, "CallWithIfNotNull");
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(OneLevel).FullName}.CallWithIfNotNull",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(Dummy).FullName}.DummyCall",
                            NetworkCall = false
                        },
                        Children = new TreeNode<MethodNode>[0]
                    },
                }
            });
        }

        [Test]
        public void TestDoubleCalls()
        {
            var method = ExtractMethod(typeof(NetworkHttpHandler).FullName, "Call");
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(NetworkHttpHandler).FullName}.Call",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(Dummy).FullName}.DummyCall",
                            NetworkCall = false
                        },
                        Children = new TreeNode<MethodNode>[0]
                    }
                }
            });
        }

        [Test]
        public void TestPropertyCalls()
        {
            var method = ExtractMethod(typeof(OneLevel).FullName, "CallProperty");
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(OneLevel).FullName}.CallProperty",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(Dummy).FullName}.DummyCall",
                            NetworkCall = false
                        },
                        Children = new TreeNode<MethodNode>[0]
                    },
                }
            });
        }

        [Test]
        public void TestTwoLevels()
        {
            var method = ExtractMethod(typeof(TwoLevels).FullName, "Call");
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(TwoLevels).FullName}.Call",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(Dummy).FullName}.DummyCall",
                            NetworkCall = false
                        },
                        Children = new TreeNode<MethodNode>[0]
                    },
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(OneLevel).FullName}.Call",
                            NetworkCall = false
                        },
                        Children = new[]
                        {
                            new TreeNode<MethodNode>
                            {
                                Node = new MethodNode
                                {
                                    Name = $"{typeof(Dummy).FullName}.DummyCall",
                                    NetworkCall = false
                                },
                                Children = new TreeNode<MethodNode>[0]
                            },
                        }
                    },
                }
            });
        }
    }
}