﻿using System;
using System.Text;
using FluentAssertions;
using Ninject;
using NUnit.Framework;
using Tolltech.BlobStorage;
using Tolltech.Common;
using Tolltech.TollGrapherTest.Common;

namespace Tolltech.TollGrapherTest.Tests
{
    [Ignore("Не хочу палить токен, нужно запускать тесты в tc")]
    public class KonturDriveTest : TestBase
    {
        private IBlobStorage blobStorage;

        protected override void SetUp()
        {
            base.SetUp();

            blobStorage = standardKernel.Get<IBlobStorage>();
            standardKernel.Get<Settings>().KonturDriveApiKey = @"";
        }

        [Test]
        public void TestDummy()
        {
            var blobStr = Guid.NewGuid().ToString();
            var blobName = Guid.NewGuid().ToString();
            var blob = Encoding.UTF8.GetBytes(blobStr);
            blobStorage.WriteBlobAndReturnName(blob, blobName);
            var actual = Encoding.UTF8.GetString(blobStorage.GetLastBlob(blobName.Substring(0, 10)));

            actual.Should().Be(blobStr);
        }

        [Test]

        public void TestNull()
        {
            blobStorage.GetLastBlob(Guid.NewGuid().ToString()).Should().BeNull();
        }

        [Test]
        public void TestDummyStream()
        {
            var blob = Guid.NewGuid();
            var blobName = Guid.NewGuid().ToString();
            blobStorage.WriteBlobJsonAndReturnName(blob, blobName);
            var actual = blobStorage.GetLastJsonBlob<Guid>(blobName);

            actual.Should().Be(blob);
        }
    }
}