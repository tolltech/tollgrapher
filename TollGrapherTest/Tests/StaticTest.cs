using System;
using FluentAssertions;
using NUnit.Framework;
using Tolltech.Models;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapherTest.Common;
using Tolltech.TollGrapherTestCases.Tests;

namespace Tolltech.TollGrapherTest.Tests
{
    public class StaticTest : TollGrapherTestCasesTestBase
    {
        [Test]
        [TestCase(typeof(StaticClassTestCase), "StaticField")]
        [TestCase(typeof(StaticClassTestCase), "StaticProperty")]
        public void TestGetProperty(Type classType, string methodName)
        {
            var method = ExtractMethod(classType.FullName, methodName);
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{classType.FullName}.{methodName}",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(StaticClass).FullName}.get_{methodName}",
                            NetworkCall = false,
                            EntryPoint = false,
                        },
                        Children = new TreeNode<MethodNode>[0]
                    },
                }
            });

            walking.Children[0].Node.IsProperty.Should().BeTrue();
        }
    }
}