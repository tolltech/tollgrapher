﻿using System;
using FluentAssertions;
using NUnit.Framework;
using Tolltech.Models;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapherTest.Common;
using Tolltech.TollGrapherTestCases.Tests;
using Tolltech.TollGrapherTestCases.Tests.Models;

namespace Tolltech.TollGrapherTest.Tests
{
    public class PropertyTest : TollGrapherTestCasesTestBase
    {
        [Test]
        [TestCase(typeof(Dummy), "DummyCallWithAccessToProperty", false)]
        [TestCase(typeof(PropertyCases), "AnonymusTypeInLinq", false)]
        [TestCase(typeof(PropertyCases), "AnonymusType", false)]
        [TestCase(typeof(PropertyCases), "CallSet", true)]
        [TestCase(typeof(PropertyCases), "CallCtorSet", true)]
        [TestCase(typeof(PropertyCases), "NullableCall", false)]
        public void TestGetProperty(Type classType, string methodName, bool expectedSet)
        {
            var method = ExtractMethod(classType.FullName, methodName);
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);

            var getSet = expectedSet ? "set" : "get";

            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{classType.FullName}.{methodName}",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(Entity).FullName}.{getSet}_Property",
                            NetworkCall = false,
                            EntryPoint = false,
                        },
                        Children = new TreeNode<MethodNode>[0]
                    },
                }
            });

            walking.Children[0].Node.IsProperty.Should().BeTrue();
        }

        [Test]
        public void TestGetPropertyCallCtorGetSet()
        {
            var classType = typeof(PropertyCases);
            var methodName = "CallCtorGetSet";
            var method = ExtractMethod(classType.FullName, methodName);
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);

            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{classType.FullName}.{methodName}",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(Entity).FullName}.set_Property",
                            NetworkCall = false,
                            EntryPoint = false,
                        },
                        Children = new TreeNode<MethodNode>[0]
                    },
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(Entity2).FullName}.get_Property",
                            NetworkCall = false,
                            EntryPoint = false,
                        },
                        Children = new TreeNode<MethodNode>[0]
                    },
                }
            });

            walking.Children[0].Node.IsProperty.Should().BeTrue();
            walking.Children[1].Node.IsProperty.Should().BeTrue();
        }

        [TestCase(typeof(PropertyCases), "NullableCallWithinLinq", false)]
        [TestCase(typeof(PropertyCases), "CallWithinLinq", false)]
        [TestCase(typeof(PropertyCases), "CallSetWithinLinq", true)]
        public void TestGetPropertyWithinLinqDoubleCall(Type classType,string methodName, bool expectedSet)
        {
            var getSet = expectedSet ? "set" : "get";

            var method = ExtractMethod(classType.FullName, methodName);
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{classType.FullName}.{methodName}",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(Entity).FullName}.{getSet}_Property",
                            NetworkCall = false,
                            EntryPoint = false,
                        },
                        Children = new TreeNode<MethodNode>[0]
                    }
                }
            });

            walking.Children[0].Node.IsProperty.Should().BeTrue();
        }

        [Test]
        public void TestGetPropertyDummyCallWithAccessToPropertyWithinInvocation()
        {
            var method = ExtractMethod(typeof(Dummy).FullName, "DummyCallWithAccessToPropertyWithinInvocation");
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(Dummy).FullName}.DummyCallWithAccessToPropertyWithinInvocation",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(Entity).FullName}.get_Property",
                            NetworkCall = false,
                            EntryPoint = false,
                        },
                        Children = new TreeNode<MethodNode>[0]
                    },
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(Dummy).FullName}.GetEntity",
                            NetworkCall = false
                        },
                        Children = new TreeNode<MethodNode>[0]
                    },
                }
            });

            walking.Children[0].Node.IsProperty.Should().BeTrue();
        }

        [Test]
        public void TestGetPropertyDummyCallWithAccessToPropertyWithinCtorInvocation()
        {
            var method = ExtractMethod(typeof(Dummy).FullName, "DummyCallWithAccessToPropertyWithinCtorInvocation");
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(Dummy).FullName}.DummyCallWithAccessToPropertyWithinCtorInvocation",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(Entity).FullName}.get_Property",
                            NetworkCall = false,
                            EntryPoint = false,
                        },
                        Children = new TreeNode<MethodNode>[0]
                    },
                }
            });

            walking.Children[0].Node.IsProperty.Should().BeTrue();
        }




        [Test]
        public void TestGetPropertyWithBody()
        {
            var method = ExtractMethod(typeof(Dummy).FullName, "DummyCallWithAccessToPropertyWithBody");
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(Dummy).FullName}.DummyCallWithAccessToPropertyWithBody",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(Entity).FullName}.get_PropertyWithBody",
                            NetworkCall = false,
                            EntryPoint = false,
                        },
                        Children = new[]
                        {
                            new TreeNode<MethodNode>
                            {
                                Node = new MethodNode
                                {
                                    Name = $"{typeof(Dummy).FullName}.DummyCall",
                                    NetworkCall = false
                                },
                                Children = new TreeNode<MethodNode>[0]
                            }
                        }
                    },
                }
            });

            walking.Children[0].Node.IsProperty.Should().BeTrue();
            walking.Children[0].Children[0].Node.IsProperty.Should().BeFalse();
        }
    }
}