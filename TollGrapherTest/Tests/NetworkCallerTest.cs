﻿using Network;
using NetworkGateway;
using NUnit.Framework;
using Tolltech.Models;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapherTest.Common;
using Tolltech.TollGrapherTestCases.Tests;
using Tolltech.TollGrapherTestCases.Tests.Networks;

namespace Tolltech.TollGrapherTest.Tests
{
    public class NetworkCallerTest : TollGrapherTestCasesTestBase
    {
        [Test]
        public void TestNetworkMethodCalls()
        {
            var method = ExtractMethod(typeof(RpcNetwork).FullName, "Call");
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(RpcNetwork).FullName}.Call",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(Dummy).FullName}.DummyCall",
                            NetworkCall = false
                        },
                        Children = new TreeNode<MethodNode>[0]
                    },
                }
            });
        }

        [Test]
        public void TestCallRpc()
        {
            var method = ExtractMethod(typeof(NetworkCaller).FullName, "CallRpc");
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(NetworkCaller).FullName}.CallRpc",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(RpcNetwork).FullName}.Call",
                            NetworkCall = true
                        },
                        Children = new[]
                        {
                            new TreeNode<MethodNode>
                            {
                                Node = new MethodNode
                                {
                                    Name = $"{typeof(Dummy).FullName}.DummyCall",
                                    NetworkCall = false
                                },
                                Children = new TreeNode<MethodNode>[0]
                            },
                        }
                    },
                }
            });
        }

        [Test]
        public void TestCallNetwork()
        {
            var method = ExtractMethod(typeof(NetworkCaller).FullName, "CallNetwork");
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(NetworkCaller).FullName}.CallNetwork",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(NetworkClient).FullName}.Call",
                            NetworkCall = false
                        },
                        Children = new[]
                        {
                            new TreeNode<MethodNode>
                            {
                                Node =new MethodNode
                                {
                                    Name =  $"{typeof(NetworkHttpHandler).FullName}.Call",
                                    NetworkCall = true
                                },
                                Children = new[]
                                {
                                    new TreeNode<MethodNode>
                                    {
                                        Node = new MethodNode
                                        {
                                            Name = $"{typeof(Dummy).FullName}.DummyCall",
                                            NetworkCall = false
                                        },
                                        Children = new TreeNode<MethodNode>[0]
                                    }
                                }
                            }
                        }
                    },
                }
            });
        }

        [Test]
        public void TestCallExpressionBody()
        {
            var method = ExtractMethod(typeof(NetworkCaller).FullName, "CallExpressionBody");
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(NetworkCaller).FullName}.CallExpressionBody",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(NetworkClient).FullName}.CallExpressionBody",
                            NetworkCall = false
                        },
                        Children = new[]
                        {
                            new TreeNode<MethodNode>
                            {
                                Node =new MethodNode
                                {
                                    Name =  $"{typeof(NetworkHttpHandler).FullName}.Call",
                                    NetworkCall = true
                                },
                                Children = new[]
                                {
                                    new TreeNode<MethodNode>
                                    {
                                        Node = new MethodNode
                                        {
                                            Name = $"{typeof(Dummy).FullName}.DummyCall",
                                            NetworkCall = false
                                        },
                                        Children = new TreeNode<MethodNode>[0]
                                    }
                                }
                            }
                        }
                    },
                }
            });
        }

        [Test]
        public void TestCallNetworkWithFieldServiceName()
        {
            var method = ExtractMethod(typeof(NetworkCaller).FullName, "CallField");
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(NetworkCaller).FullName}.CallField",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(NetworkClient).FullName}.CallField",
                            NetworkCall = false
                        },
                        Children = new[]
                        {
                            new TreeNode<MethodNode>
                            {
                                Node =new MethodNode
                                {
                                    Name =  $"{typeof(NetworkHttpHandler).FullName}.Call",
                                    NetworkCall = true
                                },
                                Children = new[]
                                {
                                    new TreeNode<MethodNode>
                                    {
                                        Node = new MethodNode
                                        {
                                            Name = $"{typeof(Dummy).FullName}.DummyCall",
                                            NetworkCall = false
                                        },
                                        Children = new TreeNode<MethodNode>[0]
                                    }
                                }
                            }
                        }
                    },
                }
            });
        }

        [Test]
        public void TestCallNetworkGateway()
        {
            var method = ExtractMethod(typeof(NetworkCaller).FullName, "CallGateway");
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(NetworkCaller).FullName}.CallGateway",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(NetworkClient).FullName}.CallGateway",
                            NetworkCall = false
                        },
                        Children = new[]
                        {
                            new TreeNode<MethodNode>
                            {
                                Node =new MethodNode
                                {
                                    Name =  $"{typeof(NetworkGatewayHttpHandler).FullName}.Call",
                                    NetworkCall = true
                                },
                                Children = new[]
                                {
                                    new TreeNode<MethodNode>
                                    {
                                        Node = new MethodNode
                                        {
                                            Name = $"{typeof(Dummy).FullName}.DummyCall",
                                            NetworkCall = false
                                        },
                                        Children = new TreeNode<MethodNode>[0]
                                    }
                                }
                            }
                        }
                    },
                }
            });
        }

        [Test]
        public void TestCallClusterClientWithHttpCLientProvider()
        {
            var method = ExtractMethod(typeof(NetworkCaller).FullName, "CallClusterClientWithHttpCLientProvider");
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(NetworkCaller).FullName}.CallClusterClientWithHttpCLientProvider",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(NetworkClient).FullName}.CallClusterClientWithHttpCLientProvider",
                            NetworkCall = false
                        },
                        Children = new[]
                        {
                            new TreeNode<MethodNode>
                            {
                                Node =new MethodNode
                                {
                                    Name =  $"{typeof(NetworkHttpHandler).FullName}.Call",
                                    NetworkCall = true
                                },
                                Children = new[]
                                {
                                    new TreeNode<MethodNode>
                                    {
                                        Node = new MethodNode
                                        {
                                            Name = $"{typeof(Dummy).FullName}.DummyCall",
                                            NetworkCall = false
                                        },
                                        Children = new TreeNode<MethodNode>[0]
                                    }
                                }
                            }
                        }
                    },
                }
            });
        }

        [Test]
        public void TestCallClusterClientWithHttpClientFactory()
        {
            var method = ExtractMethod(typeof(NetworkCaller).FullName, "CallClusterClientWithHttpClientFactory");
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(NetworkCaller).FullName}.CallClusterClientWithHttpClientFactory",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(NetworkClient).FullName}.CallClusterClientWithHttpClientFactory",
                            NetworkCall = false
                        },
                        Children = new[]
                        {
                            new TreeNode<MethodNode>
                            {
                                Node =new MethodNode
                                {
                                    Name =  $"{typeof(NetworkGatewayHttpHandler).FullName}.Call",
                                    NetworkCall = true
                                },
                                Children = new[]
                                {
                                    new TreeNode<MethodNode>
                                    {
                                        Node = new MethodNode
                                        {
                                            Name = $"{typeof(Dummy).FullName}.DummyCall",
                                            NetworkCall = false
                                        },
                                        Children = new TreeNode<MethodNode>[0]
                                    }
                                }
                            }
                        }
                    },
                }
            });
        }
    }
}