﻿using System;
using NUnit.Framework;
using Tolltech.Models;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapherTest.Common;
using Tolltech.TollGrapherTestCases.Tests;
using Tolltech.TollGrapherTestCases.Tests.Models;

namespace Tolltech.TollGrapherTest.Tests
{
    public class BaseMethodsTest : TollGrapherTestCasesTestBase
    {
        [Test]
        [TestCase(typeof(BaseMethodsTestCases), typeof(VeryBaseEntity), "VeryBaseMethod")]
        [TestCase(typeof(BaseMethodsTestCases), typeof(BaseEntity), "BaseMethod")]
        public void TestGetBaseMethods(Type classType, Type callingClassType, string methodName)
        {
            var method = ExtractMethod(classType.FullName, methodName);
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{classType.FullName}.{methodName}",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{callingClassType.FullName}.{methodName}",
                            NetworkCall = false,
                            EntryPoint = false,
                        },
                        Children = new TreeNode<MethodNode>[0]
                    },
                }
            });
        }

        [Test]
        public void TestGetBaseWithProtected()
        {
            var method = ExtractMethod(typeof(BaseMethodsTestCases).FullName, "BaseMethodWithProtectedCall");
            var walking = extractedMethodWalker.BuildEntryPointWalking(method, compiledSolution.CompiledProjects, compiledSolution.Solution, Constants.TestNamespaceName);
            walking.AssertBusinessEquals(new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = $"{typeof(BaseMethodsTestCases).FullName}.BaseMethodWithProtectedCall",
                    NetworkCall = true,
                    EntryPoint = true
                },
                Children = new[]
                {
                    new TreeNode<MethodNode>
                    {
                        Node = new MethodNode
                        {
                            Name = $"{typeof(BaseEntity).FullName}.BaseMethodWithProtectedCall",
                            NetworkCall = false,
                            EntryPoint = false,
                        },
                        Children = new[]
                        {
                            new TreeNode<MethodNode>
                            {
                                Node = new MethodNode
                                {
                                    Name = $"{typeof(VeryBaseEntity).FullName}.ProtectedVeryBaseMethod",
                                    NetworkCall = false,
                                    EntryPoint = false,
                                },
                                Children = new TreeNode<MethodNode>[0]
                            },
                        }
                    },
                }
            });
        }
    }
}