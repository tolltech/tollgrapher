﻿using Ninject;
using NUnit.Framework;
using Tolltech.Common;
using Tolltech.Common.Configuration;

namespace Tolltech.TollGrapherTest.Common
{
    [TestFixture]
    public abstract class TestBase
    {
        protected StandardKernel standardKernel;

        [SetUp]
        protected virtual void SetUp()
        {
            standardKernel = new StandardKernel(new ConfigurationModule());

            var settingsBuilder = standardKernel.Get<ISettingsBuilder>();
            standardKernel.Bind<Settings>().ToConstant(settingsBuilder.Build(Constants.TestNamespaceName, null, null, null, false, false, 1));
        }

        [TearDown]
        protected virtual void TearDown()
        {

        }
    }
}