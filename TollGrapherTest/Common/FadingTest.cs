﻿using System;
using System.Threading;
using FluentAssertions;
using NUnit.Framework;
using Tolltech.Common;

namespace Tolltech.TollGrapherTest.Common
{
    public class FadingTest : TestBase
    {
        [Test]
        public void TestDummy()
        {
            var s = new Fading<string>("random", 20);
            s.HasFaded.Should().BeFalse();
            Assert.IsTrue(s == "random");
        }

        [Test]
        public void TestFaded()
        {
            var s = new Fading<string>("random", TimeSpan.FromTicks(1));
            Thread.Sleep(100);

            string str = s;
            str.Should().BeNull();
            s.HasFaded.Should().BeTrue();
            s.Value.Should().Be("random");
        }
    }
}