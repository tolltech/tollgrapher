﻿using System.IO;
using Microsoft.CodeAnalysis;
using Ninject;
using NUnit.Framework;
using Tolltech.TollGrapher.Models;
using Tolltech.TollGrapher.Parsing;

namespace Tolltech.TollGrapherTest.Common
{
    public abstract class TollGrapherTestCasesTestBase : TestBase
    {
        protected CompiledSolution compiledSolution;
        private IMethodsExtractor methodsExtractor;
        protected readonly string testSolutionPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "../../../TollGrapherTestCases/TollGrapherTestCasesSolution.sln");
        protected IExtractedMethodWalker extractedMethodWalker;

        protected override void SetUp()
        {
            base.SetUp();

            var solutionCompiler = standardKernel.Get<ISolutionCompiler>();
            compiledSolution = solutionCompiler.CompileSolution(testSolutionPath);
            methodsExtractor = standardKernel.Get<IMethodsExtractor>();

            extractedMethodWalker = standardKernel.Get<IExtractedMethodWalker>();
        }

        protected ExtractedMethod ExtractMethod(string fullTypeName, string methodName)
        {
            return methodsExtractor.ExtractMethod(compiledSolution.CompiledProjects, fullTypeName, methodName, new ITypeSymbol[0]);
        }
    }
}