﻿using FluentAssertions;
using Tolltech.Models;
using Tolltech.TollGrapher.Common;

namespace Tolltech.TollGrapherTest.Common
{
    public static class AssertExtensions
    {
        public static void AssertBusinessEquals(this TreeNode<MethodNode> actual, TreeNode<MethodNode> expected)
        {
            actual.Node.Name.Should().Be(expected.Node.Name);
            actual.Node.NetworkCall.Should().Be(expected.Node.NetworkCall);
            actual.Node.EntryPoint.Should().Be(expected.Node.EntryPoint);

            actual.Children?.Length.Should().Be(expected.Children?.Length);

            if (actual.Children != null)
            {
                for (var i = 0; i < actual.Children.Length; ++i)
                {
                    actual.Children[i].AssertBusinessEquals(expected.Children[i]);
                }
            }
        }

        public static void AssertTreeEquals<T>(this TreeNode<T> actual, TreeNode<T> expected) where T : struct
        {
            actual.Node.Should().Be(expected.Node);

            actual.Children?.Length.Should().Be(expected.Children?.Length);

            if (actual.Children != null)
            {
                for (var i = 0; i < actual.Children.Length; ++i)
                {
                    actual.Children[i].AssertTreeEquals(expected.Children[i]);
                }
            }
        }

        //public string Name { get; set; }
        //public bool NetworkCall { get; set; }
        //public bool EntryPoint { get; set; }
    }
}