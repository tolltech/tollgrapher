﻿using NUnit.Framework;
using Tolltech.TollGrapher.Common;

namespace Tolltech.TollGrapherTest.Common
{
    public class StringExtemnsionsTest : TestBase
    {
        [Test]
        [TestCase(null, ExpectedResult = null)]
        [TestCase("", ExpectedResult = "")]
        [TestCase("  ", ExpectedResult = "  ")]
        [TestCase("random", ExpectedResult = "random")]
        [TestCase("random<>", ExpectedResult = "random")]
        [TestCase("rand<>om<>", ExpectedResult = "rand")]
        [TestCase("<>random<>", ExpectedResult = "")]
        [TestCase("random<<>>", ExpectedResult = "random")]
        [TestCase("random<>>", ExpectedResult = "random")]
        [TestCase("random<<>", ExpectedResult = "random")]
        [TestCase("random<dd<>>", ExpectedResult = "random")]
        [TestCase("random<<dd>>", ExpectedResult = "random")]
        [TestCase("random<<>dd>", ExpectedResult = "random")]
        [TestCase("random<dd<dd>dd>", ExpectedResult = "random")]
        public string TestKillGeneric(string source)
        {
            return source.KillGenericTags();
        }
    }
}