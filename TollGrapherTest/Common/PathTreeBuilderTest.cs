﻿using System.Collections.Generic;
using NUnit.Framework;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapherApi.CrazyAlgorythms;

namespace Tolltech.TollGrapherTest.Common
{
    public class PathTreeBuilderTest : TestBase
    {

        [Test]
        [TestCaseSource(nameof(PathTreetestSource))]
        public void TestBuildTree(int root, List<int>[] paths, TreeNode<int> expected)
        {
            var actual = PathTreeBuilder.BuildTree(root, paths, x => x, x => x);
            //var s = JsonConvert.SerializeObject(actual);
            actual.AssertTreeEquals(expected);
        }

        static object[] PathTreetestSource =
        {
            new TestCaseData(
            new object[]
            {
                1,
                new []
                {
                    new List<int>{ 1,2,3}
                },
                new TreeNode<int>
                {
                    Node = 1,
                    Children = new []
                               {
                                   new TreeNode<int>
                                   {
                                       Node = 2,
                                       Children = new []
                                                  {
                                                      new TreeNode<int>
                                                      {
                                                          Node = 3
                                                      },
                                                  }
                                   },
                               }
                },
            }).SetName("simple 1 list"),
            new TestCaseData(
            new object[]
            {
                1,
                new []
                {
                    new List<int>{ 1,2,3, 4},
                    new List<int>{ 1,3, 4},
                    new List<int>{ 1,2, 4},
                    new List<int>{ 1,5},
                },
                new TreeNode<int>
                {
                    Node = 1,
                    Children = new []
                               {
                                   new TreeNode<int>
                                   {
                                       Node = 2,
                                       Children = new []
                                                  {
                                                      new TreeNode<int>
                                                      {
                                                          Node = 3,
                                                          Children = new []
                                                                     {
                                                                         new TreeNode<int>
                                                                         {
                                                                             Node = 4,
                                                                         },
                                                                     }
                                                      },
                                                      new TreeNode<int>
                                                      {
                                                          Node = 4,
                                                      },
                                                  },

                                   },
                                   new TreeNode<int>
                                   {
                                       Node = 3,
                                       Children = new []
                                                  {
                                                      new TreeNode<int>
                                                      {
                                                          Node = 4,
                                                      },
                                                  }
                                   },
                                   new TreeNode<int>
                                   {
                                       Node = 5,
                                   },
                               }
                },
            }).SetName("multi list"),
            new TestCaseData(
            new object[]
            {
                1,
                new []
                {
                    new List<int>{ 1,2,3},
                    new List<int>{ 1,3,2}
                },
                new TreeNode<int>
                {
                    Node = 1,
                    Children = new []
                               {
                                   new TreeNode<int>
                                   {
                                       Node = 2,
                                       Children = new []
                                                  {
                                                      new TreeNode<int>
                                                      {
                                                          Node = 3
                                                      },
                                                  }
                                   },
                                   new TreeNode<int>
                                   {
                                       Node = 3,
                                       Children = new []
                                                  {
                                                      new TreeNode<int>
                                                      {
                                                          Node = 2
                                                      },
                                                  }
                                   },
                               }
                },
            }).SetName("list recursion")
        };
    }
}