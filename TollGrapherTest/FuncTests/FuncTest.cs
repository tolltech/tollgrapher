﻿using System.Linq;
using FluentAssertions;
using Ninject;
using NUnit.Framework;
using SourceCodeData;
using Tolltech.BlobStorage;
using Tolltech.Common;
using Tolltech.Neo4jData;
using Tolltech.TollGrapher.Parsing;
using Tolltech.TollGrapher.Walkings;
using Tolltech.TollGrapherApi;
using Tolltech.TollGrapherApi.Models;
using Tolltech.TollGrapherTest.Common;

namespace Tolltech.TollGrapherTest.FuncTests
{
    //todo: seems to these tests cant run together (neo4j concurrency?)
    public class FuncTest : TollGrapherTestCasesTestBase
    {
        private ISolutionAnalyser solutionAnalyser;
        private ISolutionWalker solutionWalker;
        private ITollGrapherSearchClient tollGrapherSearchClient;

        // ReSharper disable NotAccessedField.Local
        private IGrapherHandler grapherHandler;
        private IBlobStorage blobStorage;
        private ISourceCodeRepo sourceCodeRepo;
        // ReSharper restore NotAccessedField.Local

        protected override void SetUp()
        {
            base.SetUp();

            blobStorage = standardKernel.Get<IBlobStorage>();
            sourceCodeRepo = standardKernel.Get<ISourceCodeRepo>();
            solutionAnalyser = standardKernel.Get<ISolutionAnalyser>();
            grapherHandler = standardKernel.Get<IGrapherHandler>();
            solutionWalker = standardKernel.Get<ISolutionWalker>();
            tollGrapherSearchClient = standardKernel.Get<ITollGrapherSearchClient>();
        }

        [Test]
        public void TestGetAllEntryPoints()
        {
            var walkings = solutionAnalyser.BuildWalkings(testSolutionPath);
            solutionWalker.WriteWalkingsToNeo4j(walkings);

            var methods = tollGrapherSearchClient.GetMethodNames("Dumm", "DummyCall2", 1);
            methods.Length.Should().Be(1);
            var method = methods.Single();

            var actuals = tollGrapherSearchClient.GetAllEntryPoints(method.Id);
            actuals.Length.Should().Be(1);
            actuals[0].ClassName.Should().Be("CaseController");
            actuals[0].MethodName.Should().Be("CallDeep");
        }

        [Test]
        public void TestGetAllEntryPointsByClassWithNamespace()
        {
            var walkings = solutionAnalyser.BuildWalkings(testSolutionPath);
            solutionWalker.WriteWalkingsToNeo4j(walkings);

            var methods = tollGrapherSearchClient.GetMethodNames("Tolltech.TollGrapherTestCases.Tests", "Dummy", "DummyCall2", 1);
            methods.Length.Should().Be(1);
            var method = methods.Single();
            method.FullName.Should().BeEquivalentTo("Tolltech.TollGrapherTestCases.Tests.Dummy.DummyCall2");
        }

        [Test]
        [Ignore("Не палим ключ")]
        public void TestGetSources()
        {
            standardKernel.Get<Settings>().KonturDriveApiKey = @"";

            var walkings = solutionAnalyser.BuildWalkings(testSolutionPath);
            solutionWalker.WriteWalkingsToNeo4j(walkings);
            solutionWalker.WriteSourcesToBlobStorage(walkings);

            var methods = tollGrapherSearchClient.GetMethodNames("Dummy", "DummyCall2", 1);
            methods.Length.Should().Be(1);
            var method = methods.Single();

            var source = tollGrapherSearchClient.GetMethodSourceCode(method.Id);
        }


        [Test]
        public void TestGetAllClassNames()
        {
            var walkings = solutionAnalyser.BuildWalkings(testSolutionPath);
            solutionWalker.WriteWalkingsToNeo4j(walkings);

            var classes = tollGrapherSearchClient.GetClassNames("Dummy", 1);
            classes.Length.Should().Be(1);

            var classModel = classes.Single();
            classModel.ClassName.Should().Be("Dummy");
            classModel.Namespace.Should().Be("Tolltech.TollGrapherTestCases.Tests");
            classModel.ServiceName.Should().Be("TollGrapherTestCases");
        }

        [Test]
        public void TestGetAllEntryPointsOfProperty()
        {
            var walkings = solutionAnalyser.BuildWalkings(testSolutionPath);
            solutionWalker.WriteWalkingsToNeo4j(walkings);

            var methods = tollGrapherSearchClient.GetMethodNames("Entity2", "Property", 2);
            methods.Length.Should().Be(1);
            var method = methods.Single();
            method.MethodName.Should().Be("get_Property");

            var actuals = tollGrapherSearchClient.GetAllEntryPoints(method.Id);
            actuals.Length.Should().Be(1);
            actuals[0].ClassName.Should().Be("CaseController");
            actuals[0].MethodName.Should().Be("CallWithProperty");
            actuals[0].ReturnType.Should().Be("void");
            actuals[0].Parameters.Should().BeEmpty();
        }

        [Test]
        public void TestGetAllNetworkPaths()
        {
            var walkings = solutionAnalyser.BuildWalkings(testSolutionPath);
            solutionWalker.WriteWalkingsToNeo4j(walkings);

            var leafMethods = tollGrapherSearchClient.GetMethodNames("Dummy", "DummyCall", 10)
                .Where(x => x.MethodName == "DummyCall" && x.Parameters.Length == 0)
                .ToArray();
            leafMethods.Length.Should().Be(1);
            var leafMethod = leafMethods.Single();

            var rootMethods = tollGrapherSearchClient.GetMethodNames("CaseController", "PropertyCallWithOneNetwork", 2);
            rootMethods.Length.Should().Be(1);
            var rootMethod = rootMethods.Single();
            rootMethod.MethodName.Should().Be("PropertyCallWithOneNetwork");

            var actuals = tollGrapherSearchClient.GetAllNetworkPaths(rootMethod.Id, leafMethod.Id);
            actuals.Length.Should().Be(1);
            var actual = actuals.Single();
            actual.Select(x => $"{x.ClassName}.{x.MethodName}").ToArray()
                .Should().BeEquivalentTo(new []
                                         {
                                             "CaseController.PropertyCallWithOneNetwork",
                                             "RpcNetwork.CallProperty",
                                             "Dummy.DummyCall",
                                         });
        }

        [Test]
        public void TestCheckParametersInfo()
        {
            var walkings = solutionAnalyser.BuildWalkings(testSolutionPath);
            solutionWalker.WriteWalkingsToNeo4j(walkings);

            var methods = tollGrapherSearchClient.GetMethodNames("Dummy", "DummyCallParameters", 1);
            methods.Length.Should().Be(1);
            var method = methods.Single();

            var expected = new[]
            {
                new ParameterModel
                {
                    Modifiers = new string[0],
                    Name = "i",
                    Type = "int"
                },
                new ParameterModel
                {
                    Modifiers = new string[0],
                    Name = "l",
                    Type = "long"
                },
                new ParameterModel
                {
                    Modifiers = new[] {"params"},
                    Name = "s",
                    Type = "string[]"
                },
            };

            method.Parameters.Length.Should().Be(3);
            method.Parameters[0].ShouldBeEquivalentTo(expected[0]);
            method.Parameters[1].ShouldBeEquivalentTo(expected[1]);
            method.Parameters[2].ShouldBeEquivalentTo(expected[2]);
        }
    }
}