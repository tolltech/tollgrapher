﻿using NUnit.Framework;
using Tolltech.TollGrapherApi.DataScience;
using Tolltech.TollGrapherTest.Common;

namespace Tolltech.TollGrapherTest.DataScienceTest
{
    public class StringSearchExtensionsTest : TestBase
    {
        [Test]
        [TestCase("abc","abc", ExpectedResult = 100)]
        [TestCase("abC","aBc", ExpectedResult = 100)]
        [TestCase("ab1C","aB1c", ExpectedResult = 100)]
        [TestCase("BillManager","BillManagerExtension", ExpectedResult = 99)]
        [TestCase("ИшддЬфтфпук","BillManagerExtension", ExpectedResult = 99)]
        [TestCase("B1secMana", "Bill1secondManager", ExpectedResult = 99)]
        [TestCase("B1secMana", "BillExtraChars1secondManager", ExpectedResult = 99)]
        [TestCase("И1ыусЬфтф", "BillExtraChars1secondManager", ExpectedResult = 99)]
        [TestCase("PashaBeManager", "PashaStarSeniorProgrammerBestManager", ExpectedResult = 99)]
        [TestCase("PashaProgrammerBad", "PashaStarSeniorProgrammerBestManager", ExpectedResult = 0)]
        [TestCase("", "", ExpectedResult = 100)]
        [TestCase("", "PashaStarSeniorProgrammerBestManager", ExpectedResult = 0)]
        [TestCase("PBS", "", ExpectedResult = 0)]
        [TestCase("B", "PashaStarSeniorProgrammerBestManager", ExpectedResult = 99)]
        [TestCase("PSSPBM", "PashaStarSeniorProgrammerBestManager", ExpectedResult = 99)]
        [TestCase("psspbm", "PashaStarSeniorProgrammerBestManager", ExpectedResult = 99)]
        [TestCase("passpbm", "PashaStarSeniorProgrammerBestManager", ExpectedResult = 0)]//right?or wrong?
        public int TestOverlapp(string searchString, string srcString)
        {
            return srcString.GetOverlapPercents(searchString);
        }
    }
}