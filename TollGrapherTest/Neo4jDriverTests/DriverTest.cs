﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Neo4jClient;
using Newtonsoft.Json;
using Ninject;
using NUnit.Framework;
using Tolltech.Common;
using Tolltech.Neo4jData;
using Tolltech.Neo4jData.Schema;
using Tolltech.TollGrapherTest.Common;

namespace Tolltech.TollGrapherTest.Neo4jDriverTests
{
    public class DriverTest : TestBase
    {
        private IGrapherHandler grapherHandler;

        protected override void SetUp()
        {
            base.SetUp();

            grapherHandler = standardKernel.Get<IGrapherHandler>();

            var neo4JNodes = standardKernel.GetAll<INeo4jNode>();
            foreach (var neo4JNode in neo4JNodes)
            {
                grapherHandler.CreateUniqueConstraints(neo4JNode.GetType(), "Id");
            }
        }

        [Test]
        public void TestCreateRelated()
        {
            var obj1 = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 42,
                StrValue = "4242"
            };

            var obj2 = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 43,
                StrValue = "4343"
            };

            grapherHandler.CreateRelated(obj1, obj2, "realtionNewNodes");

            var result = grapherHandler.FindWithChilds<TestObject>(obj1.Id, "realtionNewNodes");
            result.Parent.ShouldBeEquivalentTo(obj1);
            result.Children.ShouldBeEquivalentTo(new[] { obj2 });
        }

        [Test]
        public void TestCreateRelatedAndSelectWithMultiChilds()
        {
            var obj1 = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 42,
                StrValue = "4242"
            };

            var obj2 = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 43,
                StrValue = "4343"
            };

            var obj3 = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 44,
                StrValue = "4444"
            };

            grapherHandler.CreateRelated(obj1, obj2, "realtionNewNodes");

            grapherHandler.Create(obj3);

            grapherHandler.CreateRelation<TestObject>(obj1.Id, obj3.Id, "realtionNewNodes");

            var result = grapherHandler.FindWithChilds<TestObject>(obj1.Id, "realtionNewNodes");
            result.Parent.ShouldBeEquivalentTo(obj1);
            result.Children.ShouldBeEquivalentTo(new[] { obj2, obj3 });
        }

        [Test]
        public void TestFindAllParents()
        {
            var child = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 42,
                StrValue = "4242"
            };

            var notChild = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 42,
                StrValue = "424242"
            };

            var parent1 = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 43,
                StrValue = "4343"
            };

            var parent2 = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 44,
                StrValue = "444424"
            };

            var notParent3 = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 44,
                StrValue = "44424244"
            };

            grapherHandler.CreateRelated(parent1, child, "realtion1");

            grapherHandler.Create(parent2);
            grapherHandler.CreateRelation<TestObject>(parent2.Id, child.Id, "realtion2");

            grapherHandler.CreateRelated(notParent3, notChild, "realtion1");

            grapherHandler.CreateRelation<TestObject>(notParent3.Id, notChild.Id, "notRelation");

            var result = grapherHandler.FindAllParents<TestObject, TestObject>(child.Id, new[] { "realtion1", "realtion2" });

            result.ShouldBeEquivalentTo(new[] { parent1, parent2 });
        }

        private static TestObject CreateRandomObject()
        {
            return new TestObject
                   {
                       Id = Guid.NewGuid(),
                       IntValue = 42,
                       StrValue = Guid.NewGuid().ToString()
            };
        }

        [Test]
        public void TestSelectPaths()
        {
            var root = CreateRandomObject();
            var node11 = CreateRandomObject();
            var node12 = CreateRandomObject();
            var node21 = CreateRandomObject();
            var node31 = CreateRandomObject();
            var node32 = CreateRandomObject();
            var child = CreateRandomObject();

            var edgeLabel = "realtion";
            grapherHandler.Create(root);
            grapherHandler.CreateRelation(root.Id, node11, edgeLabel);
            grapherHandler.CreateRelation(root.Id, node12, edgeLabel);
            grapherHandler.CreateRelation(node11.Id, node21, edgeLabel);
            grapherHandler.CreateRelation<TestObject>(node12.Id, node21.Id, edgeLabel);
            grapherHandler.CreateRelation(node21.Id, node31, edgeLabel);
            grapherHandler.CreateRelation(node21.Id, node32, edgeLabel);
            grapherHandler.CreateRelation(node31.Id, child, edgeLabel);
            grapherHandler.CreateRelation<TestObject>(node32.Id, child.Id, edgeLabel);

            var result = grapherHandler.SelectPaths<TestObject>(root.Id, child.Id, new [] {edgeLabel});

            result.ShouldBeEquivalentTo(new[]
                                        {
                                            new List<TestObject>{root, node11, node21, node31, child},
                                            new List<TestObject>{root, node12, node21, node31, child},
                                            new List<TestObject>{root, node11, node21, node32, child},
                                            new List<TestObject>{root, node12, node21, node32, child},
                                        });
        }

        [Test]
        public void TestFindAllParentsSeveralLayers()
        {
            var child = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 42,
                StrValue = "4242"
            };

            var childWithChild1 = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 4234322,
                StrValue = "42ds42"
            };

            var notChild = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 42,
                StrValue = "424242"
            };

            var parent1 = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 43,
                StrValue = "4343"
            };

            var parent2 = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 44,
                StrValue = "444424"
            };

            var notParent3 = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 44,
                StrValue = "44424244"
            };

            grapherHandler.Create(parent1,
                new[]
                {
                    typeof(TestObject).GetAttributeValues<LabelAttribute, string>(x => x.Name).Single(), "parentLabel"
                });
            grapherHandler.CreateRelation(parent1.Id, childWithChild1, "realtion1");

            grapherHandler.Create(child);
            grapherHandler.CreateRelation<TestObject>(childWithChild1.Id, child.Id, "realtion2");

            grapherHandler.Create(parent2);
            grapherHandler.CreateRelation<TestObject>(parent2.Id, child.Id, "realtion2");

            grapherHandler.CreateRelated(notParent3, notChild, "realtion1");

            grapherHandler.CreateRelation<TestObject>(notParent3.Id, notChild.Id, "notRelation");

            var result = grapherHandler.FindAllParents<TestObject, TestObject>(child.Id, new[] { "realtion1", "realtion2" });

            result.ShouldBeEquivalentTo(new[] { parent1, parent2, childWithChild1 });

            result = grapherHandler.FindAllParents<TestObject, TestObject>(child.Id, new[] { "realtion1", "realtion2" }, "parentLabel");

            result.ShouldBeEquivalentTo(new[] { parent1 });
        }



        [Test]
        public void TestCreateRelatedWithNullEdgeLabel()
        {
            var obj1 = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 42,
                StrValue = "4242"
            };

            var obj2 = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 43,
                StrValue = "4343"
            };

            grapherHandler.CreateRelated(obj1, obj2, "realtionNewNodes");

            var result = grapherHandler.FindWithChilds<TestObject>(obj1.Id);
            result.Parent.ShouldBeEquivalentTo(obj1);
            result.Children.ShouldBeEquivalentTo(new[] { obj2 });
        }

        [Test]
        public void TestCreateRelation()
        {
            var obj1 = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 42,
                StrValue = "4242"
            };

            var obj2 = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 43,
                StrValue = "4343"
            };

            grapherHandler.Create(obj1);
            grapherHandler.Create(obj2);
            grapherHandler.CreateRelation<TestObject>(obj1.Id, obj2.Id, "realtionExistedNodes");

            var result = grapherHandler.FindWithChilds<TestObject>(obj1.Id, "realtionExistedNodes");
            result.Parent.ShouldBeEquivalentTo(obj1);
            result.Children.ShouldBeEquivalentTo(new[] { obj2 });
        }

        [Test]
        public void TestCreateRelationDifferentNodes()
        {
            var obj1 = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 42,
                StrValue = "4242"
            };

            var obj2 = new TestObject2
            {
                Id = Guid.NewGuid(),
                IntValue = 43,
                StrValue = "4343"
            };

            grapherHandler.Create(obj1);
            grapherHandler.Create(obj2);
            grapherHandler.CreateRelation<TestObject, TestObject2>(obj1.Id, obj2.Id, "relationDIfferentNodes");

            var result = grapherHandler.FindWithChilds<TestObject, TestObject2>(obj1.Id, "realtionExistedNodes");
            result.Parent.ShouldBeEquivalentTo(obj1);
            result.Children.ShouldBeEquivalentTo(new[] { obj2 });
        }

        [Test]
        public void TestCreateRelationAndChild()
        {
            var obj1 = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 42,
                StrValue = "4242"
            };

            var obj2 = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 43,
                StrValue = "4343"
            };

            grapherHandler.Create(obj1);
            grapherHandler.CreateRelation(obj1.Id, obj2, "realtionExistedParent");

            var result = grapherHandler.FindWithChilds<TestObject>(obj1.Id, "realtionExistedParent");
            result.Parent.ShouldBeEquivalentTo(obj1);
            result.Children.ShouldBeEquivalentTo(new[] { obj2 });
        }

        [Test]
        public void FindNull()
        {
            var id = Guid.NewGuid();
            var testObject = grapherHandler.Find<TestObject>(id);
            testObject.Should().BeNull();
        }

        [Test]
        public void CreateAndFind()
        {
            var obj = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 42,
                StrValue = "4242"
            };

            grapherHandler.Create(obj);
            var testObject = grapherHandler.Find<TestObject>(obj.Id);
            testObject.ShouldBeEquivalentTo(obj);
        }

        [Test]
        public void CreateAndFindMultiLabel()
        {
            var obj = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 42,
                StrValue = "4242"
            };

            grapherHandler.Create(obj, new[] { "label1", "label2" });
            var testObject = grapherHandler.Find<TestObject>(obj.Id, "label1");
            testObject.ShouldBeEquivalentTo(obj);

            testObject = grapherHandler.Find<TestObject>(obj.Id, "label2");
            testObject.ShouldBeEquivalentTo(obj);

            testObject = grapherHandler.Find<TestObject>(obj.Id, "label3");
            testObject.Should().BeNull();
        }

        [Test]
        public void CreateDoubleUniqueIndex()
        {
            var obj1 = new TestObject
            {
                Id = Guid.NewGuid(),
                IntValue = 42,
                StrValue = "4242"
            };

            var obj2 = new TestObject
            {
                Id = obj1.Id,
                IntValue = 43,
                StrValue = "4343"
            };

            grapherHandler.Create(obj1);
            Assert.Throws<NeoException>(() => grapherHandler.Create(obj2));
        }

        [Test]
        public void TestDeleteAll()
        {
            var id1 = Guid.NewGuid();
            var obj1 = new TestObject
            {
                Id = id1,
                IntValue = 42,
                StrValue = "4242"
            };

            var id2 = Guid.NewGuid();
            var obj2 = new TestObject2
            {
                Id = id2,
                IntValue = 43,
                StrValue = "4343"
            };

            grapherHandler.Create(obj1);
            grapherHandler.Create(obj2);

            grapherHandler.DeleteAll<TestObject2>();

            grapherHandler.Find<TestObject>(id1).ShouldBeEquivalentTo(obj1);
            grapherHandler.Find<TestObject2>(id2).Should().BeNull();
        }

        [Test]
        public void TestDeleteAllNonGeneric()
        {
            var id1 = Guid.NewGuid();
            var obj1 = new TestObject
            {
                Id = id1,
                IntValue = 42,
                StrValue = "4242"
            };

            var id2 = Guid.NewGuid();
            var obj2 = new TestObject2
            {
                Id = id2,
                IntValue = 43,
                StrValue = "4343"
            };

            grapherHandler.Create(obj1);
            grapherHandler.Create(obj2);

            grapherHandler.DeleteAll("TestLabel1");

            grapherHandler.Find<TestObject2>(id2).ShouldBeEquivalentTo(obj2);
            grapherHandler.Find<TestObject>(id1).Should().BeNull();
        }

        [Test]
        public void TestComplexObjects()
        {
            var id1 = Guid.NewGuid();
            var objs = new TestObjects
            {
                Id = id1,
                Objects = new[]
                {
                    new TestObjectSingle
                    {
                        Prop = "prop1"
                    },
                }
            };
            grapherHandler.Create(objs);

            var actual = grapherHandler.Find<TestObjects>(id1);
            actual.ShouldBeEquivalentTo(objs);
        }
    }


    [Label("TestLabel1")]
    public class TestObject : INeo4jNode
    {
        public Guid Id { get; set; }
        public string StrValue { get; set; }
        public int IntValue { get; set; }
    }

    [Label("TestLabel2")]
    public class TestObject2 : INeo4jNode
    {
        public Guid Id { get; set; }
        public string StrValue { get; set; }
        public int IntValue { get; set; }
    }

    public class TestObject12 : INeo4jNode
    {
        public Guid Id { get; set; }
        public string StrValue { get; set; }
        public int IntValue { get; set; }
    }

    [Label("TestObjects")]
    public class TestObjects : INeo4jNode
    {
        public Guid Id { get; set; }
        public string privateProp { get; set; }

        [JsonIgnore]
        public TestObjectSingle[] Objects
        {
            get { return privateProp == null ? new TestObjectSingle[0] : new[] { new TestObjectSingle { Prop = privateProp }, }; }
            set { privateProp = value.Length > 0 ? value[0].Prop : null; }
        }
    }

    public class TestObjectSingle
    {
        public string Prop { get; set; }
    }
}