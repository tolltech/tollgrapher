#addin "Cake.IIS"
#addin "Cake.Yarn"

//////////////////////////////////////////////////////////////////////
// ARGUMENTS
//////////////////////////////////////////////////////////////////////

var target = Argument("target", "Default");
var configuration = Argument("configuration", "Release");
var serverName = Argument("serverName", "");

//////////////////////////////////////////////////////////////////////
// PREPARATION
//////////////////////////////////////////////////////////////////////

var localhost = "localhost";

// Define directories.

var rootDir = "../..";

var grapherWebDir = "../../Web/GrapherWeb";
var grapherWebSolutionPath = grapherWebDir + "/GrapherWeb.csproj";

var buildWebPath = grapherWebDir + "/bin";
var buildWebDir = Directory(buildWebPath) + Directory(configuration);

//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////

// SETUP //

private bool IsLocalhost(string serverName)
{
    return string.Equals(serverName, localhost, StringComparison.InvariantCultureIgnoreCase);
}

private bool RunningLocally(string serverName)
{
    return IsLocalhost(serverName) && !TeamCity.IsRunningOnTeamCity;
}

TaskSetup(setupContext =>
{
    if (!RunningLocally(serverName))
    {
        TeamCity.WriteStartBlock(setupContext.Task.Name);
    }
});

TaskTeardown(teardownContext =>
{
    if (!RunningLocally(serverName))
    {
        TeamCity.WriteEndBlock(teardownContext.Task.Name);
    }
});

//////////////////////////////////////////////////////////////////////

Task("CheckParameters")
    .Description("Checking incoming parameters")
    .Does(() =>
{
    if (string.IsNullOrEmpty(serverName))
    {
        throw new Exception("Server Name must be defined");
    }
});

//////////////////////////////////////////////////////////////////////

Task("BuildStatic")
    .IsDependentOn("CheckParameters")
    .Description("Bundling React components and compiling SCSS scripts")
    .Does(() =>
{
    var yarnRunner = Yarn.FromPath(grapherWebDir);

    if (RunningLocally(serverName))
    {
        yarnRunner.RunScript("build");
    }
    else
    {
        yarnRunner.RunScript("deploy");
    }
});

//////////////////////////////////////////////////////////////////////

Task("ApplicationPool-Create")
    .Description("Creating an ApplicationPool for GrapherWeb")
    .WithCriteria(() => PoolExists(serverName, "GrapherWeb") == false)
    .Does(() =>
{
    CreatePool(serverName, new ApplicationPoolSettings()
                           {
                               Name = "GrapherWeb",
                               IdentityType = IdentityType.LocalService,
                               ManagedRuntimeVersion = "",
                               ClassicManagedPipelineMode = false,
                               Autostart = false,
                               Overwrite = true
                           });
});

var distrPath = @"Grapher.Distr\GrapherWeb";
var webPath = System.IO.Path.GetPathRoot(Environment.SystemDirectory) + distrPath;

Task("Website-Create")
    .Description("Creating a GrapherWeb Website")
    .IsDependentOn("ApplicationPool-Create")
    .WithCriteria(() => SiteExists(serverName, "GrapherWeb") == false)
    .Does(() =>
{
    CreateWebsite(serverName, new WebsiteSettings()
                              {
                                  Name = "GrapherWeb",
                                  PhysicalDirectory = webPath,
                                  ServerAutoStart = false,
                                  Overwrite = true,

                                  Binding = IISBindings.Http
                                                .SetHostName("")
                                                .SetIpAddress("*")
                                                .SetPort(9900),

                                  ApplicationPool = new ApplicationPoolSettings()
                                  {
                                      Name = "GrapherWeb"
                                  }
                              });
});

//////////////////////////////////////////////////////////////////////

Task("Website-Stop")
    .Description("Stopping a GrapherWeb Website")
    .Does(() =>
{
    StopSite(serverName, "GrapherWeb");
});

Task("ApplicationPool-Stop")
    .Description("Stopping GrapherWeb ApplicationPool")
    .IsDependentOn("Website-Stop")
    .Does(() =>
{
    StopPool(serverName, "GrapherWeb");
});

Task("Clean")
    .Does(() =>
{
    CleanDirectory(buildWebDir);
});

Task("Restore-NuGet-Packages")
    .IsDependentOn("Clean")
    .Does(() =>
{
    NuGetRestore(rootDir);
    DotNetCoreRestore(grapherWebDir);
});

Task("PublishWeb")
    .IsDependentOn("ApplicationPool-Stop")
    .IsDependentOn("Restore-NuGet-Packages")
    .Does(() =>
{
    var settings = new DotNetCorePublishSettings
                       {
                           Framework = "net461",
                           Configuration = configuration,
                           OutputDirectory = @"\\" + serverName + @"\C$\" + distrPath
                       };

    DotNetCorePublish(grapherWebDir, settings);
});

Task("ApplicationPool-Start")
    .Description("Starting GrapherWeb ApplicationPool")
    .IsDependentOn("PublishWeb")
    .Does(() =>
{
    StartPool(serverName, "GrapherWeb");
});

Task("Website-Start")
    .Description("Stoppoing a GrapherWeb Website")
    .IsDependentOn("ApplicationPool-Start")
    .Does(() =>
{
    StartSite(serverName, "GrapherWeb");
});

//////////////////////////////////////////////////////////////////////
// TASK TARGETS
//////////////////////////////////////////////////////////////////////

Task("Publish")
    .IsDependentOn("CheckParameters")
    .IsDependentOn("BuildStatic")
    .IsDependentOn("Website-Start");

Task("RegisterWebsite")
    .IsDependentOn("CheckParameters")
    .IsDependentOn("Website-Create");

Task("Default")
    .IsDependentOn("CheckParameters")
    .IsDependentOn("RegisterWebsite")
    .IsDependentOn("Publish");

//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////

RunTarget(target);
