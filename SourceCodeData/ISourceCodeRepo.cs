﻿using System;
using System.Collections.Generic;
using Tolltech.Models;

namespace SourceCodeData
{
    public interface ISourceCodeRepo
    {
        void WriteSources(Dictionary<Guid, SourceCodeInfo> methodSources);
        SourceCodeInfo GetSources(Guid methodId);
    }
}