﻿using System;
using System.Collections.Generic;
using Tolltech.BlobStorage;
using Tolltech.Common;
using Tolltech.Models;

namespace SourceCodeData
{
    public class SourceCodeRepo : ISourceCodeRepo
    {
        private readonly IBlobStorage blobStorage;

        public SourceCodeRepo(IBlobStorage blobStorage)
        {
            this.blobStorage = blobStorage;
        }

        private const string blobName = "methodSources";

        public void WriteSources(Dictionary<Guid, SourceCodeInfo> methodSources)
        {
            blobStorage.WriteBlobJsonAndReturnName(methodSources, $"{blobName}/{DateTime.UtcNow:yyyy/MM/dd/hh/mm/ss/}");
        }

        private static Fading<Dictionary<Guid, SourceCodeInfo>> fuckingBackup = null;

        public SourceCodeInfo GetSources(Guid methodId)
        {
            Dictionary<Guid, SourceCodeInfo> f = fuckingBackup
                                         ?? (fuckingBackup = new Fading<Dictionary<Guid, SourceCodeInfo>>(
                                             blobStorage.GetLastJsonBlob<Dictionary<Guid, SourceCodeInfo>>(blobName), 60));

            SourceCodeInfo result = null;
            return f?.TryGetValue(methodId, out result) == true
                ? result
                : SourceCodeInfo.Empty;
        }
    }
}