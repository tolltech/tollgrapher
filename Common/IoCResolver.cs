using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Tolltech.Common
{
    public static class IoCResolver
    {
        private static Dictionary<string, AssemblyName> _loadedAssemblies = new Dictionary<string, AssemblyName>();

        public static void Resolve(Action<Type, Type> resolve, params string[] assemblyNames)
        {
            var executingAssembly = Assembly.GetCallingAssembly();
            RegisterReferencedAssemblies(executingAssembly, assemblyNames);

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var filteredAssemblies = assemblies.Where(x => assemblyNames.Any(y => x.FullName.StartsWith($"{y}."))).ToArray();
            var interfaces = filteredAssemblies.SelectMany(x => x.GetTypes().Where(y => y.IsInterface)).ToArray();
            var types = filteredAssemblies.SelectMany(x => x.GetTypes().Where(y => !y.IsInterface && y.IsClass && !y.IsAbstract)).ToArray();
            foreach (var @interface in interfaces)
            {
                var implementations = types.Where(x => @interface.IsAssignableFrom(x)).ToArray();
                foreach (var implementation in implementations)
                {
                    resolve(@interface, implementation);
                }
            }
        }

        private static void RegisterReferencedAssemblies(Assembly assembly, string[] assemblyNames)
        {
            var assemblies = assembly.GetReferencedAssemblies().Where(x => assemblyNames.Any(y => x.FullName.StartsWith($"{y}."))).ToArray();
            foreach (var assemblyName in assemblies)
            {
                if (!_loadedAssemblies.ContainsKey(assemblyName.FullName))
                {
                    _loadedAssemblies.Add(assemblyName.FullName, assemblyName);
                    RegisterReferencedAssemblies(Assembly.Load(assemblyName), assemblyNames);
                }
            }
        }
    }
}
