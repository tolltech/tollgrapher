﻿namespace Tolltech.Common
{
    public class ItemWithChildren<TParent, TChild>
    {
        public TParent Parent { get; set; }
        public TChild[] Children { get; set; }
    }

    public class ItemWithChildren<T> : ItemWithChildren<T,T>
    {
    }
}