﻿using System;

namespace Tolltech.Common
{
    public class Fading<T>
    {
        public T Value { get; }
        private readonly DateTime fadeTime;

        private Fading(T v)
        {
            Value = v;
        }

        public Fading(T value, TimeSpan timeout) : this(value)
        {
            fadeTime = DateTime.UtcNow.Add(timeout);
        }

        public Fading(T value, int minutes) : this(value, TimeSpan.FromMinutes(minutes))
        {

        }

        public static implicit operator T(Fading<T> v)
        {
            return v == null || v.HasFaded ? default(T) : v.Value;
        }

        public bool HasFaded => fadeTime < DateTime.UtcNow;
    }
}