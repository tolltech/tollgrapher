﻿namespace Tolltech.Common
{
    public static class StringExtensions
    {
        public static string JoinToString<T>(this T[] items, string separator = ",")
        {
            return items != null ? string.Join(separator, items) : string.Empty;
        }

        public static string JoinAndLabelsToString<T>(this T[] items, string argName)
        {
            return items.JoinToString($" AND {argName}:");
        }

        public static string JoinOrLabelsToString<T>(this T[] items, string argName)
        {
            return items.JoinToString($" OR {argName}:");
        }

        public static string JoinLabelsForCreateToString<T>(this T[] items)
        {
            return items.JoinToString(":");
        }

        public static string JoinOrLabelsForEdgeToString<T>(this T[] items)
        {
            return items.JoinToString("|");
        }
    }
}