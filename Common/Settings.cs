﻿namespace Tolltech.Common
{
    public class Settings
    {
        public string RootNamespace { get; set; }
        public string Neo4jHost { get; set; }
        public int Neo4jPort { get; set; }
        public string Neo4jPath { get; set; }
        public string Neo4jLogin { get; set; }
        public string Neo4jPass { get; set; }
        public string KonturDriveApiKey { get; set; }
        public int? KonturDriveDayToLive { get; set; }
        public bool TakeGraphFromKonturDrive { get; set; }
        public bool WriteGraphToKonturDrive { get; set; }

        public string SolutionPath { get; set; }
    }
}
