﻿namespace Tolltech.Common.Configuration
{
    public class SettingsBuilder : ISettingsBuilder
    {
        private static readonly Settings defaultSettings = new Settings
        {
            RootNamespace = "SKBKontur",
            Neo4jHost = "localhost",
            Neo4jPort = 7474,
            Neo4jPath = "db/data",
            Neo4jLogin = "neo4j",
            Neo4jPass = "tc_123456",
            KonturDriveApiKey = "",
            TakeGraphFromKonturDrive = false,
            WriteGraphToKonturDrive = false,
            KonturDriveDayToLive = null,
            SolutionPath = @"D:\billy\MegaWithoutCI.sln"
        };

        public Settings Build(string rootNamespace, string solutionPath, string neo4jHost, string konturDriveApiKey, bool takeGraphFromKonturDrive, bool writeGraphToKonturDrive, int? konturDriveDayToLive = null)
        {
            defaultSettings.RootNamespace = rootNamespace ?? defaultSettings.RootNamespace;
            defaultSettings.SolutionPath = solutionPath ?? defaultSettings.SolutionPath;
            defaultSettings.Neo4jHost = neo4jHost ?? defaultSettings.Neo4jHost;
            defaultSettings.KonturDriveApiKey = konturDriveApiKey ?? defaultSettings.KonturDriveApiKey;
            defaultSettings.TakeGraphFromKonturDrive = takeGraphFromKonturDrive;
            defaultSettings.WriteGraphToKonturDrive = writeGraphToKonturDrive;
            defaultSettings.KonturDriveDayToLive = konturDriveDayToLive;

            return defaultSettings;
        }
    }
}
