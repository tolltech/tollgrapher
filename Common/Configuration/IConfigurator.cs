﻿using Ninject;

namespace Tolltech.Common.Configuration
{
    public interface IConfigurator
    {
        IKernel Configure(string[] applicationArguments);
    }
}
