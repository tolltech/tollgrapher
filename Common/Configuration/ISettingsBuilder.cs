﻿namespace Tolltech.Common.Configuration
{
    public interface ISettingsBuilder
    {
        Settings Build(string rootNamespace, string solutionPath, string neo4jHost, string konturDriveApiKey, bool takeGraphFromKonturDrive, bool writeGraphToKonturDrive, int? konturDriveDayToLive = null);
    }
}
