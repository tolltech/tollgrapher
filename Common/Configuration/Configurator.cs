﻿using Ninject;

namespace Tolltech.Common.Configuration
{
    public class Configurator : IConfigurator
    {
        public IKernel Configure(string[] applicationArguments)
        {
            var kernel = new StandardKernel(new ConfigurationModule("log4net.config"));

            ApplySettings(applicationArguments, kernel);

            return kernel;
        }

        private static void ApplySettings(string[] applicationArgument, StandardKernel kernel)
        {
            var solutionPath = applicationArgument.Length > 0 ? applicationArgument[0] : null;
            var neo4jHost = applicationArgument.Length > 1 ? applicationArgument[1] : null;
            var konturDriveApiKey = applicationArgument.Length > 2 ? applicationArgument[2] : null;
            var takeGraphFromKonturDrive = applicationArgument.Length > 3 && bool.Parse(applicationArgument[3]);
            var writeGraphToKonturDrive = applicationArgument.Length > 4 && bool.Parse(applicationArgument[4]);

            var settingsBuilder = kernel.Get<ISettingsBuilder>();

            kernel.Bind<Settings>().ToConstant(settingsBuilder.Build(null, solutionPath, neo4jHost, konturDriveApiKey, takeGraphFromKonturDrive, writeGraphToKonturDrive));
        }
    }
}
