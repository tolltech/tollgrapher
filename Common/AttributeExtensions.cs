﻿using System;
using System.Collections.Concurrent;
using System.Linq;

namespace Tolltech.Common
{
    public static class AttributeExtensions
    {
        private static readonly ConcurrentDictionary<Tuple<Type, Type>, Attribute[]> cachedAttributes = new ConcurrentDictionary<Tuple<Type, Type>, Attribute[]>();

        public static TResult[] GetAttributeValues<TAttribute, TResult>(this Type objType, Func<TAttribute, TResult> getValue) where TAttribute : Attribute
        {
            var attributes = cachedAttributes.GetOrAdd(new Tuple<Type, Type>(objType, typeof(TAttribute)),
                tuple => tuple.Item1.GetCustomAttributes(tuple.Item2, true).OfType<TAttribute>().ToArray());
            return attributes.OfType<TAttribute>().Select(getValue).ToArray();
        }
    }
}
