﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Tolltech.Models;
using Tolltech.TollGrapher.AnalyserHelpers;

namespace Tolltech.TollGrapher.Models
{
    public class ExtractedMethod
    {
        public string Name => $"{Namespace}.{ClassName}.{ShortName}";
        public string ClassName { get; set; }
        public string ShortName { get; set; }
        public Compilation Compilation { get; set; }
        public ParameterSyntax[] Parameters { get; set; }
        public BlockSyntax MethodBody { get; set; }
        public SemanticModel SemanticModel { get; set; }
        public SyntaxTree DocumentSyntaxTree { get; set; }
        public string ServiceName { get; set; }
        public bool IsProperty { get; set; }
        public string Namespace { get; set; }
        public TypeSyntax ReturnType { get; set; }
        public string SourceCode { get; set; }
        public int StartLineNumber { get; set; }
        public int EndLineNumber { get; set; }

        public string Hash => IdGenerator.GetMethodHash(Name, this.GetMethodParametersInfos());
    }
}