﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using log4net;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Tolltech.Models;
using Tolltech.TollGrapher.AnalyserHelpers;
using Tolltech.TollGrapher.Common;

namespace Tolltech.TollGrapher.Models
{
    public class CompiledProjectModel
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CompiledProjectModel));

        private readonly Dictionary<string, Document> documentsByClassSymbolString = new Dictionary<string, Document>();
        private bool documentsWereFilled = false;

        public CompiledProjectModel(CompiledProject[] compiledProjects)
        {
            CompiledProjects = compiledProjects;
            compilationsByProject = CompiledProjects.ToDictionary(x => x.Project, x => x.Compilation);
        }

        public void FillDocuments()
        {
            foreach (var compiledProject in CompiledProjects)
            {
                foreach (var document in compiledProject.Project.Documents)
                {
                    var model = compiledProject.Compilation.GetSemanticModel(document.GetSyntaxTreeAsync().Result);
                    var classes = document.GetSyntaxRootAsync().Result.DescendantNodes().OfType<ClassDeclarationSyntax>();
                    foreach (var classDeclarationSyntax in classes)
                    {
                        var symbol = model.GetDeclaredSymbol(classDeclarationSyntax);
                        documentsByClassSymbolString[symbol.ToString().KillGenericTags()] = document;
                    }
                }
            }

            documentsWereFilled = true;
        }

        public CompiledProject[] CompiledProjects { get; }

        private readonly Dictionary<Project, Compilation> compilationsByProject;
        public Compilation GetCompilationByProject(Project project)
        {
            return compilationsByProject.GetOrAdd(project, x => x.GetCompilationAsync().Result);
        }

        private readonly ConcurrentDictionary<string, CompiledProject[]> compiledProjectCandidatesByServiceName = new ConcurrentDictionary<string, CompiledProject[]>();
        public CompiledProject[] GetCompiledProjectsCandidate(string serviceName)
        {
            return compiledProjectCandidatesByServiceName.GetOrAdd(serviceName,
                key =>
                    CompiledProjects.OrderBy(x => x.Project.Name.Contains(key) ? 0 : 1)
                        .ThenBy(x => x.Project.Name == key ? 0 : 1)
                        .ToArray());
        }

        public Document GetDocumentsCandidate(string classSymbolString)
        {
            if (!documentsWereFilled)
            {
                throw new Exception($"Call {nameof(FillDocuments)} before this method calling");
            }

            Document result;
            return documentsByClassSymbolString.TryGetValue(classSymbolString.KillGenericTags(), out result) ? result : null;
        }

        public ITypeSymbol[] GetHttpHandlerTypeSymbols(string serviceName, string methodName)
        {
            var symbols = new Dictionary<string, ITypeSymbol>();

            var projectsWithService = GetCompiledProjectsCandidate(serviceName);
            foreach (var projectCandidate in projectsWithService)
            {
                foreach (var document in projectCandidate.GetHttpHandlerCandidates())
                {
                    if (!document.SupportsSyntaxTree)
                    {
                        continue;
                    }

                    var methodDeclarationSyntaxs = document.GetSyntaxRootAsync().Result.DescendantNodes()
                        .OfType<MethodDeclarationSyntax>().Where(x => x.Identifier.ValueText == methodName).ToArray();

                    foreach (var methodDeclarationSyntax in methodDeclarationSyntaxs)
                    {
                        var model = projectCandidate.Compilation.GetSemanticModel(document.GetSyntaxTreeAsync().Result);
                        var declaredSymbol = model.GetDeclaredSymbol(methodDeclarationSyntax);
                        if (declaredSymbol.HasAttribute("HttpMethodAttribute"))
                        {
                            var classDeclarationSyntax = methodDeclarationSyntax.FindParent<ClassDeclarationSyntax>();
                            if (classDeclarationSyntax == null)
                            {
                                log.Error($"Cant find class declarationSyntax for {methodName} in {document.FilePath}");
                                $"Cant find class declarationSyntax,{methodName},{document.FilePath}".ToStructuredLogFile();
                                continue;
                            }

                            var clasSymbol = (ITypeSymbol)model.GetDeclaredSymbol(classDeclarationSyntax);

                            if (clasSymbol == null)
                            {
                                log.Error($"Cant get class symbol for {methodName} in {document.FilePath}");
                                $"Cant get class symbol,{methodName},{document.FilePath}".ToStructuredLogFile();
                                continue;
                            }

                            symbols[clasSymbol.ToString()] = clasSymbol;
                        }
                    }
                }
            }
            return symbols.Values.ToArray();
        }

        public ITypeSymbol[] GetTaskRunnerTypeSymbols(ITypeSymbol taskType)
        {
            var symbols = new Dictionary<string, ITypeSymbol>();

            foreach (var projectCandidate in CompiledProjects)
            {
                foreach (var document in projectCandidate.GetTaskRunnerCandidates())
                {
                    if (!document.SupportsSyntaxTree)
                    {
                        continue;
                    }

                    var classDeclarationSyntaxs = document.GetSyntaxRootAsync().Result.DescendantNodes()
                        .OfType<ClassDeclarationSyntax>().ToArray();

                    var model = projectCandidate.Compilation.GetSemanticModel(document.GetSyntaxTreeAsync().Result);
                    foreach (var classDeclarationSyntax in classDeclarationSyntaxs)
                    {
                        var methods = classDeclarationSyntax.DescendantNodes().OfType<MethodDeclarationSyntax>().ToArray();
                        if (methods.All(x => x.Identifier.ValueText != GrapherConstants.TaskRunnerMethodName))
                        {
                            continue;
                        }

                        var declaredSymbol = model.GetDeclaredSymbol(classDeclarationSyntax) as ITypeSymbol;
                        if (!declaredSymbol?.BaseType.IsGenericType ?? true)
                        {
                            continue;
                        }

                        var typeArguments = declaredSymbol.BaseType.TypeArguments;
                        if (typeArguments.Length != 1)
                        {
                            continue;
                        }

                        if (typeArguments[0].ToString() == taskType.ToString())
                        {
                            symbols[declaredSymbol.ToString()] = declaredSymbol;
                        }
                    }
                }
            }
            return symbols.Values.ToArray();
        }

    }
}