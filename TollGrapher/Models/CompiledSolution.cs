﻿using Microsoft.CodeAnalysis;

namespace Tolltech.TollGrapher.Models
{
    public class CompiledSolution
    {
        public CompiledProjectModel CompiledProjects { get; set; }
        public Solution Solution { get; set; }
    }
}