﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace Tolltech.TollGrapher.Models
{
    public class FieldInfo
    {
        public ITypeSymbol FieldType { get; set; }
        public string MethodName { get; set; }
        public InvocationExpressionSyntax FieldMethodInvocationExpression { get; set; }
    }
}