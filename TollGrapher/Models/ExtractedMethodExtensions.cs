﻿using Tolltech.Models;

namespace Tolltech.TollGrapher.Models
{
    public static class ExtractedMethodExtensions
    {
        public static SourceCodeInfo GetSourceCodeInfo(this ExtractedMethod extractedMethod)
        {
            return new SourceCodeInfo
            {
                SourceCode = extractedMethod.SourceCode,
                StartLineNumber = extractedMethod.StartLineNumber,
                EndLineNumber = extractedMethod.EndLineNumber
            };
        }
    }
}