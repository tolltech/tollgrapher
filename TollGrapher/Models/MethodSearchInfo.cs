﻿using Microsoft.CodeAnalysis;

namespace Tolltech.TollGrapher.Models
{
    public class MethodSearchInfo
    {
        public string MethodName { get; set; }

        public ITypeSymbol[] Parameters { get; set; }

        public ITypeSymbol[] ClassSymbols { get; set; }

        public bool IsNetwork { get; set; }
        public bool Async { get; set; }
        public bool IsSetAccessor { get; set; }
    }
}