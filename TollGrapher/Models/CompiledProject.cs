﻿using System.Collections.Concurrent;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Tolltech.TollGrapher.AnalyserHelpers;

namespace Tolltech.TollGrapher.Models
{
    public class CompiledProject
    {
        public CompiledProject(Project project, Compilation compilation)
        {
            Project = project;
            Compilation = compilation;
        }

        public Project Project { get; }
        public Compilation Compilation { get; }

        private enum SpecialClassType
        {
            HttpHandler = 1,
            TaskRunner = 2
        }

        private readonly ConcurrentDictionary<SpecialClassType, Document[]> candidates = new ConcurrentDictionary<SpecialClassType, Document[]>();

        public Document[] GetHttpHandlerCandidates()
        {
            return candidates.GetOrAdd(SpecialClassType.HttpHandler, type => GetDocumentsWithClassesFromBase("HttpHandlerBase"));
        }

        public Document[] GetTaskRunnerCandidates()
        {
            return candidates.GetOrAdd(SpecialClassType.TaskRunner, type => GetDocumentsWithClassesFromBase("TaskRunnerBase"));
        }

        private Document[] GetDocumentsWithClassesFromBase(string baseClassName)
        {
            return Project.Documents.Where(doc =>
            {
                var model = Compilation.GetSemanticModel(doc.GetSyntaxTreeAsync().Result);
                var classes = doc.GetSyntaxRootAsync().Result.DescendantNodes().OfType<ClassDeclarationSyntax>();
                foreach (var classDeclarationSyntax in classes)
                {
                    var symbol = model.GetDeclaredSymbol(classDeclarationSyntax);
                    if (symbol != null && symbol.IsInherited(baseClassName))
                    {
                        return true;
                    }
                }
                return false;
            }).ToArray();
        }
    }
}