﻿using System;
using System.IO;
using log4net;
using Newtonsoft.Json;
using Ninject;
using SourceCodeData;
using Tolltech.BlobStorage;
using Tolltech.Common;
using Tolltech.Common.Configuration;
using Tolltech.Models;
using Tolltech.Neo4jData;
using Tolltech.Neo4jData.Schema;
using Tolltech.TollGrapher.AnalyserHelpers;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapher.Parsing;
using Tolltech.TollGrapher.Walkings;

namespace Tolltech.TollGrapher
{
    public class Program
    {
        private static ILog log;

        public static void Main(string[] args)
        {
            try
            {
                var standardKernel = Configure(args);

                log = LogManager.GetLogger(typeof(Program));

                var settings = standardKernel.Get<Settings>();

                if (!File.Exists(settings.SolutionPath) && !settings.TakeGraphFromKonturDrive)
                {
                    throw new ArgumentException($"Wrong argument for solution path. Path {args[0]} doesn't exist.");
                }

                var settingsSecured = new
                {
                    settings.RootNamespace,
                    Neo4jUri = $"http://{settings.Neo4jHost}:{settings.Neo4jPort}/{settings.Neo4jPath}",
                    settings.TakeGraphFromKonturDrive,
                    settings.WriteGraphToKonturDrive,
                    settings.Neo4jLogin,
                    Neo4jPass = $"{settings.Neo4jPass?.Substring(0, 2)}...",
                    KonturDriveApiKey = string.IsNullOrEmpty(settings.KonturDriveApiKey) ? "" : $"{settings.KonturDriveApiKey?.Substring(0, 5)}...",
                    settings.SolutionPath
                };
                log.ToConsole($"Start grapher with settings {JsonConvert.SerializeObject(settingsSecured)}");

                var grapherHandler = standardKernel.Get<IGrapherHandler>();
                var neo4JNodes = standardKernel.GetAll<INeo4jNode>();
                foreach (var neo4JNode in neo4JNodes)
                {
                    grapherHandler.CreateUniqueConstraints(neo4JNode.GetType(), "Id");
                }

                var solutionAnalyser = standardKernel.Get<ISolutionAnalyser>();
                standardKernel.Get<IBlobStorage>();//need to ortoped load assembly for ninject
                standardKernel.Get<ISourceCodeRepo>();//need to ortoped load assembly for ninject
                var solutionWalker = standardKernel.Get<ISolutionWalker>();

                TreeNode<MethodNode>[] walkings;
                if (!settings.TakeGraphFromKonturDrive)
                {
                    log.ToConsole($"Start analysing graph for {settings.SolutionPath}");

                    walkings = solutionAnalyser.BuildWalkings(settings.SolutionPath);

                    log.ToConsole("Finish analysing graph");

                    if (settings.WriteGraphToKonturDrive)
                    {

                        log.ToConsole($"Start write graph for {settings.SolutionPath} to blobStorage");

                        solutionWalker.WriteWalkingsToBlobStorage(walkings);

                        log.ToConsole($"Finish write graph for {settings.SolutionPath} to blobStorage");
                    }
                    else
                    {
                        log.ToConsole("Configured not to write graph in kontur drive");
                    }
                }
                else
                {
                    walkings = solutionWalker.GetLastWalkingsFromBlobStorage();
                }

                log.ToConsole("Start output walking");

                solutionWalker.WriteWalkingsToNeo4j(walkings);

                if (!settings.TakeGraphFromKonturDrive)
                {
                    solutionWalker.WriteSourcesToBlobStorage(walkings);
                }

                log.ToConsole("Finish output walking");
            }
            catch (Exception ex)
            {
                if (log != null)
                {
                    log.ToConsole($"Something goes wrong {ex.Message} \r\n {ex.StackTrace}");
                    log.Error($"Something goes wrong", ex);
                }

                throw;
            }
        }

        private static IKernel Configure(string[] args)
        {
            return new Configurator().Configure(args);
        }
    }
}

