using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using SourceCodeData;
using Tolltech.BlobStorage;
using Tolltech.Models;
using Tolltech.Neo4jData;
using Tolltech.TollGrapher.AnalyserHelpers;
using Tolltech.TollGrapher.Common;

namespace Tolltech.TollGrapher.Walkings
{
    public class SolutionWalker : ISolutionWalker
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SolutionWalker));
        private readonly IGrapherHandler grapherHandler;
        private readonly IBlobStorage blobStorage;
        private readonly ISourceCodeRepo sourceCodeRepo;

        public SolutionWalker(IGrapherHandler grapherHandler, IBlobStorage blobStorage, ISourceCodeRepo sourceCodeRepo)
        {
            this.grapherHandler = grapherHandler;
            this.blobStorage = blobStorage;
            this.sourceCodeRepo = sourceCodeRepo;
        }

        public void WriteWalkingsToNeo4j(TreeNode<MethodNode>[] trees)
        {
            var total = trees.Length;
            var current = 0;

            grapherHandler.DeleteAll<StorageMethodNode>();
            grapherHandler.DeleteAll<ServiceNode>();

            foreach (var walking in trees)
            {
                ToNeo4j(walking);

                log.ToConsole($"Output walkings {++current}/{total} - {walking.Node.Name}");
            }
        }

        public void WriteSourcesToBlobStorage(TreeNode<MethodNode>[] trees)
        {
            log.ToConsole($"Start output sources");

            var sources = trees.SelectMany(x => x.Flatten()).GroupBy(x => x.Node.Id).ToDictionary(x => x.Key, x => x.First().Node.SourceCode);

            log.ToConsole($"Start output sources to blob Storage");

            sourceCodeRepo.WriteSources(sources);

            log.ToConsole($"Finish output sources to blob Storage");
        }


        private static readonly HashSet<Guid> methodIds = new HashSet<Guid>();
        private static readonly HashSet<Tuple<Guid, Guid>> edgesCache = new HashSet<Tuple<Guid, Guid>>();
        private static readonly HashSet<Guid> servicesIds = new HashSet<Guid>();
        private static readonly HashSet<Tuple<Guid, Guid>> serviceEdgesCache = new HashSet<Tuple<Guid, Guid>>();

        private void ToNeo4j(TreeNode<MethodNode> node, List<MethodNode> parentNodes = null, HashSet<string> parentServices = null)
        {
            var methodNode = node.Node;
            var nodeId = methodNode.Id;

            var serviceNode = new ServiceNode { ServiceName = methodNode.ServiceName };
            if (!methodIds.Contains(nodeId))
            {
                methodIds.Add(nodeId);

                var storageMethodNode = methodNode.ConvertToStorageMethodNode();
                grapherHandler.Create(storageMethodNode, storageMethodNode.GetLabels());

                if (!servicesIds.Contains(serviceNode.Id))
                {
                    grapherHandler.Create(serviceNode);
                    servicesIds.Add(serviceNode.Id);
                }

                grapherHandler.CreateRelation<ServiceNode, StorageMethodNode>(serviceNode.Id, methodNode.Id, GrapherConstants.Contains);
            }


            parentNodes = parentNodes ?? new List<MethodNode>();
            var parent = parentNodes.LastOrDefault();
            if (parent != null)
            {
                var explicitRelation = new Tuple<Guid, Guid>(parent.Id, nodeId);
                if (edgesCache.Contains(explicitRelation))
                {
                    //todo: бывыают циклы
                    return;
                }

                edgesCache.Add(explicitRelation);

                grapherHandler.CreateRelation<StorageMethodNode>(parent.Id, nodeId, parent.NetworkCall && methodNode.NetworkCall ? GrapherConstants.NetworkCall : GrapherConstants.DirectCall);

                var parentServiceNode = new ServiceNode { ServiceName = parent.ServiceName };
                var servicesRelation = new Tuple<Guid, Guid>(parentServiceNode.Id, serviceNode.Id);
                if (parent.ServiceName != methodNode.ServiceName && !serviceEdgesCache.Contains(servicesRelation))
                {
                    grapherHandler.CreateRelation<ServiceNode>(parentServiceNode.Id, serviceNode.Id, GrapherConstants.Use);

                    serviceEdgesCache.Add(servicesRelation);
                }

                //генерация implicit (неявные сетевые вызовы)
                if (methodNode.NetworkCall && !parent.NetworkCall)
                {
                    var parentNodesWithoutLast = parentNodes.Take(parentNodes.Count - 1).Reverse();
                    foreach (var currentParent in parentNodesWithoutLast)
                    {
                        if (currentParent.NetworkCall)
                        {
                            var implicitRelation = new Tuple<Guid, Guid>(currentParent.Id, methodNode.Id);
                            if (!edgesCache.Contains(implicitRelation))
                            {
                                grapherHandler.CreateRelation<StorageMethodNode>(implicitRelation.Item1, implicitRelation.Item2, GrapherConstants.ImplicitCall);

                                edgesCache.Add(implicitRelation);
                            }
                            break;
                        }
                    }
                }
            }

            if (node.Children.Length == 0)
            {
                return;
            }

            parentServices = parentServices ?? new HashSet<string>();

            parentServices.Add(methodNode.ServiceName);
            parentNodes.Add(methodNode);
            foreach (var nodeChild in node.Children)
            {
                ToNeo4j(nodeChild, parentNodes, parentServices);
            }

            parentServices.Remove(methodNode.ServiceName);
            parentNodes.RemoveAt(parentNodes.Count - 1);
        }

        private const string blobNamePrefix = "billinggraph";

        public void WriteWalkingsToBlobStorage(TreeNode<MethodNode>[] trees)
        {
            blobStorage.WriteBlobJsonAndReturnName(trees, $"{blobNamePrefix}/{DateTime.UtcNow:yyyy/MM/dd/hh/mm/ss/}");
        }

        public TreeNode<MethodNode>[] GetLastWalkingsFromBlobStorage()
        {
            return blobStorage.GetLastJsonBlob<TreeNode<MethodNode>[]>(blobNamePrefix);
        }
    }
}