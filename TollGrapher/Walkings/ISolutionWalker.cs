﻿using Tolltech.Models;
using Tolltech.TollGrapher.Common;

namespace Tolltech.TollGrapher.Walkings
{
    public interface ISolutionWalker
    {
        void WriteWalkingsToNeo4j(TreeNode<MethodNode>[] trees);
        void WriteSourcesToBlobStorage(TreeNode<MethodNode>[] trees);
        void WriteWalkingsToBlobStorage(TreeNode<MethodNode>[] trees);
        TreeNode<MethodNode>[] GetLastWalkingsFromBlobStorage();
    }
}