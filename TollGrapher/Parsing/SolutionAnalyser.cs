﻿using System;
using System.Collections.Generic;
using log4net;
using Tolltech.Common;
using Tolltech.Models;
using Tolltech.TollGrapher.AnalyserHelpers;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapher.Models;

namespace Tolltech.TollGrapher.Parsing
{
    public class SolutionAnalyser : ISolutionAnalyser
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SolutionAnalyser));
        private readonly IMethodsExtractor methodsExtractor;
        private readonly IExtractedMethodWalker extractedMethodWalker;
        private readonly ISolutionCompiler solutionCompiler;
        private readonly Settings settings;

        public SolutionAnalyser(IMethodsExtractor methodsExtractor, IExtractedMethodWalker extractedMethodWalker, ISolutionCompiler solutionCompiler, Settings settings)
        {
            this.methodsExtractor = methodsExtractor;
            this.extractedMethodWalker = extractedMethodWalker;
            this.solutionCompiler = solutionCompiler;
            this.settings = settings;
        }

        public TreeNode<MethodNode>[] BuildWalkings(string solutionPath)
        {
            var compiledSolution = solutionCompiler.CompileSolution(solutionPath);
            var entryPointMethods = methodsExtractor.GetEntryPointMethods(compiledSolution.CompiledProjects);

            return BuildWalkings(solutionPath, entryPointMethods, compiledSolution);
        }

        private TreeNode<MethodNode>[] BuildWalkings(string solutionPath, ExtractedMethod[] entryPointMethods, CompiledSolution compiledSolution)
        {
            var total = entryPointMethods.Length;
            var current = 0;

            log.ToConsole($"Start building walking for {solutionPath}");

            var trees = new List<TreeNode<MethodNode>>();
            var errorsCount = 0;
            var methodNodesCache = new Dictionary<string, TreeNode<MethodNode>>();
            foreach (var entryPointMethod in entryPointMethods)
            {
                try
                {
                    log.ToConsole($"Start building graph for {entryPointMethod.Name}");
                    trees.Add(extractedMethodWalker.BuildEntryPointWalking(entryPointMethod, compiledSolution.CompiledProjects, compiledSolution.Solution, settings.RootNamespace, methodNodesCache));
                }
                catch (Exception ex)
                {
                    ++errorsCount;
                    log.Error($"Exception by trying build walkings {entryPointMethod.ServiceName}.{entryPointMethod.Name}", ex);
                    $"Exception by trying build walkings,{entryPointMethod.ServiceName}.{entryPointMethod.Name},{ex.Message},{ex.StackTrace}".ToStructuredLogFile();
                    trees.Add(new TreeNode<MethodNode>
                    {
                        Children = new TreeNode<MethodNode>[0],
                        Node = new MethodNode
                        {
                            Name = entryPointMethod.Name,
                            Namespace = entryPointMethod.Namespace,
                            ClassName = entryPointMethod.ClassName,
                            ShortName = entryPointMethod.ShortName,
                            ServiceName = entryPointMethod.ServiceName,
                            NetworkCall = true,
                            Error = true,
                            EntryPoint = true,
                            IsProperty = entryPointMethod.IsProperty,
                            ReturnType = entryPointMethod.ReturnType.ToString(),
                            Parameters = entryPointMethod.GetMethodParametersInfos(),
                            SourceCode = entryPointMethod.GetSourceCodeInfo(),
                            Async = true
                        }
                    });
                }
                finally
                {
                    log.ToConsole($"Finish building graph for {entryPointMethod.Name}");
                    log.ToConsole($"Building walkings {++current}/{total}");
                }
            }

            log.ToConsole($"Finish building walking for {solutionPath}.Errors - {errorsCount}");
            return trees.ToArray();
        }
    }
}