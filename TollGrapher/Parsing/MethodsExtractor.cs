﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Tolltech.TollGrapher.AnalyserHelpers;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapher.Models;

namespace Tolltech.TollGrapher.Parsing
{
    public class MethodsExtractor : IMethodsExtractor
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(MethodsExtractor));

        public ExtractedMethod[] GetEntryPointMethods(CompiledProjectModel compiledProjects)
        {
            var list = new List<ExtractedMethod>();
            var projectCount = compiledProjects.CompiledProjects.Length;
            var projectIterator = 0;

            foreach (var compiledProject in compiledProjects.CompiledProjects)
            {
                log.ToConsole($"Start extractingEntryPoints for project {compiledProject.Project.Name}");

                var projectDocuments = compiledProject.Project.Documents.ToArray();

                var projectDocumentsCount = projectDocuments.Length;
                var projectDocumentIterator = 0;
                log.ToConsole($"Project has {projectDocumentsCount} documents");

                foreach (var document in projectDocuments)
                {
                    list.AddRange(GetSuitableMethods(compiledProject.Compilation, document,
                            classSymbol => classSymbol.IsInherited("IRegularProcess") || classSymbol.IsInherited("Controller") || classSymbol.IsInherited("IEventProcessor"),
                            MemberType.Method));

                    log.ToConsole($"Processed {++projectDocumentIterator}/{projectDocumentsCount}");
                }

                log.ToConsole($"Start extractingEntryPoints for project {compiledProject.Project.Name}");
                log.ToConsole($"EntryPoints project progress is {++projectIterator}/{projectCount}");
            }
            return list.ToArray();
        }

        [Obsolete("Not for Production using. It cant search documents with base types")]
        public ExtractedMethod ExtractMethod(CompiledProjectModel compiledProjects, string classSymbolString, string methodName, ITypeSymbol[] parameters)
        {
            return ExtractMethod(compiledProjects,x => x.ToString() == classSymbolString.ToString(), null, classSymbolString, methodName, parameters);
        }

        public ExtractedMethod ExtractMethod(CompiledProjectModel compiledProjects, ITypeSymbol classSymbol, string methodName, ITypeSymbol[] parameters)
        {
            return ExtractMethod(compiledProjects, classSymbol.IsInherited, classSymbol, classSymbol.ToString(), methodName, parameters);
        }

        private ExtractedMethod ExtractMethod(CompiledProjectModel compiledProjects, Func<ITypeSymbol, bool> classSymbolFilter, ITypeSymbol classSymbol, string classSymbolString, string methodName, ITypeSymbol[] parameters)
        {
            var documents =
                (classSymbol == null
                    ? new[] {compiledProjects.GetDocumentsCandidate(classSymbolString)}
                    : new[] {classSymbol}.Concat(classSymbol.GetBaseClasses())
                        .Select(x => compiledProjects.GetDocumentsCandidate(x.ToString())))
                .Where(x => x != null)
                .ToArray();

            if (!documents.Any())
            {
                return null;
            }

            var suitableMethods = new List<ExtractedMethod>();

            foreach (var document in documents)
            {
                var compilation = compiledProjects.GetCompilationByProject(document.Project);
                suitableMethods.AddRange(GetSuitableMethods(compilation, document, classSymbolFilter, null, methodName));
            }

            if (suitableMethods.Count == 0)
            {
                return null;
            }

            var suitableMethod = suitableMethods.First();
            if (suitableMethods.Count > 1)
            {
                var candidates = suitableMethods.Select(x =>
                    {
                        bool? identity;
                        var suitable = x.ParametersAreSuitable(parameters, out identity);
                        return new { Suitable = suitable, Identity = identity, Candidate = x };
                    })
                    .Where(x => x.Suitable)
                    .OrderBy(x => x.Identity.HasValue && x.Identity.Value ? 0 : 1)
                    .Select(x => x.Candidate)
                    .ToArray();


                var filePaths = string.Join(",", documents.Select(x => x.FilePath));
                if (candidates.Length == 0)
                {
                    log.Error($"Find 0 methods {methodName} in document {filePaths} with parameters {string.Join(",", parameters.Select(x => x?.ToString() ?? "null"))}");
                    $"Find 0 methods,{methodName},{filePaths},{string.Join(";", parameters.Select(x => x?.ToString() ?? "null"))}".ToStructuredLogFile();
                }

                if (candidates.Length > 1)
                {
                    log.Error($"Find more than 1 method {methodName} in document {filePaths} with parameters {string.Join(",", parameters.Select(x => x?.ToString() ?? "null"))}");
                    $"Find more than 1 method,{methodName},{filePaths},{string.Join(";", parameters.Select(x => x?.ToString() ?? "null"))}".ToStructuredLogFile();
                }

                if (candidates.Length >= 1)
                {
                    suitableMethod = candidates.First();
                }
            }

            return suitableMethod;
        }

        private ExtractedMethod[] GetSuitableMethods(Compilation compilation, Document document, Func<ITypeSymbol, bool> classSymbolFilter, MemberType? memberType, string methodName = null)
        {
            if (!document.SupportsSyntaxTree)
            {
                return new ExtractedMethod[0];
            }

            var documentSyntaxTree = document.GetSyntaxTreeAsync().Result;

            var documentRoot = documentSyntaxTree.GetRoot();
            var sourceText = documentRoot.GetText();
            var classDeclarationSyntaxes = documentRoot.DescendantNodes().OfType<ClassDeclarationSyntax>();
            var semanticModel = compilation.GetSemanticModel(documentSyntaxTree);

            var result = new List<ExtractedMethod>();
            foreach (var classDeclarationSyntax in classDeclarationSyntaxes)
            {
                var classDeclarationSemanticModel = (ITypeSymbol)semanticModel.GetDeclaredSymbol(classDeclarationSyntax);

                if (!classSymbolFilter(classDeclarationSemanticModel))
                {
                    continue;
                }

                var methodDesclarationSyntaxes = classDeclarationSyntax.DescendantNodes().OfType<MethodDeclarationSyntax>();
                var methods = methodDesclarationSyntaxes.Select(x => new
                {
                    Name = semanticModel.GetDeclaredSymbol(x).Name,
                    Parameters = x.ParameterList.Parameters.Select(y => y).ToArray(),
                    MethodBody = x.Body,
                    MemberType = MemberType.Method,
                    ReturnType = x.ReturnType,
                    Span = x.Span
                }).ToArray();

                var propertyDesclarationSyntaxes = classDeclarationSyntax.DescendantNodes().OfType<PropertyDeclarationSyntax>();
                var properties = propertyDesclarationSyntaxes.Select(x => new
                {
                    Name = semanticModel.GetDeclaredSymbol(x).Name,
                    Parameters = new ParameterSyntax[0],
                    MethodBody = x.AccessorList?.Accessors.FirstOrDefault(y => y.Keyword.ValueText == "get")?.Body,
                    MemberType = MemberType.Property,
                    ReturnType = x.Type,
                    Span = x.Span
                }).ToArray();

                var fieldDesclarationSyntaxes = classDeclarationSyntax.DescendantNodes().OfType<FieldDeclarationSyntax>().ToArray();
                var strangeFieldDeclarations = fieldDesclarationSyntaxes.Where(x => !x.Declaration.Variables.Any() || x.Declaration.Variables.Count > 1).ToArray();
                if (strangeFieldDeclarations.Any())
                {
                    log.Error($"Strange FieldDeclarations without variables {string.Join(", ", strangeFieldDeclarations.Select(x => x))}; {document.FilePath} {classDeclarationSyntax.Identifier.ValueText}");
                }

                var fields = fieldDesclarationSyntaxes.Except(strangeFieldDeclarations).Select(x => new
                {
                    Name = x.Declaration.Variables.First().Identifier.ValueText,
                    Parameters = new ParameterSyntax[0],
                    MethodBody = (BlockSyntax) null,
                    MemberType = MemberType.Field,
                    ReturnType = x.Declaration.Type,
                    Span = x.Span
                }).ToArray();

                var suitableMembers = methods.Concat(properties).Concat(fields)
                    .Where(x => string.IsNullOrWhiteSpace(methodName) || x.Name == methodName)
                    .Where(x => !memberType.HasValue || x.MemberType == memberType.Value)
                    .ToArray();

                if (suitableMembers.Length == 0)
                {
                    continue;
                }

                result.AddRange(suitableMembers
                    .Select(x => new ExtractedMethod
                    {
                        Namespace = classDeclarationSemanticModel.ContainingNamespace.ToString(),
                        ClassName = classDeclarationSemanticModel.Name,
                        ShortName = x.Name,
                        Compilation = compilation,
                        Parameters = x.Parameters,
                        MethodBody = x.MethodBody,
                        SemanticModel = semanticModel,
                        DocumentSyntaxTree = documentSyntaxTree,
                        ServiceName = classDeclarationSemanticModel.ContainingAssembly.Identity.Name.GetPostfix(),
                        IsProperty = x.MemberType == MemberType.Property || x.MemberType == MemberType.Field,
                        ReturnType = x.ReturnType,
                        SourceCode = x.MethodBody?.Parent?.ToFullString() ?? "...",
                        StartLineNumber = sourceText.Lines.IndexOf(x.Span.Start) + 1,
                        EndLineNumber = sourceText.Lines.IndexOf(x.Span.End) + 1
                    }));
            }

            return result.ToArray();
        }

        private enum MemberType
        {
            Method,
            Property,
            Field
        }
    }
}