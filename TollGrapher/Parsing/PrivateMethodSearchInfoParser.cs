using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Tolltech.TollGrapher.AnalyserHelpers;
using Tolltech.TollGrapher.Models;

namespace Tolltech.TollGrapher.Parsing
{
    public class PrivateMethodSearchInfoParser : IMethodSearchInfoParser
    {
        public int Priority => 99;

        public MethodSearchInfo GetMethodSearchInfo(SyntaxNode syntaxNode, ExtractedMethod extractedMethod, string rootNamespaceName, Solution solution, CompiledProjectModel compiledProjects)
        {
            var invocationExpressionSyntax = syntaxNode as InvocationExpressionSyntax;
            if (invocationExpressionSyntax == null)
            {
                return null;
            }

            //it is true only for privateMethods
            var identifierNameSyntax = invocationExpressionSyntax.Expression as IdentifierNameSyntax;
            if (identifierNameSyntax != null)
            {
                var classDeclarationSyntax = invocationExpressionSyntax.FindParent<ClassDeclarationSyntax>();
                if (classDeclarationSyntax != null)
                {
                    var classSymbol = extractedMethod.SemanticModel.GetDeclaredSymbol(classDeclarationSyntax) as INamedTypeSymbol;
                    if (classSymbol != null)
                    {
                        return new MethodSearchInfo
                        {
                            MethodName = identifierNameSyntax.Identifier.ValueText,
                            ClassSymbols = new[] { classSymbol },
                            IsNetwork = false,
                            Parameters = invocationExpressionSyntax.ArgumentList.Arguments
                                .Select(x => extractedMethod.GetExpressionSymbol(x.Expression))
                                .ToArray(),
                            Async = false,
                            IsSetAccessor = false
                        };
                    }
                }
            }

            return null;
        }
    }
}