﻿using System.Collections.Generic;
using System.Linq;
using log4net;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Tolltech.Models;
using Tolltech.TollGrapher.AnalyserHelpers;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapher.Models;

namespace Tolltech.TollGrapher.Parsing
{
    public class TaskMethodSearchInfoParser : IMethodSearchInfoParser
    {
        public int Priority => 5;

        private static readonly ILog log = LogManager.GetLogger(typeof(TaskMethodSearchInfoParser));

        private static readonly HashSet<string> taskClientNames = new HashSet<string> { "ITaskQueueClient", "ITaskRegistrator" };

        public MethodSearchInfo GetMethodSearchInfo(SyntaxNode syntaxNode, ExtractedMethod extractedMethod, string rootNamespaceName,
            Solution solution, CompiledProjectModel compiledProjects)
        {
            var fieldInfo = SyntaxHelpers.GetInvocationOfFieldInfo(syntaxNode, extractedMethod);

            if (fieldInfo == null)
            {
                return null;
            }

            var fieldType = fieldInfo.FieldType;

            if (!taskClientNames.Contains(fieldType.Name))
            {
                return null;
            }

            var argumentList = fieldInfo.FieldMethodInvocationExpression.ArgumentList;

            if (argumentList.Arguments.Count < 1)
            {
                return null;
            }

            var taskQueueArgumentCallExpression = argumentList.Arguments[0].Expression;
            var taskType = GetTaskType(extractedMethod, taskQueueArgumentCallExpression);

            if (taskType == null)
            {
                log.Error($"Cant determine taskType {extractedMethod.Name}");
                $"Cant determine taskType ,{extractedMethod.Name}, {taskQueueArgumentCallExpression}".ToStructuredLogFile();
                return null;
            }

            var symbols = compiledProjects.GetTaskRunnerTypeSymbols(taskType);

            if (symbols.Length == 0)
            {
                log.Error($"found 0 task Runners classes for {taskType}");
                $"found 0 task Runners classes,{taskType}".ToStructuredLogFile();
                return null;
            }

            if (symbols.Length > 1)
            {
                log.Error($"found several task runner classes({symbols.Length}) for {taskType}. {string.Join(",", symbols.Select(x => x.ToString()))}");
                $"found several task runner classes,{symbols.Length},{taskType},{string.Join(";", symbols.Select(x => x.ToString()))}".ToStructuredLogFile();
            }

            return new MethodSearchInfo
            {
                MethodName = GrapherConstants.TaskRunnerMethodName,
                IsNetwork = true,
                Parameters = new ITypeSymbol[0],
                ClassSymbols = new[] { symbols.First() },
                Async = true,
                IsSetAccessor = false
            };
        }

        private static ITypeSymbol GetTaskType(ExtractedMethod extractedMethod, ExpressionSyntax taskExpression)
        {
            ITypeSymbol taskType;

            var argumentType = extractedMethod.GetExpressionSymbol(taskExpression);
            var taskArrayType = argumentType as IArrayTypeSymbol;
            if (taskArrayType != null)
            {
                taskType = taskArrayType.ElementType;
            }
            else
            {
                taskType = argumentType;
            }

            if (taskType == null || !taskType.IsInherited("IPersistentTask") && !taskType.IsInherited("IEchelonTask"))
            {
                return null;
            }

            return taskType;
        }
    }
}