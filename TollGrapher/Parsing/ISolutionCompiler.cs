﻿using Tolltech.TollGrapher.Models;

namespace Tolltech.TollGrapher.Parsing
{
    public interface ISolutionCompiler
    {
        CompiledSolution CompileSolution(string solutionPath);
    }
}