﻿using System.Collections.Generic;
using System.Linq;
using log4net;
using Microsoft.Build.Locator;
using Microsoft.CodeAnalysis.MSBuild;
using Tolltech.TollGrapher.AnalyserHelpers;
using Tolltech.TollGrapher.Models;

namespace Tolltech.TollGrapher.Parsing
{
    public class SolutionCompiler : ISolutionCompiler
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SolutionCompiler));

        private static bool MsBuildWasLoaded = false;
        private static readonly object locker = new object();
        private static void LoadMsBuildAssemblies()
        {
            if (!MsBuildWasLoaded)
            {
                lock (locker)
                {
                    if (!MsBuildWasLoaded)
                    {
                        MSBuildLocator.RegisterDefaults();
                        MsBuildWasLoaded = true;
                    }
                }
            }
        }

        public CompiledSolution CompileSolution(string solutionPath)
        {
            LoadMsBuildAssemblies();

            var msWorkspace = MSBuildWorkspace.Create();

            var solution = msWorkspace.OpenSolutionAsync(solutionPath).Result;

            var compiledProjectsList = new List<CompiledProject>();
            var solutionProjects = solution.Projects.ToArray();

            log.ToConsole($"Start compiling projects for {solutionPath}");

            var total = solutionProjects.Length;
            var iterator = 0;

            foreach (var project in solutionProjects)
            {
                log.ToConsole($"Start compiling project {project.Name}");

                if (project.SupportsCompilation)
                {

                    var compilation = project.GetCompilationAsync().Result;

                    compiledProjectsList.Add(new CompiledProject(project, compilation));
                }

                log.ToConsole($"Finish compiling project {project.Name}");
                log.ToConsole($"Compiling projects {++iterator}/{total}");
            }

            log.ToConsole($"Finish compiling projects for {solutionPath}");

            var compiledProjectModel = new CompiledProjectModel(compiledProjectsList.ToArray());
            compiledProjectModel.FillDocuments();

            return new CompiledSolution
            {
                CompiledProjects = compiledProjectModel,
                Solution = solution
            };
        }
    }
}