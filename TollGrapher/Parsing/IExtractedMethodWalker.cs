﻿using System.Collections.Generic;
using Microsoft.CodeAnalysis;
using Tolltech.Models;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapher.Models;

namespace Tolltech.TollGrapher.Parsing
{
    public interface IExtractedMethodWalker
    {
        TreeNode<MethodNode> BuildEntryPointWalking(ExtractedMethod extractedMethod, CompiledProjectModel projects, Solution solution, string rootNamespaceName, Dictionary<string, TreeNode<MethodNode>> methodNodesCache = null);
    }
}