using System.Linq;
using log4net;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.FindSymbols;
using Tolltech.TollGrapher.AnalyserHelpers;
using Tolltech.TollGrapher.Models;

namespace Tolltech.TollGrapher.Parsing
{
    public class PropertySearchInfoParser : IMethodSearchInfoParser
    {
        public int Priority => 98;

        private static readonly ILog log = LogManager.GetLogger(typeof(PropertySearchInfoParser));

        public MethodSearchInfo GetMethodSearchInfo(SyntaxNode syntaxNode, ExtractedMethod extractedMethod, string rootNamespaceName, Solution solution, CompiledProjectModel compiledProjects)
        {
            //it is true if it's method invocation, not for property
            if (syntaxNode.Parent is InvocationExpressionSyntax)
            {
                return null;
            }

            PropertyCallInfo propertyCallInfo;
            if (syntaxNode is MemberAccessExpressionSyntax)
            {
                var memberAccessExpressiontSyntax = (MemberAccessExpressionSyntax)syntaxNode;
                var propertyName = memberAccessExpressiontSyntax.Name.Identifier.ValueText;


                propertyCallInfo = new PropertyCallInfo
                {
                    PropertyName = propertyName,
                    EntityExpression = memberAccessExpressiontSyntax.Expression,
                    IsSetAccessor = (syntaxNode.Parent as AssignmentExpressionSyntax)?.Left == memberAccessExpressiontSyntax
                };
            }
            else if (syntaxNode is ConditionalAccessExpressionSyntax)
            {
                var conditionalAccessExpressionSyntax = (ConditionalAccessExpressionSyntax)syntaxNode;
                var propertyName = (conditionalAccessExpressionSyntax.WhenNotNull as MemberBindingExpressionSyntax)
                    ?.Name.Identifier.ValueText;

                propertyCallInfo = new PropertyCallInfo
                {
                    PropertyName = propertyName,
                    EntityExpression = conditionalAccessExpressionSyntax.Expression,
                    IsSetAccessor = false
                };
            }
            else if (syntaxNode is AssignmentExpressionSyntax)
            {
                var assignmentExpressionSyntax = (AssignmentExpressionSyntax)syntaxNode;

                var propertyName = (assignmentExpressionSyntax.Left as IdentifierNameSyntax)?.Identifier.ValueText;
                if (propertyName == null)
                {
                    return null;
                }

                var objectCreationExpressionSyntax =
                ((assignmentExpressionSyntax.Parent as InitializerExpressionSyntax)?
                    .Parent as ObjectCreationExpressionSyntax);

                if (objectCreationExpressionSyntax == null)
                {
                    return null;
                }

                propertyCallInfo = new PropertyCallInfo
                {
                    PropertyName = propertyName,
                    EntityExpression = objectCreationExpressionSyntax,
                    IsSetAccessor = true
                };

            }
            else
            {
                return null;
            }

            var entityExpression = propertyCallInfo.EntityExpression;

            //for checking if namespace
            var symbolInfo = extractedMethod.GetTypeSymbolInfo(entityExpression);
            if (symbolInfo?.IsNamespace ?? false)
            {
                return null;
            }

            var entityType = extractedMethod.GetExpressionSymbol(entityExpression);

            if (entityType == null)
            {
                log.Error($"Cant find entity for property {propertyCallInfo.PropertyName} {propertyCallInfo.EntityExpression} {extractedMethod.ClassName}");
                return null;
            }

            if (!entityType.IsInteresting(rootNamespaceName))
            {
                return null;
            }

            var entityTypes = entityType.TypeKind == TypeKind.Interface || entityType.IsAbstract
                ? SymbolFinder.FindImplementationsAsync(entityType, solution).Result.Cast<ITypeSymbol>().ToArray()
                : new[] { entityType };

            return new MethodSearchInfo
            {
                MethodName = propertyCallInfo.PropertyName,
                ClassSymbols = entityTypes,
                IsNetwork = false,
                Parameters = new ITypeSymbol[0],
                Async = false,
                IsSetAccessor = propertyCallInfo.IsSetAccessor
            };
        }

        private class PropertyCallInfo
        {
            public string PropertyName { get; set; }
            public ExpressionSyntax EntityExpression { get; set; }
            public bool IsSetAccessor { get; set; }
        }
    }
}