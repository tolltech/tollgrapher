﻿using Tolltech.Models;
using Tolltech.TollGrapher.Common;

namespace Tolltech.TollGrapher.Parsing
{
    public interface ISolutionAnalyser
    {
        TreeNode<MethodNode>[] BuildWalkings(string solutionPath);
    }
}