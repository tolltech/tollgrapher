﻿using Microsoft.CodeAnalysis;
using Tolltech.TollGrapher.Models;

namespace Tolltech.TollGrapher.Parsing
{
    public interface IMethodSearchInfoParser
    {
        int Priority { get; }
        MethodSearchInfo GetMethodSearchInfo(SyntaxNode syntaxNode, ExtractedMethod extractedMethod, string rootNamespaceName, Solution solution, CompiledProjectModel compiledProjects);
    }
}