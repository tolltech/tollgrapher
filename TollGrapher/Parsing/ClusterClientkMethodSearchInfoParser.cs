﻿using System.Linq;
using log4net;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Tolltech.TollGrapher.AnalyserHelpers;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapher.Models;

namespace Tolltech.TollGrapher.Parsing
{
    public class ClusterClientkMethodSearchInfoParser : IMethodSearchInfoParser
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ClusterClientkMethodSearchInfoParser));
        public int Priority => 2;

        private static readonly string[] supportedRequestMethodNames = new[] { "Post", "Get", "Trace", "Patch", "Options", "Head", "Put" };

        public MethodSearchInfo GetMethodSearchInfo(SyntaxNode syntaxNode, ExtractedMethod extractedMethod, string rootNamespaceName, Solution solution, CompiledProjectModel compiledProjects)
        {
            var fieldInfo = SyntaxHelpers.GetInvocationOfFieldInfo(syntaxNode, extractedMethod);

            if (fieldInfo == null)
            {
                return null;
            }

            var fieldType = fieldInfo.FieldType;

            if (fieldType.Name != "IHttpClient")
            {
                return null;
            }

            var methodName = GetHttpHandlerMethodName(extractedMethod, fieldInfo);
            if (string.IsNullOrEmpty(methodName))
            {
                log.Error($"Cant determine clusterClient MethodName {extractedMethod.Name}");
                $"Cant determine clusterClient MethodName,{extractedMethod.Name}".ToStructuredLogFile();
                return null;
            }

            var serviceName = GetHttpHandlerServiceName(extractedMethod, fieldInfo);
            if (string.IsNullOrEmpty(serviceName))
            {
                log.Error($"Cant determine clusterClient ServiceName {extractedMethod.Name}");
                $"Cant determine clusterClient ServiceName,{extractedMethod.Name},{methodName}".ToStructuredLogFile();
                return null;
            }

            var symbols = compiledProjects.GetHttpHandlerTypeSymbols(serviceName, methodName);

            if (symbols.Length == 0)
            {
                log.Error($"found 0 network classes for {methodName} of {serviceName}");
                $"found 0 network classes,{methodName},{serviceName}".ToStructuredLogFile();
                return null;
            }

            if (symbols.Length > 1)
            {
                //log.Error($"found several network classes({symbols.Count}) for {methodName} of {serviceName}. {string.Join(",", symbols.Select(x=>x.Key.ToString()))}");
            }

            return new MethodSearchInfo
            {
                MethodName = methodName,
                IsNetwork = true,
                Parameters = new ITypeSymbol[0],
                ClassSymbols = new[] { symbols.First() },
                Async = false,
                IsSetAccessor = false
            };
        }

        private static string GetHttpHandlerServiceName(ExtractedMethod extractedMethod, FieldInfo fieldInfo)
        {
            var httpClientFieldName = ((fieldInfo.FieldMethodInvocationExpression.Expression as MemberAccessExpressionSyntax)?.Expression as IdentifierNameSyntax)?.Identifier.ValueText;
            if (string.IsNullOrEmpty(httpClientFieldName))
            {
                return null;
            }

            var clientCreateAssignmentExpression = extractedMethod.GetFirstAssignExpression(httpClientFieldName);

            if (clientCreateAssignmentExpression == null)
            {
                return null;
            }

            var clientProviderInvocationExpressionSyntax = clientCreateAssignmentExpression.Right as InvocationExpressionSyntax;
            var clientProviderFieldMemberAccess = clientProviderInvocationExpressionSyntax?.Expression as MemberAccessExpressionSyntax;

            if (clientProviderFieldMemberAccess == null)
            {
                return null;
            }

            var clientProviderFieldName = clientProviderFieldMemberAccess.Name.Identifier.ValueText;
            var clientProviderType = extractedMethod.GetExpressionSymbol(clientProviderFieldMemberAccess.Expression, clientProviderFieldName);
            if (clientProviderType == null)
            {
                return null;
            }

            var clientProviderTypeName = clientProviderType.Name;
            var clientProviderMethodName = clientProviderFieldMemberAccess.Name.Identifier.ValueText;
            if (!(clientProviderTypeName == "IHttpClientProvider" && clientProviderMethodName == "Get") &&
                !(clientProviderTypeName == "IHttpClientFactory" && clientProviderMethodName == "Create"))
            {
                return null;
            }

            var clientFactorArgumentList = clientProviderInvocationExpressionSyntax.ArgumentList.Arguments;
            if (clientFactorArgumentList.Count < 1)
            {
                return null;
            }

            var clientFactorArgumentExpression = clientFactorArgumentList[0].Expression;
            return extractedMethod.GetValueExpression<LiteralExpressionSyntax>(clientFactorArgumentExpression)?.Token.ValueText;
        }

        private static string GetHttpHandlerMethodName(ExtractedMethod extractedMethod, FieldInfo fieldInfo)
        {
            var argumentList = fieldInfo.FieldMethodInvocationExpression.ArgumentList;

            if (argumentList.Arguments.Count < 1)
            {
                return null;
            }

            var requestExpression = argumentList.Arguments[0].Expression;
            var requestUriExpressionSyntax = extractedMethod.GetValueExpression<InvocationExpressionSyntax>(requestExpression).Expression as MemberAccessExpressionSyntax;

            if (requestUriExpressionSyntax == null)
            {
                return null;
            }

            MemberAccessExpressionSyntax requestMemberAccessSyntax = null;
            CSharpSyntaxNode currentNode = requestUriExpressionSyntax;
            while (currentNode != null)
            {
                if (currentNode is MemberAccessExpressionSyntax)
                {
                    var memberAccessExpressionSyntax = (MemberAccessExpressionSyntax)currentNode;
                    var childExpression = memberAccessExpressionSyntax.Expression;

                    if (childExpression == null)
                    {
                        return null;
                    }

                    if (!(childExpression is IdentifierNameSyntax))
                    {
                        currentNode = childExpression;
                        continue;
                    }
                    else
                    {
                        requestMemberAccessSyntax = memberAccessExpressionSyntax;
                        break;
                    }
                }
                else if (currentNode is InvocationExpressionSyntax)
                {
                    currentNode = ((InvocationExpressionSyntax)currentNode).Expression;
                }
                else
                {
                    currentNode = null;
                }
            }

            if (requestMemberAccessSyntax == null)
            {
                return null;
            }

            var className = ((IdentifierNameSyntax)requestMemberAccessSyntax.Expression).Identifier.ValueText;

            if (className != "Request")
            {
                return null;
            }

            var requestMethodName = requestMemberAccessSyntax.Name.Identifier.ValueText;
            if (!supportedRequestMethodNames.Contains(requestMethodName))
            {
                return null;
            }

            var requestInvocationSyntax = requestMemberAccessSyntax.Parent as InvocationExpressionSyntax;
            if (requestInvocationSyntax == null)
            {
                return null;
            }

            var requetCallArgumentsList = requestInvocationSyntax.ArgumentList;

            if (requetCallArgumentsList.Arguments.Count < 1)
            {
                return null;
            }

            var firstRequestCallArgumentExpression = requetCallArgumentsList.Arguments[0].Expression;
            return extractedMethod.GetValueExpression<LiteralExpressionSyntax>(firstRequestCallArgumentExpression)?.Token.ValueText;
        }
    }
}