﻿using System.Linq;
using log4net;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Tolltech.TollGrapher.AnalyserHelpers;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapher.Models;

namespace Tolltech.TollGrapher.Parsing
{
    public class NetworkMethodSearchInfoParser : IMethodSearchInfoParser
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(NetworkMethodSearchInfoParser));
        public int Priority => 1;

        public MethodSearchInfo GetMethodSearchInfo(SyntaxNode syntaxNode, ExtractedMethod extractedMethod, string rootNamespaceName, Solution solution, CompiledProjectModel compiledProjects)
        {
            var fieldInfo = SyntaxHelpers.GetInvocationOfFieldInfo(syntaxNode, extractedMethod);

            if (fieldInfo == null)
            {
                return null;
            }

            var fieldType = fieldInfo.FieldType;

            if (fieldType.Name != "IDomain")
            {
                return null;
            }

            var argumentList = fieldInfo.FieldMethodInvocationExpression.ArgumentList;

            if (argumentList.Arguments.Count < 2)
            {
                return null;
            }

            var firstDomainCallArgumentExpression = argumentList.Arguments[0].Expression;
            var serviceName = extractedMethod.GetValueExpression<LiteralExpressionSyntax>(firstDomainCallArgumentExpression)?.Token.ValueText;

            if (string.IsNullOrEmpty(serviceName))
            {
                return null;
            }

            var commandExpression = argumentList.Arguments[1].Expression;
            var secondDomainCallArgumentExpression = extractedMethod.GetValueExpression<InvocationExpressionSyntax>(commandExpression);

            if (secondDomainCallArgumentExpression?.ArgumentList?.Arguments == null)
            {
                log.Error($"Cant Get Arguments for {extractedMethod.Name} in {fieldInfo.MethodName}");
                $"Cant Get Arguments,{extractedMethod.Name},{fieldInfo.MethodName}".ToStructuredLogFile();
                return null;
            }

            if (secondDomainCallArgumentExpression.ArgumentList.Arguments.Count == 0)
            {
                return null;
            }

            var commandCreateFirstArgumentExpression = secondDomainCallArgumentExpression.ArgumentList.Arguments[0].Expression;
            var methodName = extractedMethod.GetValueExpression<LiteralExpressionSyntax>(commandCreateFirstArgumentExpression)?.Token.ValueText;

            if (string.IsNullOrEmpty(methodName))
            {
                return null;
            }

            var symbols = compiledProjects.GetHttpHandlerTypeSymbols(serviceName, methodName);

            if (symbols.Length == 0)
            {
                log.Error($"found 0 network classes for {methodName} of {serviceName}");
                $"found 0 network classes,{methodName},{serviceName}".ToStructuredLogFile();
                return null;
            }

            if (symbols.Length > 1)
            {
                //log.Error($"found several network classes({symbols.Count}) for {methodName} of {serviceName}. {string.Join(",", symbols.Select(x=>x.Key.ToString()))}");
            }

            return new MethodSearchInfo
            {
                MethodName = methodName,
                IsNetwork = true,
                Parameters = new ITypeSymbol[0],
                ClassSymbols = new[] { symbols.First() },
                Async = false,
                IsSetAccessor = false
            };
        }
    }
}