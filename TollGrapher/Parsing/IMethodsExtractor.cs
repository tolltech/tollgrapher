﻿using Microsoft.CodeAnalysis;
using Tolltech.TollGrapher.Models;

namespace Tolltech.TollGrapher.Parsing
{
    public interface IMethodsExtractor
    {
        ExtractedMethod[] GetEntryPointMethods(CompiledProjectModel compiledProjects);
        ExtractedMethod ExtractMethod(CompiledProjectModel compiledProjects, string classSymbolString, string methodName, ITypeSymbol[] parameters);
        ExtractedMethod ExtractMethod(CompiledProjectModel compiledProjects, ITypeSymbol classSymbol, string methodName, ITypeSymbol[] parameters);
    }
}
