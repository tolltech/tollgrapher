﻿using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.FindSymbols;
using Tolltech.TollGrapher.AnalyserHelpers;
using Tolltech.TollGrapher.Models;

namespace Tolltech.TollGrapher.Parsing
{
    public class DefaultMethodSearchInfoParser : IMethodSearchInfoParser
    {
        public int Priority => 100;

        public MethodSearchInfo GetMethodSearchInfo(SyntaxNode syntaxNode, ExtractedMethod extractedMethod, string rootNamespaceName, Solution solution, CompiledProjectModel compiledProjects)
        {
            var fieldInfo = SyntaxHelpers.GetInvocationOfFieldInfo(syntaxNode, extractedMethod);

            if (fieldInfo == null)
            {
                return null;
            }

            var fieldType = fieldInfo.FieldType;
            if (!fieldType.IsInteresting(rootNamespaceName))
            {
                return null;
            }

            var fieldImplementations = fieldType.TypeKind == TypeKind.Interface || fieldType.IsAbstract
                ? SymbolFinder.FindImplementationsAsync(fieldType, solution).Result.Cast<ITypeSymbol>().ToArray()
                : new[] {fieldType};

            var parameters = fieldInfo.FieldMethodInvocationExpression.ArgumentList.Arguments
                .Select(x => extractedMethod.GetExpressionSymbol(x.Expression))
                .ToArray();

            return new MethodSearchInfo
            {
                ClassSymbols = fieldImplementations,
                MethodName = fieldInfo.MethodName,
                Parameters = parameters,
                IsNetwork = BusinessHelper.IsNetworkCall(fieldType),
                Async = false,
                IsSetAccessor = false
            };
        }
    }
}