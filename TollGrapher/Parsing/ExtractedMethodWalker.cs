﻿using System.Collections.Generic;
using System.Linq;
using log4net;
using Microsoft.CodeAnalysis;
using Tolltech.Models;
using Tolltech.TollGrapher.AnalyserHelpers;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapher.Models;

namespace Tolltech.TollGrapher.Parsing
{
    public class ExtractedMethodWalker : IExtractedMethodWalker
    {
        private readonly IMethodsExtractor methodsExtractor;
        private readonly IMethodSearchInfoParser[] methodSearchInfoParsers;
        private static readonly ILog log = LogManager.GetLogger(typeof(ExtractedMethodWalker));

        public ExtractedMethodWalker(IMethodsExtractor methodsExtractor, IMethodSearchInfoParser[] methodSearchInfoParsers)
        {
            this.methodsExtractor = methodsExtractor;
            this.methodSearchInfoParsers = methodSearchInfoParsers.OrderBy(x => x.Priority).ToArray();
        }

        public TreeNode<MethodNode> BuildEntryPointWalking(ExtractedMethod extractedMethod, CompiledProjectModel projects, Solution solution, string rootNamespaceName, Dictionary<string, TreeNode<MethodNode>> methodNodesCache = null)
        {
            methodNodesCache = methodNodesCache ?? new Dictionary<string, TreeNode<MethodNode>>();

            var visitedMethods = new HashSet<string>();
            var children = WalkThroughChildren(extractedMethod, solution, projects, visitedMethods, rootNamespaceName, methodNodesCache);

            return new TreeNode<MethodNode>
            {
                Children = children,
                Node = new MethodNode
                {
                    Name = extractedMethod.Name,
                    Namespace = extractedMethod.Namespace,
                    ClassName = extractedMethod.ClassName,
                    ShortName = extractedMethod.ShortName,
                    NetworkCall = true,
                    ServiceName = extractedMethod.ServiceName,
                    EntryPoint = true,
                    IsProperty = extractedMethod.IsProperty,
                    ReturnType = extractedMethod.ReturnType.ToString(),
                    Parameters = extractedMethod.GetMethodParametersInfos(),
                    SourceCode = extractedMethod.GetSourceCodeInfo(),
                    Async = true
                }
            };
        }

        private static readonly HashSet<string> stopMethodNames = new HashSet<string>{ "AjaxError", "Content", "File", "GetType", "IfNotNull", "Json", "nameof", "NewtonsoftJson",
            "PartialView", "RedirectToAction", "SafeHandleForm" , "ToString", "View" };

        private TreeNode<MethodNode>[] WalkThroughChildren(ExtractedMethod extractedMethod, Solution solution, CompiledProjectModel compiledProjects, HashSet<string> visitedMethods, string rootNamespaceName, Dictionary<string, TreeNode<MethodNode>> methodNodesCache, int recusrsionLevel = 0)
        {
            var currentLevelNodes = new Dictionary<string, TreeNode<MethodNode>>();

            visitedMethods.Add(extractedMethod.Hash);

            var descendantNodes = extractedMethod.MethodBody?.DescendantNodes();
            if (descendantNodes == null)
            {
                visitedMethods.Remove(extractedMethod.Hash);
                return new TreeNode<MethodNode>[0];
            }

            foreach (var syntaxNode in descendantNodes)
            {
                var methodSearchInfo = methodSearchInfoParsers.Select(x => x.GetMethodSearchInfo(syntaxNode, extractedMethod, rootNamespaceName, solution, compiledProjects)).FirstOrDefault(x => x != null);

                if (methodSearchInfo == null || stopMethodNames.Contains(methodSearchInfo.MethodName))
                {
                    continue;
                }

                foreach (var symbol in methodSearchInfo.ClassSymbols)
                {
                    if (symbol == null)
                    {
                        log.Error($"Cant extract method {methodSearchInfo.MethodName} couldnt found Symbol. Parent {extractedMethod.Name}");
                        $"Cant extract method,{methodSearchInfo.MethodName},{extractedMethod.Name}".ToStructuredLogFile();
                        continue;
                    }

                    if (symbol.ContainingNamespace.Name.StartsWith("System."))
                    {
                        continue;
                    }

                    var childMethod = methodsExtractor.ExtractMethod(compiledProjects, symbol, methodSearchInfo.MethodName, methodSearchInfo.Parameters);

                    if (childMethod == null)
                    {
                        log.Error($"Cant extract method {methodSearchInfo.MethodName} for {symbol}");
                        $"Cant extract method,{methodSearchInfo.MethodName},{symbol}".ToStructuredLogFile();
                        continue;
                    }

                    childMethod.ShortName = childMethod.IsProperty ? AddGetSet(childMethod.ShortName, methodSearchInfo.IsSetAccessor) : childMethod.ShortName;

                    if (currentLevelNodes.ContainsKey(childMethod.Hash))
                    {
                        continue;
                    }

                    if (visitedMethods.Contains(childMethod.Hash))
                    {
                        log.Info($"Found recursion for method {childMethod.Name} of {symbol}");
                        $"Found recursion for method,{childMethod.Name},{symbol}".ToStructuredLogFile();
                        continue;
                    }

                    var treeNode = BuildTreeNodeOrGetCached(solution, compiledProjects, visitedMethods, rootNamespaceName, methodNodesCache, recusrsionLevel, childMethod, methodSearchInfo, symbol);
                    currentLevelNodes.Add(childMethod.Hash, treeNode);
                }
            }

            visitedMethods.Remove(extractedMethod.Hash);
            return currentLevelNodes.Values.ToArray();
        }

        private TreeNode<MethodNode> BuildTreeNodeOrGetCached(Solution solution, CompiledProjectModel compiledProjects, HashSet<string> visitedMethods,
            string rootNamespaceName, Dictionary<string, TreeNode<MethodNode>> methodNodesCache, int recusrsionLevel, ExtractedMethod childMethod,
            MethodSearchInfo methodSearchInfo, ISymbol symbol)
        {
            TreeNode<MethodNode> treeNode;
            if (methodNodesCache.TryGetValue(childMethod.Hash, out treeNode))
            {
                return treeNode;
            }

            var children = WalkThroughChildren(childMethod, solution, compiledProjects, visitedMethods, rootNamespaceName, methodNodesCache, recusrsionLevel + 1);

            treeNode = new TreeNode<MethodNode>
            {
                Node = new MethodNode
                {
                    Name = childMethod.Name,
                    Namespace = childMethod.Namespace,
                    ClassName = childMethod.ClassName,
                    ShortName = childMethod.ShortName,
                    NetworkCall = methodSearchInfo.IsNetwork,
                    Async = methodSearchInfo.Async,
                    ServiceName = symbol.ContainingAssembly.Identity.Name.GetPostfix(),
                    IsProperty = childMethod.IsProperty,
                    ReturnType = childMethod.ReturnType.ToString(),
                    Parameters = childMethod.GetMethodParametersInfos(),
                    SourceCode = childMethod.GetSourceCodeInfo()
                },
                Children = children
            };

            methodNodesCache[treeNode.Node.Hash] = treeNode;
            return treeNode;
        }

        private const string set = "set";
        private const string get = "get";

        public static string AddGetSet(string src, bool isSetAccessor)
        {
            var getSet = isSetAccessor ? set : get;
            return $"{getSet}_{src}";
        }
    }
}