﻿using System;
using log4net;

namespace Tolltech.TollGrapher.AnalyserHelpers
{
    public static class LogExtensions
    {
        public static void ToConsole(this ILog log, string line)
        {
            log.Info(line);
            Console.WriteLine(line);
        }
    }
}