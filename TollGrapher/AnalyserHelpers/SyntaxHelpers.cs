﻿using System;
using System.Linq;
using log4net;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Tolltech.Models;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapher.Models;
using CSharpExtensions = Microsoft.CodeAnalysis.CSharp.CSharpExtensions;

namespace Tolltech.TollGrapher.AnalyserHelpers
{
    public static class SyntaxHelpers
    {
        private static ILog log = LogManager.GetLogger(typeof(SyntaxHelpers));

        public static T FindParent<T>(this SyntaxNode syntaxNode) where T : SyntaxNode
        {
            var s = syntaxNode.Parent;

            while (true)
            {
                if (s == null)
                {
                    return null;
                }

                var result = s as T;
                if (result != null)
                {
                    return result;
                }

                s = s.Parent;
            }
        }


        public static FieldInfo GetInvocationOfFieldInfo(SyntaxNode syntaxNode, ExtractedMethod extractedMethod)
        {
            var invocationExpressionSyntax = syntaxNode as InvocationExpressionSyntax;
            if (invocationExpressionSyntax == null)
            {
                return null;
            }

            var memberAccessExpressionSyntax = invocationExpressionSyntax.Expression as MemberAccessExpressionSyntax;
            if (memberAccessExpressionSyntax == null)
            {
                return null;
            }

            var methodName = memberAccessExpressionSyntax.Name.Identifier.ValueText;

            var fieldType = extractedMethod.GetExpressionSymbol(memberAccessExpressionSyntax.Expression);

            if (fieldType == null)
            {
                log.Error($"cant get symbol of member {methodName} of document {extractedMethod.DocumentSyntaxTree.FilePath}");
                $"cant get symbol,{methodName},{extractedMethod.DocumentSyntaxTree.FilePath}".ToStructuredLogFile();
                return null;
            }

            return new FieldInfo
            {
                FieldType = fieldType,
                MethodName = methodName,
                FieldMethodInvocationExpression = invocationExpressionSyntax
            };
        }

        public static INamespaceOrTypeSymbol GetTypeSymbolInfo(this ExtractedMethod extractedMethod, ExpressionSyntax entityExpression)
        {
            return extractedMethod.SemanticModel.GetSymbolInfo(entityExpression).Symbol as INamespaceOrTypeSymbol;
        }

        public static ITypeSymbol GetExpressionSymbol(this ExtractedMethod extractedMethod, SyntaxNode syntaxExpression)
        {
            return extractedMethod.GetExpressionSymbol(syntaxExpression, (syntaxExpression as IdentifierNameSyntax)?.Identifier.ValueText ?? string.Empty);
        }

        public static ITypeSymbol GetExpressionSymbol(this ExtractedMethod extractedMethod, SyntaxNode syntaxExpression, string identifierName)
        {
            var fieldDeclaratorSyntax = extractedMethod.FindInFieldsDeclarations(identifierName);

            if (fieldDeclaratorSyntax != null)
            {
                var typeSymbol = (extractedMethod.SemanticModel.GetDeclaredSymbol(fieldDeclaratorSyntax) as IFieldSymbol)?.Type;
                if (typeSymbol != null)
                {
                    return typeSymbol;
                }
            }

            var propertiesDeclarations = extractedMethod.FindInPropertiesDeclarations(identifierName);
            if (propertiesDeclarations != null)
            {
                var typeSymbol = (extractedMethod.SemanticModel.GetDeclaredSymbol(propertiesDeclarations) as IPropertySymbol)?.Type;
                if (typeSymbol != null)
                {
                    return typeSymbol;
                }
            }

            var typeInfo = extractedMethod.SemanticModel.GetTypeInfo(syntaxExpression).Type;
            if (typeInfo != null)
            {
                return typeInfo;
            }

            var symbol = extractedMethod.SemanticModel.GetSymbolInfo(syntaxExpression).Symbol;
            return (symbol as ILocalSymbol)?.Type
                   ?? (symbol as IParameterSymbol)?.Type
                   ?? (symbol as ITypeSymbol);
        }

        private static PropertyDeclarationSyntax FindInPropertiesDeclarations(this ExtractedMethod extractedMethod, string identifierName)
        {
            if (string.IsNullOrEmpty(identifierName))
            {
                return null;
            }

            var properties = extractedMethod.DocumentSyntaxTree.GetRoot().DescendantNodes().OfType<PropertyDeclarationSyntax>().ToArray();
            return properties.FirstOrDefault(x => x.Identifier.ValueText == identifierName);
        }

        private static VariableDeclaratorSyntax FindInFieldsDeclarations(this ExtractedMethod extractedMethod, string identifierName)
        {
            if (string.IsNullOrEmpty(identifierName))
            {
                return null;
            }

            var fields = extractedMethod.DocumentSyntaxTree.GetRoot().DescendantNodes().OfType<FieldDeclarationSyntax>().ToArray();
            return fields.SelectMany(x => x.Declaration.Variables).FirstOrDefault(x => x.Identifier.ValueText == identifierName);
        }

        public static AssignmentExpressionSyntax GetFirstAssignExpression(this ExtractedMethod extractedMethod, string identifierName)
        {
            Func<AssignmentExpressionSyntax, bool> func = x =>
            {
                var assignmentIdentifierNameExpression = x.Left as IdentifierNameSyntax ??
                                                         (x.Left as MemberAccessExpressionSyntax)?.Name as
                                                         IdentifierNameSyntax;
                return assignmentIdentifierNameExpression != null &&
                       assignmentIdentifierNameExpression.Identifier.ValueText == identifierName;
            };

            return extractedMethod.MethodBody.DescendantNodes().OfType<AssignmentExpressionSyntax>()
                       .FirstOrDefault(func)
                   ?? extractedMethod.DocumentSyntaxTree.GetRoot().DescendantNodes()
                       .OfType<AssignmentExpressionSyntax>().FirstOrDefault(func);
        }

        public static T GetValueExpression<T>(this ExtractedMethod extractedMethod, ExpressionSyntax identifierExpression) where T : ExpressionSyntax
        {
            T expressionSyntax;
            var identifierName = (identifierExpression as IdentifierNameSyntax)?.Identifier.ValueText;
            if (identifierName != null)
            {
                expressionSyntax = FindInPropertiesValues<T>(extractedMethod, identifierName)
                    ?? FindInFieldsValues<T>(extractedMethod, identifierName)
                    ?? FindInLocalVariablesValues<T>(extractedMethod, identifierName);
            }
            else
            {
                expressionSyntax = (identifierExpression as T);
            }
            return expressionSyntax;
        }

        private static T FindInLocalVariablesValues<T>(ExtractedMethod extractedMethod, string identifierName) where T : ExpressionSyntax
        {
            var variableDeclaratorSyntax = extractedMethod.MethodBody?.DescendantNodes()
                .OfType<VariableDeclarationSyntax>()
                .SelectMany(x => x.Variables)
                .FirstOrDefault(x => x.Identifier.ValueText == identifierName);

            return variableDeclaratorSyntax?.Initializer?.Value as T;
        }

        private static T FindInFieldsValues<T>(ExtractedMethod extractedMethod, string identifierName) where T : ExpressionSyntax
        {
            var fields = extractedMethod.DocumentSyntaxTree.GetRoot().DescendantNodes().OfType<FieldDeclarationSyntax>().ToArray();

            var variableDeclaratorSyntax = fields.SelectMany(x => x.Declaration.Variables).FirstOrDefault(x => x.Identifier.ValueText == identifierName);

            return (variableDeclaratorSyntax?.Initializer?.Value as T);
        }

        private static T FindInPropertiesValues<T>(ExtractedMethod extractedMethod, string identifierName) where T : ExpressionSyntax
        {
            var properties = extractedMethod.DocumentSyntaxTree.GetRoot().DescendantNodes().OfType<PropertyDeclarationSyntax>().ToArray();
            var property = properties.FirstOrDefault(x => x.Identifier.ValueText == identifierName);

            var expressionBody = property?.ExpressionBody?.Expression as T;
            if (expressionBody != null)
            {
                return expressionBody;
            }

            var returnStatemnets = property?.AccessorList?.Accessors.Where(x => x.Body != null).SelectMany(x => x.Body.Statements.OfType<ReturnStatementSyntax>()).ToArray();
            if ((returnStatemnets?.Length ?? 0) > 1)
            {
                log.Error($"Found several return values for properties {identifierName} of {extractedMethod.Name} {extractedMethod.DocumentSyntaxTree.FilePath}");
                $"Found several return values,{extractedMethod.Name},{identifierName},{extractedMethod.DocumentSyntaxTree.FilePath}".ToStructuredLogFile();
            }

            return returnStatemnets?.Select(x => x.Expression).OfType<T>().FirstOrDefault();
        }

        public static MethodParameterInfo[] GetMethodParametersInfos(this ExtractedMethod extractedMethod)
        {
            return extractedMethod
                .Parameters
                .Select(x => new MethodParameterInfo
                {
                    Type = x.Type.ToString(),
                    Modifiers = x.Modifiers.Select(y => y.ValueText).ToArray(),
                    Name = x.Identifier.ValueText
                })
                .ToArray();
        }

        public static bool ParametersAreSuitable(this ExtractedMethod extractedMethod, ITypeSymbol[] parameters, out bool? identity)
        {
            identity = null;
            var parametersSyntax = extractedMethod.Parameters;

            var strongParameters = parametersSyntax.TakeWhile(x => x.Default == null && x.Modifiers.All(y => y.ValueText != "params")).ToArray();

            if (parameters.Length < strongParameters.Length)
            {
                return false;
            }

            for (var i = 0; i < parameters.Length; ++i)
            {
                if (i >= parametersSyntax.Length)
                {
                    return false;
                }

                var realParameter = parameters[i];
                var declaredParameter = parametersSyntax[i];

                var declaredTypeSymbol = extractedMethod.SemanticModel.GetSymbolInfo(declaredParameter.Type).Symbol as ITypeSymbol;

                if (declaredTypeSymbol == null)
                {
                    return false;
                }

                if (realParameter == null)
                {
                    if (declaredTypeSymbol.IsValueType && declaredTypeSymbol.Name != "Nullable")
                    {
                        return false;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (realParameter.ToString() == declaredTypeSymbol.ToString())
                {
                    continue;
                }

                var conversion = CSharpExtensions.ClassifyConversion(extractedMethod.Compilation, realParameter, declaredTypeSymbol);
                if (conversion.Exists && conversion.IsImplicit)
                {
                    identity = false;
                    continue;
                }

                if (declaredParameter.Modifiers.Any(x => x.Text == "params"))
                {
                    var declaredParameterElementType = (declaredTypeSymbol as IArrayTypeSymbol)?.ElementType;
                    if (declaredParameterElementType == null)
                    {
                        log.Error($"Strange params parameterType of {extractedMethod.Name} in document {extractedMethod.DocumentSyntaxTree.FilePath} {declaredTypeSymbol}");
                        $"Strange params parameterType,{extractedMethod.Name},{extractedMethod.DocumentSyntaxTree.FilePath},{declaredTypeSymbol}".ToStructuredLogFile();
                        return false;
                    }

                    if (realParameter.ToString() != declaredParameterElementType.ToString())
                    {
                        continue;
                    }

                    conversion = CSharpExtensions.ClassifyConversion(extractedMethod.Compilation, realParameter, declaredParameterElementType);
                    if (!(conversion.Exists && conversion.IsImplicit))
                    {
                        return false;
                    }

                    identity = false;
                    continue;
                }
                else
                {
                    return false;
                }
            }

            if (!identity.HasValue)
            {
                identity = true;
            }

            return true;
        }

        public static bool IsInteresting(this ITypeSymbol symbol, string rootNamespaceName)
        {
            if (!(symbol.ContainingNamespace?.ToString().StartsWith(rootNamespaceName) ?? false))
            {
                return false;
            }

            if (symbol.ContainingNamespace?.ToString().StartsWith("SKBKontur.Billy.Core") ?? false)
            {
                return false;
            }

            if (symbol.ContainingNamespace?.ToString().Contains("Testing") ?? false)
            {
                return false;
            }

            if (symbol.TypeKind == TypeKind.Enum)
            {
                return false;
            }

            return true;
        }
    }
}