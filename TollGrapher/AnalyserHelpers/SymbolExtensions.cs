﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;

namespace Tolltech.TollGrapher.AnalyserHelpers
{
    public static class SymbolExtensions
    {
        public static bool IsInheritedClass(this INamedTypeSymbol symbol, string baseClassName)
        {
            var s = symbol.BaseType;

            while (true)
            {
                if (s == null)
                {
                    return false;
                }

                if (s.Name == baseClassName)
                {
                    return true;
                }

                s = s.BaseType;
            }
        }

        public static ITypeSymbol[] GetBaseClasses(this ITypeSymbol symbol)
        {
            var result = new List<ITypeSymbol>();

            var s = symbol.BaseType;
            while (true)
            {
                if (s == null)
                {
                    return result.ToArray();
                }

                result.Add(s);
                s = s.BaseType;
            }
        }

        public static bool IsInherited(this ITypeSymbol childSymbol, ITypeSymbol baseSymbol)
        {
            var currentSymbols = new[] { childSymbol };
            while (true)
            {
                if (currentSymbols.Length == 0)
                {
                    return false;
                }

                if (currentSymbols.Any(x => x.OriginalDefinition.ToString() == baseSymbol.OriginalDefinition.ToString()))
                {
                    return true;
                }

                var symbols = currentSymbols.Select(x => (ITypeSymbol)x.BaseType).Where(x => x != null).ToList();

                if (baseSymbol.TypeKind == TypeKind.Interface)
                {
                    symbols.AddRange(currentSymbols.SelectMany(x => x.AllInterfaces.Select(y => (ITypeSymbol)y).ToArray()).ToArray());
                }

                currentSymbols = symbols.ToArray();
            }
        }

        public static bool IsInherited(this ITypeSymbol childSymbol, string baseSymbolName)
        {
            var currentSymbols = new[] { childSymbol };
            while (true)
            {
                if (currentSymbols.Length == 0)
                {
                    return false;
                }

                if (currentSymbols.Any(x => x.Name == baseSymbolName))
                {
                    return true;
                }

                var symbols = currentSymbols.Select(x => (ITypeSymbol)x.BaseType).Where(x => x != null).ToList();

                symbols.AddRange(currentSymbols.SelectMany(x => x.AllInterfaces.Select(y => (ITypeSymbol)y).ToArray()).ToArray());

                currentSymbols = symbols.ToArray();
            }
        }

        public static bool IsInherited(this ISymbol childSymbol, ITypeSymbol baseSymbol)
        {
            var typeSymbol = childSymbol as ITypeSymbol;

            if (typeSymbol == null)
            {
                return false;
            }

            return IsInherited(typeSymbol, baseSymbol);
        }

        public static bool IsInherited(this ISymbol childSymbol, string baseSymbolName)
        {
            var typeSymbol = childSymbol as ITypeSymbol;

            if (typeSymbol == null)
            {
                return false;
            }

            return IsInherited(typeSymbol, baseSymbolName);
        }

        public static bool HasAttribute(this ISymbol symbol, string attributeName)
        {
            return symbol.GetAttributes().Any(a => a.AttributeClass.MetadataName == attributeName
            || a.AttributeClass.MetadataName == attributeName.Replace("Attribute", ""));//todo: проверить нормально через символы
        }
    }
}