﻿using Microsoft.CodeAnalysis;

namespace Tolltech.TollGrapher.AnalyserHelpers
{
    public static class BusinessHelper
    {
        public static bool IsNetworkCall(ITypeSymbol fieldType)
        {
            if (fieldType.HasAttribute("NetworkServiceAttribute"))
            {
                return true;
            }

            return false;
        }
    }
}