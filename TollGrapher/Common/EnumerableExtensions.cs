﻿using System;
using System.Collections.Generic;

namespace Tolltech.TollGrapher.Common
{
    public static class EnumerableExtensions
    {
        public static TValue GetOrAdd<TKey, TValue>(this IDictionary<TKey,TValue> dict, TKey key, Func<TKey, TValue> valueFunc)
        {
            TValue val;
            if (dict.TryGetValue(key, out val))
            {
                return val;
            }

            val = valueFunc(key);
            dict[key] = val;

            return val;
        }
    }
}
