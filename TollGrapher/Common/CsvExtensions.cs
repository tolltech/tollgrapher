﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection;

namespace Tolltech.TollGrapher.Common
{
    public static class CsvExtensions
    {
        private static readonly ConcurrentDictionary<Type, PropertyInfo[]> cachedProperties = new ConcurrentDictionary<Type, PropertyInfo[]>();

        public static string GetCsvHeader<T>(string separator = ",")
        {
            var props = GetOrAddProperties<T>();
            return string.Join(separator, props.OrderBy(x => x.Name).Select(x => x.Name));
        }

        private static PropertyInfo[] GetOrAddProperties<T>()
        {
            return cachedProperties.GetOrAdd(typeof(T), t => t.GetProperties(BindingFlags.Instance | BindingFlags.Public));
        }

        public static string GetCsvRow<T>(this T obj, string separator = ",")
        {
            var props = GetOrAddProperties<T>();
            var propValues = props.OrderBy(x => x.Name).Select(x =>
                {
                    var v = x.GetValue(obj);
                    return x.PropertyType == typeof(bool)
                        ? (bool)v ? "1" : "0"
                        : v.ToString();
                }
            );

            return string.Join(separator, propValues);
        }
    }
}