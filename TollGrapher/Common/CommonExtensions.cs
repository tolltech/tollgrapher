﻿namespace Tolltech.TollGrapher.Common
{
    public static class CommonExtensions
    {
        public static string ToIntString(this bool b) => b ? "1" : "0";
    }
}