﻿using System.Collections.Generic;
using System.Linq;
using Tolltech.Common;
using Tolltech.Models;
using Tolltech.Neo4jData.Schema;

namespace Tolltech.TollGrapher.Common
{
    public static class MethodNodeExtension
    {
        public static string[] GetLabels(this StorageMethodNode node)
        {
            var labels = new List<string>
            {
                typeof(StorageMethodNode).GetAttributeValues<LabelAttribute, string>(x => x.Name).Single(),
                node.NetworkCall ? GrapherConstants.NetworkMethod : GrapherConstants.LocalMethod,
            };

            if (node.EntryPoint)
            {
                labels.Add(GrapherConstants.EntryPoint);
            }

            if (node.IsProperty)
            {
                labels.Add(GrapherConstants.Property);
            }

            if (node.Async)
            {
                labels.Add(GrapherConstants.Async);
            }

            return labels.ToArray();
        }

        public static StorageMethodNode ConvertToStorageMethodNode(this MethodNode node)
        {
            return new StorageMethodNode
            {
                Name = node.Name,
                Namespace = node.Namespace,
                ClassName = node.ClassName,
                EntryPoint = node.EntryPoint,
                Async = node.Async,
                Error = node.Error,
                Id = node.Id,
                IsProperty = node.IsProperty,
                NetworkCall = node.NetworkCall,
                ReturnType = node.ReturnType,
                ServiceName = node.ServiceName,
                ShortName = node.ShortName,
            }.SetParameters(node.Parameters);
        }
    }
}