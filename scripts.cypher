// импорт нодов
using periodic commit
load csv with headers from 'file:///nodes.csv' as row
create (:Method {Id: row.id, MethodName: row.MethodName, ServiceName: row.ServiceName, Network: row.Network});

// ограничение на уникальность id
create constraint on (m:Method) assert m.Id is unique;

// короткие имена для нодов
match (n:Method)
set n.ShortName = split(n.MethodName, '.')[size(split(n.MethodName, '.'))-1], 
n.ClassName = split(n.MethodName, '.')[size(split(n.MethodName, '.'))-2];

// разные лэйблы для сетевых/несетевых методов
match (n {Network:'1'}) set n:NetworkMethod;

match (n {Network:'0'}) set n:LocalMethod;

//match (n {Network:'1'}) remove n:Method;

// импорт связей
using periodic commit
load csv with headers from 'file:///edges.csv' as row
match (a {Id: row.parentId}), (b {Id: row.childId}) 
foreach (dummy IN case when a.Network = '1' and b.Network = '1' then [1] else [] end | merge (a)-[:networkCall]->(b))
foreach (dummy IN case when a.Network = '0' or b.Network = '0' then [1] else [] end | merge (a)-[:directCall]->(b));

// генерация неявных связей между сетевыми нодами
match path = (n {Network: '1'})-[:directCall*]->(m {Network: '1'}) 
with size(filter(nod in nodes(path) where nod.Network = '1')) as cnt, n, m
where not (n)-[]->(m) and cnt = 2
create (n)-[:implicitCall]->(m);

// добавляем ноды сервисов
match (n)
with collect(distinct n.ServiceName) as services
foreach (s IN services | create (:Service {ServiceName: s}));

// проставляем связи между сервисами и методами
match (n:NetworkMethod), (s:Service)
where n.ServiceName = s.ServiceName
merge (s)-[:contains]->(n);

// проставляем зависимости между сервисами
match (s1:Service)-[:contains]->(n:NetworkMethod)-[:networkCall|implicitCall]->(m:NetworkMethod)<-[:contains]-(s2:Service)
where s1 <> s2
merge (s1)-[:use]->(s2)

// количество отображаемых нодов
// :config initialNodeDisplay: 3000