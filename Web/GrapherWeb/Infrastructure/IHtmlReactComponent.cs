using Microsoft.AspNetCore.Html;

namespace Tolltech.TollGrapher.GrapherWeb.Infrastructure
{
    public interface IHtmlReactComponent
    {
        IHtmlContent RenderHtml();
        string RenderJavaScript();
    }
}
