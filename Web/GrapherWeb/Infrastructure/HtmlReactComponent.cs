using System.Collections.Generic;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Tolltech.TollGrapher.GrapherWeb.Infrastructure
{
    public class HtmlReactComponent : IHtmlReactComponent
    {
        public HtmlReactComponent(string componentName, string containerId, string containerTag, object props)
        {
            _componentName = componentName;
            _containerId = containerId;
            _containerTag = containerTag;
            _props = props;
        }

        private readonly object _props;
        private readonly string _componentName;
        private readonly string _containerId;
        private readonly string _containerTag;

        public IHtmlContent RenderHtml()
        {
            var htmlTag = new TagBuilder(_containerTag);
            htmlTag.Attributes.Add("id", _containerId);

            return htmlTag;
        }

        public string RenderJavaScript()
        {
            return $"ReactDOM.render({GetComponentInitialiser()}, document.getElementById({JsonConvert.SerializeObject(_containerId)}))";
        }

        private string GetComponentInitialiser()
        {
            var serializedObject = JsonConvert.SerializeObject(_props, new JsonSerializerSettings
            {
                Converters = new List<JsonConverter> { new StringEnumConverter() }
            });

            return $"React.createElement({_componentName}.default, {serializedObject})";
        }
    }
}
