﻿namespace Tolltech.TollGrapher.GrapherWeb.Infrastructure
{
    public interface IHtmlReactContainer
    {
        string GetInitJavaScript();
        IHtmlReactComponent CreateComponent<T>(string componentName, T props, string containerTag, string containerId);
    }
}
