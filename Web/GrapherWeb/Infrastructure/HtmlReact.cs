﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Tolltech.TollGrapher.GrapherWeb.Infrastructure
{
    public static class HtmlReact
    {
        private static readonly IHtmlReactContainer Container = new HtmlReactContainer();

        public static IHtmlContent Component(string componentName)
        {
            return Component(componentName, (object)null);
        }

        public static IHtmlContent Component<T>(string componentName, T props, string htmlTag = null, string containerId = null)
        {
            var component = Container.CreateComponent(componentName, props, htmlTag, containerId);
            return component.RenderHtml();
        }

        public static IHtmlContent Init()
        {
            var scriptTag = new TagBuilder("script")
            {
                TagRenderMode = TagRenderMode.Normal
            };

            scriptTag.InnerHtml.AppendHtml(Container.GetInitJavaScript());

            return scriptTag;
        }
    }
}
