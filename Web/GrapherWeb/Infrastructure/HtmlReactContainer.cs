﻿using System.Collections.Concurrent;
using System.Text;

namespace Tolltech.TollGrapher.GrapherWeb.Infrastructure
{
    public class HtmlReactContainer : IHtmlReactContainer
    {
        private readonly ConcurrentDictionary<string, IHtmlReactComponent> _components = new ConcurrentDictionary<string, IHtmlReactComponent>();

        public IHtmlReactComponent CreateComponent<T>(string componentName, T props, string containerTag, string containerId)
        {
            if (string.IsNullOrWhiteSpace(containerId))
            {
                containerId = $"react{_components.Count}";
            }
            if (string.IsNullOrWhiteSpace(containerTag))
            {
                containerTag = "div";
            }

            return _components.GetOrAdd(containerId, k => new HtmlReactComponent(componentName, containerId, containerTag, props));
        }

        public string GetInitJavaScript()
        {
            var stringBuilder = new StringBuilder();
            foreach (var reactComponent in _components.Values)
            {
                stringBuilder.Append(reactComponent.RenderJavaScript());
                stringBuilder.AppendLine(";");
            }

            return stringBuilder.ToString();
        }
    }
}
