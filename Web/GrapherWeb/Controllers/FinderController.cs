﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Tolltech.TollGrapher.GrapherWeb.Models.Finder;
using Tolltech.TollGrapherApi;
using Tolltech.TollGrapherApi.Models;

namespace Tolltech.TollGrapher.GrapherWeb.Controllers
{
    public class FinderController : Controller
    {
        private readonly ITollGrapherSearchClient searchClient;
        private readonly IFinderViewModelConverter finderViewModelConverter;

        public FinderController(
            ITollGrapherSearchClient searchClient,
            IFinderViewModelConverter finderViewModelConverter
        )
        {
            this.searchClient = searchClient;
            this.finderViewModelConverter = finderViewModelConverter;
        }

        public IActionResult Index()
        {
            return View("Index", new FinderViewModel
            {
                ClassNameAutocompleteUrl = Url.Action("ClassNameAutocomplete", "Finder"),
                MethodNameAutocompleteUrl = Url.Action("MethodNameAutocomplete", "Finder"),
                FindUrl = Url.Action("Find", "Finder")
            });
        }

        public IActionResult ClassNameAutocomplete(string className)
        {
            var methodModels = searchClient.GetClassNames(className, 10);
            var result = methodModels.Select(finderViewModelConverter.Convert).ToArray();

            return Json(result);
        }

        public IActionResult MethodNameAutocomplete(string namespaceName, string className, string methodName)
        {
            var methodModels = searchClient.GetMethodNames(namespaceName, className, methodName, 10);
            return Convert(methodModels);
        }

        public IActionResult Find(Guid methodId)
        {
            var allEntryPoints = searchClient.GetAllEntryPoints(methodId);
            return Convert(allEntryPoints);
        }

        private JsonResult Convert(MethodModel[] methodModels)
        {
            return Json(methodModels.Select(finderViewModelConverter.Convert).ToArray());
        }
    }
}
