﻿using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace Tolltech.TollGrapher.GrapherWeb
{
    public static class EntryPoint
    {
        public static void Main(string[] args)
        {
            new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build()
                .Run();
        }
    }
}
