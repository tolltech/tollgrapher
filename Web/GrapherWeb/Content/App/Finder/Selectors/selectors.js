import { createSelector } from "reselect";

const getClassName = state => state.className || "";
const getClassNameSuggestions = state => state.classNameSuggestions || [];

const getMethodName = state => state.methodName || "";
const getMethodNameSuggestions = state => state.methodNameSuggestions || [];

export const getFindResult = state => state.findResult || {};

export const getClassNameAutocompleteItems = createSelector(
    getClassName,
    getClassNameSuggestions,
    (className, classNameSuggestions) => ({
        className,
        classNameSuggestions
    })
);

export const getMethodNameAutocompleteItems = createSelector(
    getMethodName,
    getMethodNameSuggestions,
    (methodName, methodNameSuggestions) => ({
        methodName,
        methodNameSuggestions
    })
);