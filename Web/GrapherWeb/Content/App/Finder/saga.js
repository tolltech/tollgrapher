import { takeLatest, select, throttle } from "redux-saga/effects"
import { fetchData, httpMethod } from "../Helpers/Saga/saga"

import * as actionTypes from "./actionTypes"
import * as actions from "./actions"

const throttleDelay = 500;

function* classNameTypingWatcher() {
    yield throttle(throttleDelay, [actionTypes.CLASS_NAME_TYPE], function* () {
        const { ClassNameAutocompleteUrl, className } = yield select(state => ({
            ClassNameAutocompleteUrl: state.ClassNameAutocompleteUrl,
            className: state.className
        }));

        if (!className || className === "") {
            return [];
        }

        yield fetchData({
            url: ClassNameAutocompleteUrl,
            data: { className },
            requestMethod: httpMethod.get,
            onSuccess: actions.getClassNameSuggestionsSuccess,
            onError: actions.getClassNameSuggestionsFailed
        });
    })
}

function* methodNameTypingWatcher() {
    yield throttle(throttleDelay, [actionTypes.METHOD_NAME_TYPE], function* () {
        const { MethodNameAutocompleteUrl, methodName, className = "", namespace = "" } = yield select(state => ({
            MethodNameAutocompleteUrl: state.MethodNameAutocompleteUrl,
            methodName: state.methodName,
            className: state.className,
            namespace: state.namespace
        }));

        if (!className || className === "" || !methodName || methodName === "") {
            return [];
        }

        yield fetchData({
            url: MethodNameAutocompleteUrl,
            data: { namespaceName: namespace, className, methodName },
            requestMethod: httpMethod.get,
            onSuccess: actions.getMethodNameSuggestionsSuccess,
            onError: actions.getMethodNameSuggestionsFailed
        });
    })
}

function* chooseMethodWatcher() {
    yield takeLatest([actionTypes.CHOOSE_METHOD], function* () {
        const { FindUrl, methodId, className, methodName } = yield select(state => ({
            FindUrl: state.FindUrl,
            methodId: state.methodNodeId,
            className: state.className,
            methodName: state.methodName
        }));

        yield fetchData({
            url: FindUrl,
            data: { methodId },
            requestMethod: httpMethod.get,
            onSuccess: actions.findEntryPointsSuccess,
            onFailed: actions.findEntryPointsFailed,
            additionalResponseData: { className, methodName }
        });
    })
}

export default function* rootSaga() {
    yield [
        classNameTypingWatcher(),
        methodNameTypingWatcher(),
        chooseMethodWatcher()
    ];
}