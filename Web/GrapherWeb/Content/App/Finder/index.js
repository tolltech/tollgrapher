import { PureComponent } from "react";
import { Provider } from "react-redux";

import configureStore from "../Helpers/Redux/configureStore";

import App from "../Helpers/App/App"

import Finder from "./Components/Finder";
import { initialState, reducer } from "./reducer";
import rootSaga from "./saga";

export default class FinderPage extends PureComponent {
    render() {
        const store = configureStore({ ...initialState, ...this.props }, reducer, rootSaga);
        return (
            <Provider store={store}>
                <App>
                    <Finder />
                </App>
            </Provider>
        );
    }
}

FinderPage.propTypes = {};