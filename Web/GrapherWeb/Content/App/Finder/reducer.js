import { combineReducers } from "redux";
import reduceReducers from "reduce-reducers"
import { handleActions } from "redux-actions";

import * as actionTypes from "./actionTypes";

import app from "../Helpers/App/reducer"

export const initialState = {
    namespace: "",

    className: "",
    classNameSuggestions: [],

    methodName: "",
    methodNameSuggestions: [],

    findResult: {}
};

export const reducer = combineReducers({
    ClassNameAutocompleteUrl: (state = "") => state,
    MethodNameAutocompleteUrl: (state = "") => state,
    FindUrl: (state = "") => state,

    app,

    namespace: handleActions({
        [actionTypes.CHOOSE_CLASS]: (state, { payload }) => payload.namespace
    }, initialState.namespace),
    className: handleActions({
        [actionTypes.CLASS_NAME_TYPE]: (state, { payload: className }) => className,
        [actionTypes.CHOOSE_CLASS]: (state, { payload }) => payload.className
    }, initialState.className),
    classNameSuggestions: handleActions({
        [actionTypes.GET_CLASS_NAME_SUGGESTIONS_SUCCESS]: (state, { payload: result }) => result
    }, initialState.classNameSuggestions),
    classNameSuggestingFailed: handleActions({
        [actionTypes.GET_CLASS_NAME_SUGGESTIONS_FAILED]: (state) => true,
    }, false),

    methodName: handleActions({
        [actionTypes.CLASS_NAME_TYPE]: (state) => "",
        [actionTypes.METHOD_NAME_TYPE]: (state, { payload: methodName }) => methodName
    }, initialState.methodName),
    methodNameSuggestions: handleActions({
        [actionTypes.GET_METHOD_NAME_SUGGESTIONS_SUCCESS]: (state, { payload: result }) => result
    }, initialState.methodNameSuggestions),
    methodNameSuggestingFailed: handleActions({
        [actionTypes.GET_METHOD_NAME_SUGGESTIONS_FAILED]: (state) => true,
    }, false),

    methodNodeId: handleActions({
        [actionTypes.CHOOSE_METHOD]: (state, { payload: methodNodeId }) => methodNodeId
    }, ""),

    isSuggesting: handleActions({
        [actionTypes.CLASS_NAME_TYPE]: (state) => true,
        [actionTypes.METHOD_NAME_TYPE]: (state) => true,
        [actionTypes.GET_CLASS_NAME_SUGGESTIONS_SUCCESS]: (state) => false,
        [actionTypes.GET_METHOD_NAME_SUGGESTIONS_SUCCESS]: (state) => false,
        [actionTypes.GET_CLASS_NAME_SUGGESTIONS_FAILED]: (state) => false,
        [actionTypes.GET_METHOD_NAME_SUGGESTIONS_FAILED]: (state) => false,
    }, false),

    isSearching: handleActions({
        [actionTypes.FIND_ENTRY_POINTS]: (state) => true,
        [actionTypes.FIND_ENTRY_POINTS_SUCCESS]: (state) => false,
        [actionTypes.FIND_ENTRY_POINTS_FAILED]: (state) => false
    }, false),
    findResult: handleActions({
        [actionTypes.FIND_ENTRY_POINTS_SUCCESS]: (state, { payload }) => ({
            entryPoints: payload.result,
            className: payload.className,
            methodName: payload.methodName
        })
    }, initialState.findResult)
});