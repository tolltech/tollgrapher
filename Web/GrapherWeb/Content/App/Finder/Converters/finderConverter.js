import MenuItem from "material-ui/MenuItem";

import MethodDescription from "../../Shared/Components/MethodDescription";
import ClassDescription from "../../Shared/Components/ClassDescription";

export const convertClassesToMenuItems = classes => classes.map((c, i) => ({
    text: c.className,
    value: <MenuItem key={i}
                     primaryText={<ClassDescription className={c} />}/>,
    rawEntity: c
}));

export const convertMethodsToMenuItems = methods => methods.map((m, i) => ({
    text: m.methodName,
    value: <MenuItem key={i}
                     primaryText={<MethodDescription method={m} />} />,
    methodId: m.methodId,
    rawEntity: m
}));