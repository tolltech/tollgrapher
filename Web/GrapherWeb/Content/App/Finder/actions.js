import * as actionTypes from "./actionTypes";
import { createAction } from "redux-actions";

export const classNameType = createAction(actionTypes.CLASS_NAME_TYPE);
export const methodNameType = createAction(actionTypes.METHOD_NAME_TYPE);
export const getClassNameSuggestionsSuccess = createAction(actionTypes.GET_CLASS_NAME_SUGGESTIONS_SUCCESS);
export const getMethodNameSuggestionsSuccess = createAction(actionTypes.GET_METHOD_NAME_SUGGESTIONS_SUCCESS);
export const getClassNameSuggestionsFailed = createAction(actionTypes.GET_CLASS_NAME_SUGGESTIONS_FAILED);
export const getMethodNameSuggestionsFailed = createAction(actionTypes.GET_METHOD_NAME_SUGGESTIONS_FAILED);

export const chooseClass = createAction(actionTypes.CHOOSE_CLASS);
export const chooseMethod = createAction(actionTypes.CHOOSE_METHOD);
export const findEntryPoints = createAction(actionTypes.FIND_ENTRY_POINTS);
export const findEntryPointsSuccess = createAction(actionTypes.FIND_ENTRY_POINTS_SUCCESS);
export const findEntryPointsFailed = createAction(actionTypes.FIND_ENTRY_POINTS_FAILED);