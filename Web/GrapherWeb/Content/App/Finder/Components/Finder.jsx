import { PureComponent } from "react";

import SearchBlock from "./SearchBlock"
import EntryPointsList from "./EntryPointsList"

class Finder extends PureComponent {
    render() {
        return (
            <section>
                <SearchBlock/>
                <EntryPointsList/>
            </section>
        )
    };
}

export default Finder;