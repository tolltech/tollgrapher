import { PureComponent } from "react";
import PropTypes from "prop-types";

import MaterialAutocomplete from "material-ui/AutoComplete";

class Autocomplete extends PureComponent {
    noFilter = (text, key) => true;

    shouldOpen = value => {
        return !!(value && value !== "");
    };

    onSelected = selectedValue => {
        const { onSelect } = this.props;

        onSelect && onSelect(selectedValue);
    };

    render() {
        const { searchText, labelText, onUpdateInput, dataSource } = this.props;

        return (
            <MaterialAutocomplete searchText={searchText}
                                  floatingLabelText={labelText}
                                  onUpdateInput={onUpdateInput}
                                  dataSource={dataSource}
                                  filter={this.noFilter}
                                  fullWidth={true}
                                  openOnFocus={this.shouldOpen(searchText)}
                                  onNewRequest={this.onSelected} />
        );
    }
}

Autocomplete.propTypes = {
    searchText: PropTypes.string,
    labelText: PropTypes.string,
    onUpdateInput: PropTypes.func,
    dataSource: PropTypes.arrayOf(PropTypes.object),
    whenChosen: PropTypes.func
};

export default Autocomplete;