import { PureComponent } from "react";
import { connect } from "react-redux";

import Autocomplete from "./Autocomplete";

import styles from "./SearchBlock.scss";

import { classNameType, methodNameType, chooseClass, chooseMethod } from "../actions";
import { getClassNameAutocompleteItems, getMethodNameAutocompleteItems } from "../Selectors/selectors";

import { convertClassesToMenuItems, convertMethodsToMenuItems } from "../Converters/finderConverter"

class SearchBlock extends PureComponent {
    handleClassSelection = selectedClass => {
        const { chooseClass } = this.props;

        chooseClass({
            namespace: selectedClass.namespace,
            className: selectedClass.className
        });
    };

    requestFind = methodId => {
        const { chooseMethod } = this.props;

        chooseMethod(methodId);
    };

    render() {
        const {
            className, classNameSuggestions,
            methodName, methodNameSuggestions,
            classNameType, methodNameType
        } = this.props;

        const classNameSuggestionItems = convertClassesToMenuItems(classNameSuggestions);
        const methodNameSuggestionItems = convertMethodsToMenuItems(methodNameSuggestions);

        return (
            <div className={styles.inputs}>
                <Autocomplete searchText={className}
                              labelText="Class name"
                              onUpdateInput={classNameType}
                              dataSource={classNameSuggestionItems}
                              onSelect={selectedValue => this.handleClassSelection(selectedValue.rawEntity)} />
                <Autocomplete searchText={methodName}
                              labelText="Method name"
                              onUpdateInput={methodNameType}
                              dataSource={methodNameSuggestionItems}
                              onSelect={selectedValue => this.requestFind(selectedValue.methodId)} />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    ...getClassNameAutocompleteItems(state),
    ...getMethodNameAutocompleteItems(state)
});

export default connect(mapStateToProps, { classNameType, methodNameType, chooseClass, chooseMethod })(SearchBlock);