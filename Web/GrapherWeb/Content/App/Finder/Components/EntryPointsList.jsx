import { PureComponent } from "react";
import { connect } from "react-redux";

import styles from "./EntryPointsList.scss";

import Paper from "material-ui/Paper"
import Divider from "material-ui/Divider"

import { getFindResult } from "../Selectors/selectors";

import { convertMethodsToMenuItems } from "../Converters/finderConverter"

class EntryPointsList extends PureComponent {
    render() {
        const { findResult } = this.props;
        const { entryPoints, className, methodName } = findResult;

        const items = convertMethodsToMenuItems(entryPoints || []).map((r, i) => (
            <div key={i}>
                {i > 0 && (<Divider />)}
                {r.value}
            </div>

        ));

        return (
            !!items.length && (<section className={styles.entryPoints}>
                <h2>Entry points for {className}.{methodName}</h2>
                <Paper>
                    {items}
                </Paper>
            </section>)
        );
    }
}

const mapStateToProps = state => ({
    findResult: getFindResult(state)
});

export default connect(mapStateToProps)(EntryPointsList);