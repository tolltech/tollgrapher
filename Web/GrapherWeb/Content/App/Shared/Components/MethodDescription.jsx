import { PureComponent } from "react";

import styles from "./MethodDescription.scss";

import Method from "./Method";

class MethodDescription extends PureComponent {
    getMethodDeclaration = method => ({
        returnType: method.returnType || "",
        methodName: method.methodName || "",
        parameters: method.parameters || []
    });

    getMethodNameItem = method => {
        const { returnType, methodName, parameters } = this.getMethodDeclaration(method);
        return (
            <Method returnType={returnType} methodName={methodName} parameters={parameters}/>
        )
    };

    render() {
        const { method } = this.props;

        return (
            <div className={styles.symbolDescriptionBlock}>
                <span className={styles.namespace}>{method.declaringType}</span>
                <span className={styles.symbol}>{this.getMethodNameItem(method)}</span>
            </div>
        );
    }
}

export default MethodDescription;