import { PureComponent } from "react";

import MethodParameter from "./MethodParameter"

class MethodParameters extends PureComponent {
    render() {
        const { parameters } = this.props;

        const parameterNodes = !!parameters.length && parameters.map((p, i) => (
            <MethodParameter key={i} parameter={p} />
        )) || null;

        return (
            <span>({parameterNodes})</span>
        );
    }
}

export default MethodParameters;