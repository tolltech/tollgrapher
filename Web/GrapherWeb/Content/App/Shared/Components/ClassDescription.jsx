import { PureComponent } from "react";

import styles from "./ClassDescription.scss";

class ClassDescription extends PureComponent {
    render() {
        const { className } = this.props;

        return (
            <div className={styles.symbolDescriptionBlock}>
                <span className={styles.namespace}>{className.namespace}</span>
                <span className={styles.symbol}>{className.className}</span>
            </div>
        );
    }
}

export default ClassDescription;