import { PureComponent } from "react";

import styles from "./MethodParameter.scss";

class MethodParameter extends PureComponent {
    render() {
        const { parameter } = this.props;

        return (
            <span className={styles.methodParameter}>
                {!!parameter.modifiers.length && (<span className={styles.parameterModifier}>{parameter.modifiers.join(" ")}</span>)}
                <span className={styles.type}>{parameter.type}</span>
                <span>{parameter.name}</span>
            </span>
        );
    }
}

export default MethodParameter;