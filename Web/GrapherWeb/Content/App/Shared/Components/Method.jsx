import { PureComponent } from "react";

import styles from "./Method.scss";

import MethodParameters from "./MethodParameters"

class Method extends PureComponent {
    render() {
        const { returnType, methodName, parameters } = this.props;

        return (
            <span>
                <span className={styles.type}>{returnType}</span>
                <span className={styles.name}>{methodName}</span>
                <MethodParameters parameters={parameters} />
            </span>
        );
    }
}

export default Method;