export const BEGIN_REQUEST = "@@app/BEGIN_REQUEST";
export const END_REQUEST = "@@app/END_REQUEST";
export const SHOW_PROGRESS_BAR = "@@app/SHOW_PROGRESS_BAR";
export const HIDE_PROGRESS_BAR = "@@app/HIDE_PROGRESS_BAR";
export const PUSH_APP_ERROR = "@@app/PUSH_APP_ERROR";
export const HIDE_APP_ERROR = "@@app/HIDE_APP_ERROR";