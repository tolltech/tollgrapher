import * as actionTypes from "./actionTypes";
import { createAction } from "redux-actions";

export const beginRequest = createAction(actionTypes.BEGIN_REQUEST);
export const endRequest = createAction(actionTypes.END_REQUEST);
export const showProgressBar = createAction(actionTypes.SHOW_PROGRESS_BAR);
export const hideProgressBar = createAction(actionTypes.HIDE_PROGRESS_BAR);
export const pushAppError = createAction(actionTypes.PUSH_APP_ERROR);
export const hideAppError = createAction(actionTypes.HIDE_APP_ERROR);