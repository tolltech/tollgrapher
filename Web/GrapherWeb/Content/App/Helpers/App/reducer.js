import { handleActions } from "redux-actions";

import * as actionTypes from "./actionTypes";

const initialState = {
    showAppError: false,
    appErrorText: ""
};

const reducer = handleActions({
    [actionTypes.BEGIN_REQUEST]: state => ({
        ...state,
        isRequesting: true
    }),
    [actionTypes.SHOW_PROGRESS_BAR]: state => ({
        ...state,
        progressBarIsShown: state.progressBarIsShown || true
    }),
    [actionTypes.END_REQUEST]: state => ({
        ...state,
        isRequesting: false
    }),
    [actionTypes.HIDE_PROGRESS_BAR]: state => ({
        ...state,
        progressBarIsShown: state.isRequesting
    }),
    [actionTypes.PUSH_APP_ERROR]: (state, {payload: text}) => ({
        ...state,
        isRequesting: false,
        showAppError: true,
        appErrorText: text
    }),
    [actionTypes.HIDE_APP_ERROR]: (state) => ({
        ...state,
        ...initialState,
        isRequesting: false,
    })
}, initialState);

export default reducer;