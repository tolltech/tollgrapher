import { PureComponent } from "react";
import { connect } from "react-redux";

import { hideAppError } from "./actions";

import styles from "./App.scss";
import cx from "classnames";

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import myOwnBeautifulStyle from "./material-ui-theme";

import ProgressBar from "material-ui/LinearProgress"
import Snackbar from "material-ui/Snackbar/Snackbar"

class App extends PureComponent {
    render() {
        const {
            showAppError, appErrorText, hideAppError, progressBarIsShown,
            children
        } = this.props;

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(myOwnBeautifulStyle)}>
                <app>
                    <div className={cx(styles.header, "top")}>
                        {progressBarIsShown && <ProgressBar mode="indeterminate"/>}
                    </div>
                    <div className="container">
                        {children}
                    </div>
                    <Snackbar
                        open={showAppError}
                        message={appErrorText}
                        autoHideDuration={4000}
                        onRequestClose={hideAppError}
                    />
                </app>
            </MuiThemeProvider>
        );
    }
}

App.propTypes = {};

export default connect(state => ({ ...state.app }), { hideAppError })(App);