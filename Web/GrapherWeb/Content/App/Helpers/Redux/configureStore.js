import { compose, createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";

const _createStore = (initialState, rootReducer, middleware = []) => {
    return createStore(
        rootReducer,
        initialState,
        compose(applyMiddleware(...middleware))
    );
};

const configureStore = (initialState, rootReducer, rootSaga = null) => {
    if (rootSaga === null) {
        return _createStore(initialState, rootReducer);
    }

    const sagaMiddleware = createSagaMiddleware();
    const store = _createStore(initialState, rootReducer, [sagaMiddleware]);
    sagaMiddleware.run(rootSaga);

    return store;
};

export default configureStore;