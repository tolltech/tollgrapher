'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass:build', function () {
    return gulp.src('./Content/styles/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./wwwroot/css'));
});

gulp.task('sass:deploy', function () {
    return gulp.src('./Content/styles/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('./wwwroot/css'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./Content/styles/**/*.scss', ['sass']);
});
