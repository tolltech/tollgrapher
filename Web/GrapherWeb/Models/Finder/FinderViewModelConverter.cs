﻿using Tolltech.TollGrapherApi.Models;

namespace Tolltech.TollGrapher.GrapherWeb.Models.Finder
{
    public class FinderViewModelConverter : IFinderViewModelConverter
    {
        public MethodViewModel Convert(MethodModel methodModel)
        {
            var methodDeclaringType = methodModel.FullName.Substring(0, methodModel.FullName.LastIndexOf('.'));
            return new MethodViewModel
            {
                MethodId = methodModel.Id,
                ClassName = methodModel.ClassName,
                MethodName = methodModel.MethodName,
                DeclaringType = methodDeclaringType,
                ServiceName = methodModel.ServiceName,
                Parameters = methodModel.Parameters,
                ReturnType = methodModel.ReturnType
            };
        }

        public ClassViewModel Convert(ClassModel classModel)
        {
            return new ClassViewModel
            {
                ClassName = classModel.ClassName,
                ServiceName = classModel.ServiceName,
                Namespace = classModel.Namespace
            };
        }
    }
}
