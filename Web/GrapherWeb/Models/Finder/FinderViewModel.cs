﻿namespace Tolltech.TollGrapher.GrapherWeb.Models.Finder
{
    public class FinderViewModel
    {
        public string ClassNameAutocompleteUrl { get; set; }
        public string MethodNameAutocompleteUrl { get; set; }
        public string FindUrl { get; set; }
    }
}
