﻿namespace Tolltech.TollGrapher.GrapherWeb.Models.Finder
{
    public class FindResult
    {
        public string[] EntryPoints { get; set; }
    }
}
