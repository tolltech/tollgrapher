﻿using Tolltech.TollGrapherApi.Models;

namespace Tolltech.TollGrapher.GrapherWeb.Models.Finder
{
    public interface IFinderViewModelConverter
    {
        MethodViewModel Convert(MethodModel methodModel);
        ClassViewModel Convert(ClassModel classModel);
    }
}
