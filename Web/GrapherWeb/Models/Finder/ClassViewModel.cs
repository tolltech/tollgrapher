﻿namespace Tolltech.TollGrapher.GrapherWeb.Models.Finder
{
    public class ClassViewModel
    {
        public string ClassName { get; set; }
        public string ServiceName { get; set; }
        public string Namespace { get; set; }
    }
}
