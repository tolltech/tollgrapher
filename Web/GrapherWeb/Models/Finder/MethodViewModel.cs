﻿using System;
using Tolltech.TollGrapherApi.Models;

namespace Tolltech.TollGrapher.GrapherWeb.Models.Finder
{
    public class MethodViewModel
    {
        public Guid MethodId { get; set; }

        public string ServiceName { get; set; }

        public string ClassName { get; set; }

        public string MethodName { get; set; }
        public string DeclaringType { get; set; }

        public ParameterModel[] Parameters { get; set; }
        public string ReturnType { get; set; }
    }
}
