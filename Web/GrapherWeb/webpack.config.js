const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const paths = {
    outputDir: '/wwwroot/dist'
};

const createEntry = (partialPath, entryNameToExpose) => {
    const expose = entryNameToExpose ? `expose-loader?${entryNameToExpose}!` : null;
    return `${expose}${partialPath}`;
};

const config = {
    entry: {
        FinderPage: createEntry(path.join(__dirname, './Content/App/Finder/index.js'), "FinderPage"),

        libs: [
            'bootstrap',
            'bootstrap/dist/css/bootstrap.css',
            'react',
            'react-dom',
            'jquery',
            'babel-polyfill'
        ],

        common: [
            path.join(__dirname, "global-modules.js")
        ]
    },
    output: {
        path: path.join(__dirname, paths.outputDir),
        filename: "[name].js",
        publicPath: "/dist/",
        library: "[name]",
        chunkFilename: "chunks/[name].[chunkhash].js"
    },
    module: {
        rules: [
            {
                test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|eot|ttf)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 50000,
                            name: 'assets/[name]_[hash].[ext]'
                        }
                    }
                ]
            },
            {
                test: /\.jsx?$/,
                include: [
                    path.resolve(__dirname + '/Content/App'),
                    path.resolve(__dirname + '/node_modules/retail-ui')
                ],
                loader: 'babel-loader',
                query: {
                    cacheDirectory: true,
                    presets: ['es2015', 'react', 'stage-0']
                }
            },
            {
                test: /\.(css|scss)$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'sass-loader']
                })
            },
            {
                test: /\.less$/,
                loaders: ['style-loader', 'css-loader', 'less-loader'],
                include: /retail-ui/
            },
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: 'styles/[name].css',
            allChunks: true
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        }),
        new webpack.optimize.CommonsChunkPlugin({
            names: ["common", "manifest"],
            minChunks: Infinity
        }),
        new webpack.ProvidePlugin({
            "React": "react"
        }),
        new webpack.SourceMapDevToolPlugin({
            moduleFilenameTemplate: path.relative(paths.outputDir, '[resourcePath]')
        })
    ],
    resolve: {
        extensions: [ '.js', '.jsx' ]
    },

    devtool: "#inline-source-map",
    devServer: {
        cache: true,
        contentBase: "/wwwroot/dist",
        publicPath: "/dist/",
        proxy: {
            "*": "http://localhost:9900"
        },
        stats: { colors: true }
    }
};

module.exports = config;
