import React from "react";
import ReactDOM from "react-dom";
import injectTapEventPlugin from 'react-tap-event-plugin';

global.React = React;
global.ReactDOM = ReactDOM;

injectTapEventPlugin();
