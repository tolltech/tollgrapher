﻿using Microsoft.Extensions.DependencyInjection;
using Tolltech.Common;

namespace Tolltech.TollGrapher.GrapherWeb.Configuration
{
    public static class ContainerConfigurator
    {
        public static void ConfigureContainer(this IServiceCollection services)
        {
            IoCResolver.Resolve((serviceType, implementationType) => services.AddSingleton(serviceType, implementationType), "Tolltech");

            var settings = new Settings
            {
                Neo4jHost = "vm-billy-neo1",
                Neo4jPort = 7474,
                Neo4jPath = "db/data/",
                Neo4jLogin = "neo4j",
                Neo4jPass = "tc_123456"
            };

            services.AddSingleton(settings);
        }
    }
}
