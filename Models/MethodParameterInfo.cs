﻿namespace Tolltech.Models
{
    public class MethodParameterInfo
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string[] Modifiers { get; set; }
    }
}