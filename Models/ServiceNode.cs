﻿using System;
using Tolltech.Neo4jData.Schema;

namespace Tolltech.Models
{
    [Label("Service")]
    public class ServiceNode : INeo4jNode
    {
        public string ServiceName { get; set; }

        private Guid? id;

        public Guid Id
        {
            get { return id ?? (id = this.GenerateIdByProperty(x => x.ServiceName)).Value; }
            set { id = value; }
        }
    }
}