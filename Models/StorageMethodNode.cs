﻿using System;
using Tolltech.Neo4jData.Schema;

namespace Tolltech.Models
{
    [Label("Method")]
    public class StorageMethodNode : INeo4jNode
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Namespace { get; set; }
        public bool NetworkCall { get; set; }
        public bool EntryPoint { get; set; }
        public bool Async { get; set; }
        public string ClassName { get; set; }
        public string ShortName { get; set; }
        public string ServiceName { get; set; }
        public bool Error { get; set; }
        public bool IsProperty { get; set; }
        public string ReturnType { get; set; }

        [Obsolete("Dont use. This field is only for hack storage")]
        public string StoredParametersStr { get; set; }

        public StorageMethodNode SetParameters(MethodParameterInfo[] parameters)
        {
            StoredParametersStr = SerializerHelpers.SerializeParametersInfo(parameters);
            return this;
        }

        public MethodParameterInfo[] GetParameters()
        {
            return SerializerHelpers.DeserializeParametersInfo(StoredParametersStr);
        }

        public override string ToString()
        {
            return $"{Name}.{NetworkCall}";
        }
    }
}