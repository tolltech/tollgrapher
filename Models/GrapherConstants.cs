﻿namespace Tolltech.Models
{
    public static class GrapherConstants
    {
        public static readonly string ImplicitCall = "implicitCall";
        public static readonly string NetworkCall = "networkCall";
        public static readonly string DirectCall = "directCall";
        public static readonly string Use = "use";
        public static readonly string Contains = "contains";
        public static readonly string NetworkMethod = "NetworkMethod";
        public static readonly string LocalMethod = "LocalMethod";
        public static readonly string EntryPoint = "EntryPoint";
        public static readonly string Property = "Property";
        public static readonly string Async = "Async";

        public static readonly string TaskRunnerMethodName = "UnsafeRun";
    }
}