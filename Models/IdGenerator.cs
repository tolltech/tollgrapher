﻿using System;
using System.Text;

namespace Tolltech.Models
{
    public static class IdGenerator
    {
        private static readonly int guidLength = Guid.NewGuid().ToString("N").Length;

        public static string GetMethodHash(string methodName, MethodParameterInfo[] parameterInfos)
        {
            return $"{methodName}_{SerializerHelpers.SerializeParametersInfo(parameterInfos)}";
        }

        public static Guid GenerateIdByProperty<T>(this T obj, Func<T, string> getProperty)
        {
            return GetRandomIdByName(getProperty(obj));
        }

        private static Guid GetRandomIdByName(string rndBase)
        {
            var rand = new Random(rndBase.GetHashCode());
            //930c0185-90a2-4788-8397-1a753f00c381
            //e401069f-b662-4097-a7f5-3a73ae94c47d

            var sb = new StringBuilder();
            for (var i = 0; i < guidLength; ++i)
            {
                var digit = rand.Next(16);

                if (digit < 10)
                {
                    sb.Append(digit.ToString());
                }
                else
                {
                    sb.Append((char)('a' + digit % 10));
                }
            }

            return Guid.Parse(sb.ToString());
        }
    }
}