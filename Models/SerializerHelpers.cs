﻿using System;
using System.Linq;

namespace Tolltech.Models
{
    public class SerializerHelpers
    {
        public static string SerializeParametersInfo(MethodParameterInfo[] parameters)
        {
            return
                parameters == null
                    ? null
                    : $"{string.Join(";", parameters.Select(x => $"{x.Type},{x.Name},{string.Join("|", x.Modifiers)}"))}";
        }

        public static MethodParameterInfo[] DeserializeParametersInfo(string stored)
        {
            if (string.IsNullOrEmpty(stored))
            {
                return new MethodParameterInfo[0];
            }

            var storedParams = stored.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            return storedParams
                .Select(x =>
                {
                    var splits = x.Split(new[] { "," }, StringSplitOptions.None);

                    if (splits.Length != 3)
                    {
                        return null;
                    }

                    return new MethodParameterInfo
                    {
                        Type = splits[0],
                        Name = splits[1],
                        Modifiers = splits[2].Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries)
                    };
                })
                .Where(x => x != null)
                .ToArray();
        }
    }
}