﻿using System;
using Newtonsoft.Json;

namespace Tolltech.Models
{
    public class MethodNode
    {
        public string Name { get; set; }
        public string Namespace { get; set; }
        public bool NetworkCall { get; set; }
        public bool EntryPoint { get; set; }
        public bool Async { get; set; }
        public string ClassName { get; set; }
        public string ShortName { get; set; }
        public string ServiceName { get; set; }
        public bool Error { get; set; }
        public bool IsProperty { get; set; }
        public string ReturnType { get; set; }
        public string Hash => IdGenerator.GetMethodHash(Name, Parameters);
        public MethodParameterInfo[] Parameters { get; set; }

        [JsonIgnore]
        public SourceCodeInfo SourceCode { get; set; }


        public override string ToString()
        {
            return $"{Name}.{NetworkCall}";
        }

        private Guid? id;


        public Guid Id
        {
            get { return id ?? (id = this.GenerateIdByProperty(x => Hash)).Value; }
            set { id = value; }
        }
    }
}
