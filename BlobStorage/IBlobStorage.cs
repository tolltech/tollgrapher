﻿namespace Tolltech.BlobStorage
{
    public interface IBlobStorage
    {
        byte[] GetLastBlob(string namePrefix);
        byte[] GetBlob(string name);
        string WriteBlobAndReturnName(byte[] blob, string blobName);

        string WriteBlobJsonAndReturnName<T>(T blob, string blobName);
        T GetLastJsonBlob<T>(string namePrefix);
    }
}