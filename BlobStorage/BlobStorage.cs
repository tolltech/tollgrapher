using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using Kontur.Drive.Client;
using Kontur.Drive.ServiceModel.Requests;
using Kontur.Logging;
using Newtonsoft.Json;
using Tolltech.Common;
using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace Tolltech.BlobStorage
{
    public class BlobStorage : IBlobStorage
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(BlobStorage));

        private readonly Settings settings;

        private const string createDateFieldName = "sortableCreateDate";

        public BlobStorage(Settings settings)
        {
            this.settings = settings;
        }

        public byte[] GetLastBlob(string namePrefix)
        {
            var driveClient = new DriveClient(TokenAuthProvider.Create(settings.KonturDriveApiKey), new FakeLog());

            var prefix = new EntityNameBuilder()
                .SetServiceId("billinggrapher")
                .SetName(namePrefix)
                .GetName();


            var result = driveClient.Documents.FindByPrefix(prefix, 2, 0, createDateFieldName, SortDirection.Desc);

            if (!result.IsSuccessful)
            {
                throw new Exception(result.Error);
            }

            var name = result.Response.FirstOrDefault()?.Name;

            if (string.IsNullOrWhiteSpace(name))
            {
                return null;
            }

            return GetBlobByName(name, driveClient);
        }

        public byte[] GetBlob(string name)
        {
            var driveClient = new DriveClient(TokenAuthProvider.Create(settings.KonturDriveApiKey), new FakeLog());

            return GetBlobByName(name, driveClient);
        }

        private static byte[] GetBlobByName(string name, DriveClient driveClient)
        {
            var contentDownloadResult = driveClient.Contents.Get(name);
            if (!contentDownloadResult.IsSuccessful)
            {
                throw new Exception(contentDownloadResult.Error);
            }

            return contentDownloadResult.Response;
        }

        public string WriteBlobAndReturnName(byte[] blob, string blobName)
        {
            return WriteBlobAndReturnName(new MemoryStream(blob), blobName);
        }

        public string WriteBlobJsonAndReturnName<T>(T blob, string blobName)
        {
            var fileId = $"{Guid.NewGuid()}.{blobName.Replace(@"\", "_").Replace(@"/", "_")}.treeNode.json";
            using (var fileWriteStream = new FileStream(fileId, FileMode.Create))
            using (var sw = new StreamWriter(fileWriteStream))
            using (var jsonTextWriter = new JsonTextWriter(sw))
            {
                var serializer = new JsonSerializer();
                serializer.Serialize(jsonTextWriter, blob);
            }

            var gzFileId = $"{fileId}.gz";

            Compress(fileId, gzFileId);

            var msg = $"Blob size of {blobName} { new FileInfo(fileId).Length / 1024 / 1024 } Mb Compresses size { new FileInfo(gzFileId).Length / 1024 / 1024} Mb";
            log.InfoFormat(msg);
            Console.WriteLine(msg);

            string result;
            using (var fileStream = new FileStream(gzFileId, FileMode.Open))
            {
                result = WriteBlobAndReturnName(fileStream, blobName);
            }

            File.Delete(fileId);
            File.Delete(gzFileId);

            return result;
        }

        public T GetLastJsonBlob<T>(string namePrefix)
        {
            var fileId = $"{Guid.NewGuid()}.treeNode.json";
            var gzFileId = $"{fileId}.gz";
            var blob = GetLastBlob(namePrefix);

            File.WriteAllBytes(gzFileId, blob);

            Decompress(gzFileId, fileId);

            T result;

            using (var fileWriteStream = new FileStream(fileId, FileMode.Open))
            using (var sr = new StreamReader(fileWriteStream))
            using (var jsonTextReader = new JsonTextReader(sr))
            {
                var serializer = new JsonSerializer();

                result = serializer.Deserialize<T>(jsonTextReader);
            }

            File.Delete(fileId);
            File.Delete(gzFileId);

            return result;
        }

        private string WriteBlobAndReturnName(Stream blobStream, string blobName)
        {
            var driveClient = new DriveClient(TokenAuthProvider.Create(settings.KonturDriveApiKey), new FakeLog());

            var now = DateTime.UtcNow;
            var name = new EntityNameBuilder()
                .SetServiceId("billinggrapher")
                .SetName(blobName)
                .GetName();

            var result = driveClient.Documents.Create(name, new[] { new DocumentField(createDateFieldName, now.ToString("s")) }, daysToLive: settings.KonturDriveDayToLive);

            if (!result.IsSuccessful)
            {
                throw new Exception(result.Error);
            }

            var result2 = driveClient.Contents.Upload(new DriveContent(name, MimeType.Json, blobStream, settings.KonturDriveDayToLive));

            if (!result2.IsSuccessful)
            {
                throw new Exception(result2.Error);
            }

            return result2.Response.Name;
        }

        private static void Compress(string fileId, string gzFileId)
        {
            using (var originalFileStream = new FileStream(fileId, FileMode.Open))
            {
                using (var compressedFileStream = new FileStream(gzFileId, FileMode.Create))
                {
                    using (var compressionStream = new GZipStream(compressedFileStream, CompressionMode.Compress))
                    {
                        originalFileStream.CopyTo(compressionStream);
                    }
                }
            }
        }

        private static void Decompress(string gzFileId, string fileId)
        {
            using (var originalFileStream = new FileStream(gzFileId, FileMode.Open))
            {
                using (var decompressedFileStream = new FileStream(fileId, FileMode.Create))
                {
                    using (var decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
                    {
                        decompressionStream.CopyTo(decompressedFileStream);
                    }
                }
            }
        }
    }
}