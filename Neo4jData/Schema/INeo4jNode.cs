﻿using System;

namespace Tolltech.Neo4jData.Schema
{
    public interface INeo4jNode
    {
        Guid Id { get; set; }
    }
}