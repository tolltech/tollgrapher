﻿using System;

namespace Tolltech.Neo4jData.Schema
{
    //[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)] не работает создание уникального констрейнта
    public class LabelAttribute  : Attribute
    {
        public LabelAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}