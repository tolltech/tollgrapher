﻿using System;
using System.Collections.Generic;
using Tolltech.Common;
using Tolltech.Neo4jData.Schema;

namespace Tolltech.Neo4jData
{
    public interface IGrapherHandler
    {
        void CreateUniqueConstraints(Type nodeType, string property, string label = null);

        void Create<T>(T item, string[] labels = null) where T : INeo4jNode;
        void CreateRelated<T>(T parentItem, T childItem, string edgeLabel = "", string nodeLabel = null) where T : INeo4jNode;
        void CreateRelation<T>(Guid parentId, Guid childId, string edgeLabel = "") where T : INeo4jNode;
        void CreateRelation<TParent, TChild>(Guid parentId, Guid childId, string edgeLabel = "") where TParent : INeo4jNode where TChild: INeo4jNode;
        void CreateRelation<T>(Guid parentId, T childItem, string edgeLabel = "", string nodeLabel = null) where T : INeo4jNode;

        T Find<T>(Guid id, string label = null) where T : INeo4jNode;//todo: expressions generator
        void DeleteAll<T>() where T : INeo4jNode;
        void DeleteAll(string label);

        T[] SelectAll<T>(string label = null) where T : INeo4jNode;

        List<T>[] SelectPaths<T>(Guid rootId, Guid leafId, string[] allowedEdgeLabels = null) where T : INeo4jNode;

        ItemWithChildren<T> FindWithChilds<T>(Guid parentId, string edgeLabel = "") where T : INeo4jNode;
        ItemWithChildren<TParent, TChild> FindWithChilds<TParent, TChild>(Guid parentId, string edgeLabel = "") where TParent : INeo4jNode where TChild : INeo4jNode;

        TParent[] FindAllParents<TChild, TParent>(Guid startNodeId, string[] allowedEdgeLabels = null, string parentNodeLabel = null) where TChild : INeo4jNode where TParent : INeo4jNode;
    }
}