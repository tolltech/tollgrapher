﻿using System;
using System.Collections.Generic;
using System.Linq;
using Neo4jClient;
using Neo4jClient.Cypher;
using Tolltech.Common;
using Tolltech.Neo4jData.Schema;

namespace Tolltech.Neo4jData
{
    public class GrapherHandler : IGrapherHandler
    {
        private readonly Settings settings;

        public GrapherHandler(Settings settings)
        {
            this.settings = settings;
        }

        public void CreateUniqueConstraints(Type nodeType, string property, string label = null)
        {
            var labelsStr = GetSingleLabelString(nodeType, label);

            if (string.IsNullOrWhiteSpace(labelsStr))
            {
                return;
            }

            using (var graphClient = CreateGraphClient())
            {
                graphClient.Connect();

                graphClient.Cypher
                    .CreateUniqueConstraint($"c:{labelsStr}", $"c.{property}")
                    .ExecuteWithoutResults();
            }
        }

        public void Create<T>(T item, string[] labels = null) where T : INeo4jNode
        {
            var labelsStr = labels?.JoinLabelsForCreateToString() ?? GetSingleLabelString<T>();

            using (var graphClient = CreateGraphClient())
            {
                graphClient.Connect();

                graphClient.Cypher.Create($"(item:{labelsStr} {{newItem}})")
                    .WithParam("newItem", item)
                    .ExecuteWithoutResults();
            }
        }

        private static string GetSingleLabelString<T>(string customLabel = null) where T : INeo4jNode
        {
            return GetSingleLabelString(typeof(T), customLabel);
        }

        private static string GetSingleLabelString(Type nodeType, string customLabel = null)
        {
            return customLabel ?? nodeType.GetAttributeValues<LabelAttribute, string>(x => x.Name).SingleOrDefault();
        }

        public void CreateRelated<T>(T parentItem, T childItem, string edgeLabel = "", string nodeLabel = null) where T : INeo4jNode
        {
            var labelsStr = GetSingleLabelString<T>(nodeLabel);

            using (var graphClient = CreateGraphClient())
            {
                graphClient.Connect();

                graphClient.Cypher
                    .Create($"(parent:{labelsStr} {{parentItem}})-[:{edgeLabel}]->(child:{labelsStr} {{childItem}})")
                    .WithParam("parentItem", parentItem)
                    .WithParam("childItem", childItem)
                    .ExecuteWithoutResults();
            }
        }

        public void CreateRelation<T>(Guid parentId, Guid childId, string edgeLabel = "") where T : INeo4jNode
        {
            CreateRelation<T, T>(parentId, childId, edgeLabel);
        }

        public void CreateRelation<TParent, TChild>(Guid parentId, Guid childId, string edgeLabel = "") where TParent : INeo4jNode where TChild : INeo4jNode
        {
            var parentLabelsStr = GetSingleLabelString<TParent>();
            var childLabelsStr = GetSingleLabelString<TChild>();

            using (var graphClient = CreateGraphClient())
            {
                graphClient.Connect();

                graphClient.Cypher
                    .Match($"(parent:{parentLabelsStr})",$"(child:{childLabelsStr})")
                    .Where((TParent parent) => parent.Id == parentId)
                    .AndWhere((TChild child) => child.Id == childId)
                    .CreateUnique($"(parent)-[:{edgeLabel}]->(child)")
                    .ExecuteWithoutResults();
            }
        }

        public void CreateRelation<T>(Guid parentId, T childItem, string edgeLabel = "", string nodeLabel = null) where T : INeo4jNode
        {
            var labelsStr = GetSingleLabelString<T>(nodeLabel);

            using (var graphClient = CreateGraphClient())
            {
                graphClient.Connect();

                graphClient.Cypher
                    .Match($"(parent:{labelsStr})")
                    .Where((T parent) => parent.Id == parentId)
                    .Create($"(parent)-[:{edgeLabel}]->(child:{labelsStr} {{childItem}})")
                    .WithParam("childItem", childItem)
                    .ExecuteWithoutResults();
            }
        }

        public T Find<T>(Guid id, string label = null) where T : INeo4jNode
        {
            var labelsStr = GetSingleLabelString<T>(label);

            using (var graphClient = CreateGraphClient())
            {
                graphClient.Connect();

                var result = graphClient.Cypher
                    .Match($"(item:{labelsStr})")
                    .Where((T item) => item.Id == id)
                    .Return(item => item.As<T>())
                    .Results;

                return result.FirstOrDefault();
            }
        }

        public void DeleteAll<T>() where T : INeo4jNode
        {
            var labelsStr = GetSingleLabelString<T>(null);

            using (var graphClient = CreateGraphClient())
            {
                graphClient.Connect();

                graphClient.Cypher
                    .OptionalMatch($"(item:{labelsStr})")
                    .DetachDelete("item")
                    .ExecuteWithoutResults();
            }
        }

        public void DeleteAll(string label)
        {
            using (var graphClient = CreateGraphClient())
            {
                graphClient.Connect();

                graphClient.Cypher
                    .OptionalMatch($"(item:{label})")
                    .DetachDelete("item")
                    .ExecuteWithoutResults();
            }
        }

        public T[] SelectAll<T>(string label = null) where T : INeo4jNode
        {
            var labelsStr = GetSingleLabelString<T>(label);

            using (var graphClient = CreateGraphClient())
            {
                graphClient.Connect();

                var result = graphClient.Cypher
                    .Match($"(item:{labelsStr})")
                    .Return(item => item.As<T>())
                    .Results;

                return result.ToArray();
            }
        }

        public List<T>[] SelectPaths<T>(Guid rootId, Guid leafId, string[] allowedEdgeLabels = null) where T : INeo4jNode
        {
            var labelsStr = GetSingleLabelString<T>();
            var edgeLabelsStr = allowedEdgeLabels.JoinOrLabelsForEdgeToString();

            using (var graphClient = CreateGraphClient())
            {
                graphClient.Connect();

                var result = graphClient.Cypher
                    .Match($"p = (root:{labelsStr})-[:{edgeLabelsStr}*]->(leaf:{labelsStr})")
                    .Where((T leaf) => leaf.Id == leafId)
                    .AndWhere((T root) => root.Id == rootId)
                    .Return(() => Return.As<IEnumerable<T>>("nodes(p)"))
                    .Results;

                return result.Select(x => x.ToList()).ToArray();
            }
        }

        public ItemWithChildren<T> FindWithChilds<T>(Guid parentId, string edgeLabel = "") where T : INeo4jNode
        {
            var item = FindWithChilds<T, T>(parentId, edgeLabel);
            return new ItemWithChildren<T>
            {
                Parent = item.Parent,
                Children = item.Children
            };
        }

        public ItemWithChildren<TParent, TChild> FindWithChilds<TParent, TChild>(Guid parentId, string edgeLabel = "") where TParent : INeo4jNode where TChild : INeo4jNode
        {
            var parentLabelsStr = GetSingleLabelString<TParent>();
            var childLabelsStr = GetSingleLabelString<TChild>();

            using (var graphClient = CreateGraphClient())
            {
                graphClient.Connect();

                return graphClient.Cypher
                    .OptionalMatch($"(parent:{parentLabelsStr})-[{edgeLabel}]-(child:{childLabelsStr})")
                    .Where((TParent parent) => parent.Id == parentId)
                    .Return((parent, child) => new
                    {
                        Parent = parent.As<TParent>(),
                        Children = child.CollectAs<TChild>()
                    })
                    .Results
                    .Select(x => new ItemWithChildren<TParent, TChild>
                    {
                        Parent = x.Parent,
                        Children = x.Children.ToArray()
                    })
                    .FirstOrDefault();
            }
        }

        public TParent[] FindAllParents<TChild, TParent>(Guid startNodeId, string[] allowedEdgeLabels = null, string parentNodeLabel = null) where TChild : INeo4jNode where TParent : INeo4jNode
        {
            var childLabelStr = GetSingleLabelString<TChild>();
            var parentLabelString = GetSingleLabelString<TParent>(parentNodeLabel);
            var edgeLabelsStr = allowedEdgeLabels.JoinOrLabelsForEdgeToString();

            using (var graphClient = CreateGraphClient())
            {
                graphClient.Connect();

                var result = graphClient.Cypher
                    .Match($"(child:{childLabelStr})<-[:{edgeLabelsStr}*]-(parent:{parentLabelString})")
                    .Where((TChild child) => child.Id == startNodeId)
                    .Return(parent => parent.As<TParent>())
                    .Results;

                return result.ToArray();
            }
        }

        private GraphClient CreateGraphClient()
        {
            var uri = new Uri($"http://{settings.Neo4jHost}:{settings.Neo4jPort}/{settings.Neo4jPath}");
            return new GraphClient(uri, settings.Neo4jLogin, settings.Neo4jPass);
        }
    }
}
