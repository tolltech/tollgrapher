﻿using System;
using System.Collections.Generic;
using Tolltech.Models;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapherApi.Models;

namespace Tolltech.TollGrapherApi
{
    public interface ITollGrapherSearchClient
    {
        ClassModel[] GetClassNames(string classNameSearchString, int count);
        MethodModel[] GetMethodNames(string methodNameSearchString, int count);
        MethodModel[] GetMethodNames(string classNameSearchString, string methodNameSearchString, int count);
        MethodModel[] GetMethodNames(string namespaceName, string className, string methodNameSearchString, int count);

        MethodModel[] GetAllEntryPoints(Guid methodNodeId, int? count = null);
        SourceCodeInfo GetMethodSourceCode(Guid methodId);
        List<MethodModel>[] GetAllNetworkPaths(Guid rootId, Guid leafId);
        TreeNode<MethodModel> GetTreeOfAllNetworkPaths(Guid rootId, Guid leafId);
    }
}