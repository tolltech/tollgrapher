﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tolltech.TollGrapher.Common;

namespace Tolltech.TollGrapherApi.CrazyAlgorythms
{
    public static class PathTreeBuilder
    {
        public static TreeNode<TTarget> BuildTree<TSource, TTarget, TId>(TSource node, List<TSource>[] paths, Func<TSource, TTarget> convert, Func<TSource, TId> getId) where TId : struct
        {
            return InnerBuildTree(node, paths, convert, getId, new Dictionary<TId, TreeNode<TTarget>>());
        }

        private static TreeNode<TTarget> InnerBuildTree<TSource, TTarget, TId>(TSource node, List<TSource>[] paths, Func<TSource, TTarget> convert, Func<TSource, TId> getId, Dictionary<TId, TreeNode<TTarget>> cached) where TId : struct
        {
            if (cached.TryGetValue(getId(node), out var cachedNode))
            {
                return cachedNode;
            }

            var children = paths
                .Select(x => x
                            .SkipWhile(y => !getId(y).Equals(getId(node)))
                            .Skip(1)
                            .Take(1))
                .Where(x => x.Any())
                .Select(x => x.Single())
                .GroupBy(getId)
                .Select(x => x.First())
                .ToArray();

            var newNode = new TreeNode<TTarget>
            {
                Node = convert(node)
            };

            cached[getId(node)] = newNode;
            newNode.Children = children.Select(x => InnerBuildTree(x, paths, convert, getId, cached)).ToArray();

            return newNode;
        }
    }
}