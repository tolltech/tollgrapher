using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using SourceCodeData;
using Tolltech.Common;
using Tolltech.Models;
using Tolltech.Neo4jData;
using Tolltech.TollGrapher.Common;
using Tolltech.TollGrapherApi.CrazyAlgorythms;
using Tolltech.TollGrapherApi.DataScience;
using Tolltech.TollGrapherApi.Models;

namespace Tolltech.TollGrapherApi
{
    public class TollGrapherSearchClient : ITollGrapherSearchClient
    {
        private static Fading<ConcurrentBag<StorageMethodNode>> fuckingBackup = null;
        private readonly IGrapherHandler grapherHandler;
        private readonly ISourceCodeRepo sourceCodeRepo;

        private static readonly string[] simpleEdgeLabels = new[] { GrapherConstants.DirectCall, GrapherConstants.NetworkCall };

        public TollGrapherSearchClient(IGrapherHandler grapherHandler, ISourceCodeRepo sourceCodeRepo)
        {
            this.grapherHandler = grapherHandler;
            this.sourceCodeRepo = sourceCodeRepo;
        }

        //todo: ���������� �� ������
        private ConcurrentBag<StorageMethodNode> GetFuckingBackup()
        {
            return fuckingBackup ?? (fuckingBackup = new Fading<ConcurrentBag<StorageMethodNode>>(new ConcurrentBag<StorageMethodNode>(grapherHandler.SelectAll<StorageMethodNode>()), 60));
        }

        public ClassModel[] GetClassNames(string classNameSearchString, int count)
        {
            return GetFuckingBackup()
                .GroupBy(x => new { x.Namespace, x.ClassName })
                .Select(x => x.First())
                .Select(x => new { Method = x, Overlapped = x.ClassName.GetOverlapPercents(classNameSearchString) })
                .Where(x => x.Overlapped > 0)
                .OrderByDescending(x => x.Overlapped)
                .ThenBy(x => x.Method.ClassName)
                .Select(x => ConvertToClass(x.Method))
                .Take(count)
                .ToArray();
        }

        public MethodModel[] GetMethodNames(string methodNameSearchString, int count)
        {
            return GetFuckingBackup()
                .Select(x => new { Method = x, Overlapped = x.ShortName.GetOverlapPercents(methodNameSearchString) })
                .Where(x => x.Overlapped > 0)
                .OrderByDescending(x => x.Overlapped)
                .ThenBy(x => x.Method.ShortName)
                .Select(x => Convert(x.Method))
                .Take(count)
                .ToArray();
        }

        public MethodModel[] GetMethodNames(string classNameSearchString, string methodNameSearchString, int count)
        {
            var classMethods = GetClassNames(classNameSearchString, 100);

            if (classMethods.Length == 0)
            {
                return GetMethodNames(methodNameSearchString, count);
            }

            return GetFuckingBackup()
                .Where(x => classMethods.Any(y => y.ClassName == x.ClassName))
                .Select(x => new { Method = x, Overlapped = x.ShortName.GetOverlapPercents(methodNameSearchString) })
                .Where(x => x.Overlapped > 0)
                .OrderByDescending(x => x.Overlapped)
                .ThenBy(x => x.Method.ShortName)
                .Select(x => Convert(x.Method))
                .Take(count)
                .ToArray();
        }

        public MethodModel[] GetMethodNames(string namespaceName, string className, string methodNameSearchString, int count)
        {
            return GetFuckingBackup()
                .Where(x => x.ClassName == className && x.Namespace == namespaceName)
                .Select(x => new { Method = x, Overlapped = x.ShortName.GetOverlapPercents(methodNameSearchString) })
                .Where(x => x.Overlapped > 0)
                .OrderByDescending(x => x.Overlapped)
                .ThenBy(x => x.Method.ShortName)
                .Select(x => Convert(x.Method))
                .Take(count)
                .ToArray();
        }

        public MethodModel[] GetAllEntryPoints(Guid methodNodeId, int? count = null)
        {
            var result = grapherHandler.FindAllParents<StorageMethodNode, StorageMethodNode>(methodNodeId, simpleEdgeLabels, GrapherConstants.EntryPoint)
                .GroupBy(x => x.Id)
                .Select(x => x.First())
                .Select(Convert)
                .OrderBy(x => x.FullName);

            return (count.HasValue ? result.Take(count.Value) : result).ToArray();
        }

        public SourceCodeInfo GetMethodSourceCode(Guid methodId)
        {
            return sourceCodeRepo.GetSources(methodId);
        }

        public List<MethodModel>[] GetAllNetworkPaths(Guid rootId, Guid leafId)
        {
            var paths = grapherHandler.SelectPaths<StorageMethodNode>(rootId, leafId, simpleEdgeLabels);
            return paths
                .Select(x => x
                            .Where(y => y.Id == rootId || y.Id == leafId || y.NetworkCall)
                            .Select(Convert)
                            .ToList())
                .ToArray();
        }

        public TreeNode<MethodModel> GetTreeOfAllNetworkPaths(Guid rootId, Guid leafId)
        {
            var paths = grapherHandler.SelectPaths<StorageMethodNode>(rootId, leafId, simpleEdgeLabels);
            var root = paths.SelectMany(x => x).ToArray().FirstOrDefault(x => x.Id == rootId);

            if (root == null)
            {
                return null;
            }

            return PathTreeBuilder.BuildTree(root, paths, Convert, x => x.Id);
        }

        private static MethodModel Convert(StorageMethodNode method)
        {
            return new MethodModel
            {
                ClassName = method.ClassName,
                MethodName = method.ShortName,
                FullName = method.Name,
                ServiceName = method.ServiceName,
                Id = method.Id,
                Namespace = method.Namespace,
                ReturnType = method.ReturnType,
                IsProperty = method.IsProperty,
                Parameters = method.GetParameters().Select(x => new ParameterModel
                {
                    Name = x.Name,
                    Type = x.Type,
                    Modifiers = x.Modifiers.Select(y => y).ToArray()
                }).ToArray()
            };
        }

        private static ClassModel ConvertToClass(StorageMethodNode method)
        {
            return new ClassModel
            {
                ClassName = method.ClassName,
                Namespace = method.Namespace,
                ServiceName = method.ServiceName
            };
        }
    }
}