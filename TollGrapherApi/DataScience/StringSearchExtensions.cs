﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tolltech.TollGrapherApi.DataScience
{
    public static class StringSearchExtensions
    {
        private const int Perfect = 100;
        private const int AlmostPerfect = 99;
        private const int Zero = 0;

        public static int GetOverlapPercents(this string src, string searchString)
        {
            searchString = searchString.ToLatinFromRussiaWithLove();

            if (src == searchString)
            {
                return Perfect;
            }

            if (string.Equals(src, searchString, StringComparison.CurrentCultureIgnoreCase))
            {
                return Perfect;
            }

            var searchStringNormalized = NormalizeCamelCase(searchString);
            var srcNormalized = NormalizeCamelCase(src);


            var successCount = 0;
            var allCount = searchStringNormalized.Count;
            while (srcNormalized.Count > 0 && searchStringNormalized.Count > 0)
            {
                var srcPart = srcNormalized[0];
                var searchStringPart = searchStringNormalized[0];

                if (srcPart.StartsWith(searchStringPart, StringComparison.CurrentCultureIgnoreCase))
                {
                    ++successCount;
                    searchStringNormalized.RemoveAt(0);
                }

                srcNormalized.RemoveAt(0);
            }

            return allCount == 0 || successCount < allCount
                ? Zero
                : allCount == successCount
                    ? AlmostPerfect
                    : successCount * 100 / allCount;
        }

        private static List<string> NormalizeCamelCase(string str)
        {
            if (str.Where(char.IsLetterOrDigit).All(x => !char.IsUpper(x)))
            {
                return str.Select(x => x.ToString()).ToList();
            }

            //todo: shall i do ACM here?
            var result = new List<string>();

            var sb = new StringBuilder();
            foreach (var chr in str)
            {
                if ((char.IsUpper(chr) || char.IsDigit(chr)) && sb.Length > 0)
                {
                    result.Add(sb.ToString());
                    sb.Clear();
                }

                sb.Append(chr);
            }

            if (sb.Length > 0)
            {
                result.Add(sb.ToString());
            }

            return result;
        }
    }
}