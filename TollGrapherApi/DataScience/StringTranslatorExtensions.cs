﻿using System.Collections.Generic;
using System.Linq;

namespace Tolltech.TollGrapherApi.DataScience
{
    public static class KeyboardRuEnConverter
    {
        private static readonly Dictionary<char, char> translateSymbols;

        static KeyboardRuEnConverter()
        {
            #region symbolTables

            var keyboardSymbols = new[]
                                      {
                                          new KeyValuePair<char, char>('а', 'f'),
                                          new KeyValuePair<char, char>('б', ','),
                                          new KeyValuePair<char, char>('в', 'd'),
                                          new KeyValuePair<char, char>('г', 'u'),
                                          new KeyValuePair<char, char>('д', 'l'),
                                          new KeyValuePair<char, char>('е', 't'),
                                          new KeyValuePair<char, char>('ё', '`'),
                                          new KeyValuePair<char, char>('ж', ';'),
                                          new KeyValuePair<char, char>('з', 'p'),
                                          new KeyValuePair<char, char>('и', 'b'),
                                          new KeyValuePair<char, char>('й', 'q'),
                                          new KeyValuePair<char, char>('к', 'r'),
                                          new KeyValuePair<char, char>('л', 'k'),
                                          new KeyValuePair<char, char>('м', 'v'),
                                          new KeyValuePair<char, char>('н', 'y'),
                                          new KeyValuePair<char, char>('о', 'j'),
                                          new KeyValuePair<char, char>('п', 'g'),
                                          new KeyValuePair<char, char>('р', 'h'),
                                          new KeyValuePair<char, char>('с', 'c'),
                                          new KeyValuePair<char, char>('т', 'n'),
                                          new KeyValuePair<char, char>('у', 'e'),
                                          new KeyValuePair<char, char>('ф', 'a'),
                                          new KeyValuePair<char, char>('х', '['),
                                          new KeyValuePair<char, char>('ц', 'w'),
                                          new KeyValuePair<char, char>('ч', 'x'),
                                          new KeyValuePair<char, char>('ш', 'i'),
                                          new KeyValuePair<char, char>('щ', 'o'),
                                          new KeyValuePair<char, char>('ы', 's'),
                                          new KeyValuePair<char, char>('ь', 'm'),
                                          new KeyValuePair<char, char>('ъ', ']'),
                                          new KeyValuePair<char, char>('э', '\''),
                                          new KeyValuePair<char, char>('ю', '.'),
                                          new KeyValuePair<char, char>('я', 'z'),

                                          new KeyValuePair<char, char>('А', 'F'),
                                          new KeyValuePair<char, char>('Б', '<'),
                                          new KeyValuePair<char, char>('В', 'D'),
                                          new KeyValuePair<char, char>('Г', 'U'),
                                          new KeyValuePair<char, char>('Д', 'L'),
                                          new KeyValuePair<char, char>('Е', 'T'),
                                          new KeyValuePair<char, char>('Ё', '~'),
                                          new KeyValuePair<char, char>('Ж', ':'),
                                          new KeyValuePair<char, char>('З', 'P'),
                                          new KeyValuePair<char, char>('И', 'B'),
                                          new KeyValuePair<char, char>('Й', 'Q'),
                                          new KeyValuePair<char, char>('К', 'R'),
                                          new KeyValuePair<char, char>('Л', 'K'),
                                          new KeyValuePair<char, char>('М', 'V'),
                                          new KeyValuePair<char, char>('Н', 'Y'),
                                          new KeyValuePair<char, char>('О', 'J'),
                                          new KeyValuePair<char, char>('П', 'G'),
                                          new KeyValuePair<char, char>('Р', 'H'),
                                          new KeyValuePair<char, char>('С', 'C'),
                                          new KeyValuePair<char, char>('Т', 'N'),
                                          new KeyValuePair<char, char>('У', 'E'),
                                          new KeyValuePair<char, char>('Ф', 'A'),
                                          new KeyValuePair<char, char>('Х', '{'),
                                          new KeyValuePair<char, char>('Ц', 'W'),
                                          new KeyValuePair<char, char>('Ч', 'X'),
                                          new KeyValuePair<char, char>('Ш', 'I'),
                                          new KeyValuePair<char, char>('Щ', 'O'),
                                          new KeyValuePair<char, char>('Ы', 'S'),
                                          new KeyValuePair<char, char>('Ь', 'M'),
                                          new KeyValuePair<char, char>('Ъ', '}'),
                                          new KeyValuePair<char, char>('Э', '"'),
                                          new KeyValuePair<char, char>('Ю', '>'),
                                          new KeyValuePair<char, char>('Я', 'Z')
                                      };

            #endregion

            translateSymbols = keyboardSymbols.ToDictionary(x => x.Key, x => x.Value);
        }

        public static string ToLatinFromRussiaWithLove(this string input)
        {
            return !string.IsNullOrEmpty(input)
                ? new string(input.Select(ch => translateSymbols.ContainsKey(ch) ? translateSymbols[ch] : ch).ToArray())
                : input;
        }
    }
}