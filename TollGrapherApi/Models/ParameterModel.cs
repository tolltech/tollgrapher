﻿namespace Tolltech.TollGrapherApi.Models
{
    public class ParameterModel
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string[] Modifiers { get; set; }
    }
}