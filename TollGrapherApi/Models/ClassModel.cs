﻿namespace Tolltech.TollGrapherApi.Models
{
    public class ClassModel
    {
        public string ClassName { get; set; }
        public string ServiceName { get; set; }
        public string Namespace { get; set; }
    }
}