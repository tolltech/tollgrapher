﻿using System;

namespace Tolltech.TollGrapherApi.Models
{
    public class MethodModel
    {
        public Guid Id { get; set; }
        public string ClassName { get; set; }
        public string MethodName { get; set; }
        public string FullName { get; set; }
        public string ServiceName { get; set; }
        public string Namespace { get; set; }
        public string ReturnType { get; set; }
        public ParameterModel[] Parameters { get; set; }
        public bool IsProperty { get; set; }
    }
}