﻿using Tolltech.TollGrapherTestCases.Infra;

namespace SKBKontur.Billy.Core
{
    public class Request
    {
        public static Request Post(string uri)
        {
            return null;
        }

        public static Request Get(string uri)
        {
            return null;
        }

        public static Request Put(string uri)
        {
            return null;
        }

        public static Request Head(string uri)
        {
            return null;
        }

        public static Request Options(string uri)
        {
            return null;
        }

        public static Request Patch(string uri)
        {
            return null;
        }

        public static Request Trace(string uri)
        {
            return null;
        }

        public Request WithContent<T>(T body, SerializationType serializationType)
        {
            return null;
        }

        public Request WithAdditionalQueryParameter<T>(string key, T value)
        {
            return null;
        }
    }
}