﻿using System;

namespace Tolltech.TollGrapherTestCases.Infra
{
    public static class IfNotNullExtension
    {
        public static T2 IfNotNull<T1, T2>(this T1 o, Func<T1, T2> func)
        {
            return o != null ? func(o) : default(T2);
        }

        public static void IfNotNull<T1>(this T1 o, Action<T1> func)
        {
            func(o);
        }

        public static T2 IfNotNull<T1, T2>(this T1 o, Func<T1, T2> func, T2 def)
        {
            return o != null ? func(o) : def;
        }
    }
}