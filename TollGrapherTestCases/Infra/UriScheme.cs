﻿namespace Tolltech.TollGrapherTestCases.Infra
{
    public enum UriScheme
    {
        http,
        https
    }
}