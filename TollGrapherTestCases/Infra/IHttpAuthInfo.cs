﻿using System.Net;

namespace Tolltech.TollGrapherTestCases.Infra
{
    public interface IHttpAuthInfo
    {
        void Apply(HttpWebRequest request);
    }
}