﻿using SKBKontur.Billy.Core;

namespace Tolltech.TollGrapherTestCases.Infra
{
    public interface IHttpClient
    {
        void Send(Request request);
    }

    public interface IHttpClientProvider
    {
        IHttpClient Get(string serviceName);
    }

    public interface IHttpClientFactory
    {
        IHttpClient Create(string serviceName);
    }
}