﻿using System;

namespace Tolltech.TollGrapherTestCases.Infra
{
    [AttributeUsage(AttributeTargets.Interface, Inherited = false, AllowMultiple = false)]
    public class NetworkServiceAttribute : Attribute
    {
        public NetworkServiceAttribute()
        {
        }
    }
}