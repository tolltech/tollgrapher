﻿namespace Tolltech.TollGrapherTestCases.Infra
{
    public interface IDomain
    {
        void RunOnRandomReplica(string serviceName, IDomainProxyCommand command, int maxReplicaUseCount = 3);

        void RunOnRandomReplicaAsync(string serviceName, IDomainProxyCommand command, int maxReplicaUseCount = 3);

        void RunOnStickReplica<TId>(string serviceName, IDomainProxyCommand command, TId id, int maxReplicaUseCount = 3, bool isHard = false) where TId : struct;

        void RunOnStickReplicaAsync<TId>(string serviceName, IDomainProxyCommand command, TId id, int maxReplicaUseCount = 3, bool isHard = false) where TId : struct;

        void RunOnStickReplica<TId>(string serviceName, IDomainProxyCommand command, TId id, int maxReplicaUseCount = 5) where TId : struct;

        void RunOnStickReplicaAsync<TId>(string serviceName, IDomainProxyCommand command, TId id, int maxReplicaUseCount = 5) where TId : struct;

        void RunOnSpecificReplica(string serviceName, IDomainProxyCommand command, string hostName, int maxReplicaUseCount = 3);

        void RunOnSpecificReplicaAsync(string serviceName, IDomainProxyCommand command, string hostName, int maxReplicaUseCount = 3);

        void RunOnFirstLuckyReplica(string serviceName, IDomainProxyCommand command, int maxReplicaUseCount = 3);

        void RunOnFirstLuckyReplicaAsync(string serviceName, IDomainProxyCommand command, int maxReplicaUseCount = 3);

        void RunOnCurrentOrRandomReplica(string serviceName, IDomainProxyCommand command, int maxReplicaUseCount = 3);

        void RunOnCurrentOrRandomReplicaAsync(string serviceName, IDomainProxyCommand command, int maxReplicaUseCount = 3);

        void RunOnAllAliveReplicas(string serviceName, IDomainProxyCommand command);

        void RunOnAllAliveReplicasAsync(string serviceName, IDomainProxyCommand command);
    }
}