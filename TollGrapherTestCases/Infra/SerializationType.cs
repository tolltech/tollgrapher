﻿namespace Tolltech.TollGrapherTestCases.Infra
{
    public enum SerializationType
    {
        Json = 0,

        Protobuf = 1,

        Grobuf = 2,

        Xml = 3
    }
}