﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace Tolltech.TollGrapherTestCases.Infra
{
    public interface IProxyCommandFactory
    {
        IDomainProxyCommand CreatePut(string path, byte[] body, NameValueCollection parameters = null, UriScheme scheme = UriScheme.http, SerializationType responseType = SerializationType.Json, IHttpAuthInfo authInfo = null, int timeout = 120000, IDictionary<string, string> headers = null);

        IDomainProxyCommand CreatePost(string path, byte[] body, NameValueCollection parameters = null, UriScheme scheme = UriScheme.http, SerializationType responseType = SerializationType.Json, IHttpAuthInfo authInfo = null, int timeout = 120000, IDictionary<string, string> headers = null);

        IDomainProxyCommand CreatePatch(string path, byte[] body, NameValueCollection parameters = null, UriScheme scheme = UriScheme.http, SerializationType responseType = SerializationType.Json, IHttpAuthInfo authInfo = null, int timeout = 120000, IDictionary<string, string> headers = null);

        IDomainProxyCommand CreateGet(string path, NameValueCollection parameters = null, UriScheme scheme = UriScheme.http, SerializationType responseType = SerializationType.Json, IHttpAuthInfo authInfo = null, int timeout = 120000, IDictionary<string, string> headers = null);

        IDomainProxyCommand CreateDelete(string path, NameValueCollection parameters = null, UriScheme scheme = UriScheme.http, SerializationType responseType = SerializationType.Json, IHttpAuthInfo authInfo = null, int timeout = 120000, Dictionary<string, string> headers = null);

        IProxyCommand CreatePut(Uri uri, byte[] body, NameValueCollection parameters = null, SerializationType responseType = SerializationType.Json, IHttpAuthInfo authInfo = null, int timeout = 120000, IDictionary<string, string> headers = null);

        IProxyCommand CreatePost(Uri uri, byte[] body, NameValueCollection parameters = null, SerializationType responseType = SerializationType.Json, IHttpAuthInfo authInfo = null, int timeout = 120000, IDictionary<string, string> headers = null);

        IProxyCommand CreateGet(Uri uri, NameValueCollection parameters = null, SerializationType responseType = SerializationType.Json, IHttpAuthInfo authInfo = null, int timeout = 120000, IDictionary<string, string> headers = null);
    }
}