using Tolltech.TollGrapherTestCases.Tests;
using Tolltech.TollGrapherTestCases.Tests.Networks;

namespace Tolltech.TollGrapherTestCases.Infra
{
    public class CaseController : Controller
    {
        private readonly IRpcDeepNetwork rpcDeepNetwork;
        private readonly ITwoLevels twoLevels;
        private readonly IDummy dummy;
        private readonly IRPCNetwork rpcNetwork;

        public CaseController(IRpcDeepNetwork rpcDeepNetwork, ITwoLevels twoLevels, IDummy dummy, IRPCNetwork rpcNetwork)
        {
            this.rpcDeepNetwork = rpcDeepNetwork;
            this.twoLevels = twoLevels;
            this.dummy = dummy;
            this.rpcNetwork = rpcNetwork;
        }

        public void CallDeep()
        {
            rpcDeepNetwork.CallNetworkQueue();
        }

        public void CallDeepMultiService()
        {
            twoLevels.CallWithNetwork();
        }

        public void CallWithProperty()
        {
            dummy.DummyCallWithAccessToPropertyWithDeepBody();
        }

        public void DummyCallParameters()
        {
            dummy.DummyCallParameters(42, 43, "sda");
        }

        public void PropertyCallWithOneNetwork()
        {
            rpcNetwork.CallProperty();
        }
    }
}