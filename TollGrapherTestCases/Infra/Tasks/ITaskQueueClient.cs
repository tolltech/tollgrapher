﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading.Tasks;
using Tolltech.TollGrapherTestCases.Tests;

namespace Tolltech.TollGrapherTestCases.Infra.Tasks
{
    public interface ITaskQueueClient
    {
        void Enqueue<T>(T[] tasks) where T : class, IPersistentTask;
        Task EnqueueAsync<T>(params T[] tasks) where T : class, IPersistentTask;
    }

    public interface ITaskRegistrator
    {
        void Register<T>(T task) where T : class, IPersistentTask;
        void Register<T>(T task, IQueryExecutor queryExecutor) where T : class, IPersistentTask;
    }

    public interface IPersistentTask
    {

    }

    public interface IEchelonTask : IPersistentTask
    {

    }

    public interface IQueryExecutor
    {

    }

    public interface ITaskRunner
    {
        bool Run(IPersistentTask task);
        bool IsLong { get; }
    }

    public class EchelonTask : IEchelonTask
    {

    }

    public class EchelonTasktRunner : TaskRunnerBase<EchelonTask>
    {
        private readonly IDummy _dummy;

        public EchelonTasktRunner(IDummy dummy)
        {
            _dummy = dummy;
            IsLong = true;
        }

        protected override void UnsafeRun(EchelonTask sendMailTask)
        {
            _dummy.DummyCall();
        }
    }

    public class SqlTask : IPersistentTask
    {

    }

    public class SqlTaskRunner : TaskRunnerBase<SqlTask>
    {
        private readonly IDummy _dummy;

        public SqlTaskRunner(IDummy dummy)
        {
            _dummy = dummy;
            IsLong = true;
        }

        protected override void UnsafeRun(SqlTask sendMailTask)
        {
            _dummy.DummyCall2();
        }
    }


    public abstract class TaskRunnerBase<T> : ITaskRunner where T : class, IPersistentTask
    {
        private static readonly ConcurrentDictionary<Guid, int> failsCount = new ConcurrentDictionary<Guid, int>();

        protected TaskRunnerBase()
        {
            IsLong = false;
        }

        public bool Run(IPersistentTask persistentTask)
        {
            var task = (T)persistentTask;
            var timer = new Stopwatch();
            var methodTime = 0L;
            try
            {

                timer.Start();
                UnsafeRun(task);
                methodTime = timer.ElapsedMilliseconds;
                OnSuccess(task);
            }
            catch (Exception ex)
            {
                methodTime = timer.ElapsedMilliseconds;
                OnFail(task, ex);
            }
            timer.Stop();


            return IsTaskHandled(task);
        }

        public bool IsLong { get; protected set; }

        private bool IsTaskHandled(T task)
        {
            return true;
        }

        protected abstract void UnsafeRun(T task);

        //todo: сделать сбор статистики
        protected virtual void OnSuccess(T task)
        {
        }

        protected virtual void OnFail(T task, Exception ex)
        {
        }
    }

}