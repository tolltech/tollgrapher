﻿using System;

namespace Tolltech.TollGrapherTestCases.Infra
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class HttpMethodAttribute : Attribute
    {
        public HttpMethodAttribute()
        {
        }
    }
}