﻿using Tolltech.TollGrapherTestCases.Infra;
using Tolltech.TollGrapherTestCases.Tests;
using Tolltech.TollGrapherTestCases.Tests.Networks;

namespace Network
{
    public class NetworkHttpHandler : HttpHandlerBase
    {
        private readonly IDummy dummy;
        private readonly IRPCNetwork rpcNetwork;

        public NetworkHttpHandler(IDummy dummy, IRPCNetwork rpcNetwork)
        {
            this.dummy = dummy;
            this.rpcNetwork = rpcNetwork;
        }

        [HttpMethod]
        public void Call()
        {
            dummy.DummyCall();
            dummy.DummyCall();
        }

        [HttpMethod]
        public void CallWithRpc()
        {
            rpcNetwork.Call();
        }
    }
}