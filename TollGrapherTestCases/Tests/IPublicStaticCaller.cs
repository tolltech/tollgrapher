﻿namespace Tolltech.TollGrapherTestCases.Tests
{
    public interface IPublicStaticCaller
    {
        void Call();
    }
}