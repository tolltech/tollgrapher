﻿using Tolltech.TollGrapherTestCases.Infra;
using Tolltech.TollGrapherTestCases.Tests.Networks;

namespace Tolltech.TollGrapherTestCases.Tests
{
    public class OneLevel : IOneLevel
    {
        private readonly IDummy dummy;
        private readonly INetworkClient networkClient;
        private IDummy dummyProperty { get; set; }

        public OneLevel(IDummy dummy, INetworkClient networkClient)
        {
            this.dummy = dummy;
            this.networkClient = networkClient;
        }

        public void Call()
        {
           var s = string.Format("213");
           dummy.DummyCall();
        }

        public void CallWithIfNotNull()
        {
            var s = string.Format("213");
            dummy.IfNotNull(x=>x.DummyCall());
        }

        public void CallProperty()
        {
            dummyProperty.DummyCall();
        }

        public void CallWithNetwork()
        {
            networkClient.CallWithRpc();
        }
    }
}