﻿namespace Tolltech.TollGrapherTestCases.Tests.OVerloads
{
    public interface IOverloader
    {
        void Call();
        void Call(int i, long? l);
        void Call(int i, int? i2);
        void Call(int i, long? l, params string[] s);
    }
}