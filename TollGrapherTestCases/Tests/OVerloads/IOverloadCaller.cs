﻿namespace Tolltech.TollGrapherTestCases.Tests.OVerloads
{
    public interface IOverloadCaller
    {
        void Call11();
        void Call12();
        void Call21();
        void Call22();
        void Call31();
        void Call32();
        void Call33();
    }
}