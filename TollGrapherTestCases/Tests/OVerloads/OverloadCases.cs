﻿namespace Tolltech.TollGrapherTestCases.Tests.OVerloads
{
    public class OverloadCases
    {
        public void CallInt(int i)
        {
        }

        public void CallInts(int[] i)
        {

        }

        public void CallNullableInts(int?[] i)
        {

        }

        public void CallNullableInt(int? i)
        {

        }

        public void CallLong(long i)
        {

        }

        public void CallIBase(IBase i)
        {

        }

        public void CallBase(Base i)
        {

        }

        public void CallChild(Child i)
        {

        }

        public void CallIBases(IBase[] bases)
        {

        }

        public void CallBases(Base[] bases)
        {

        }

        public void CallChilds(Child[] bases)
        {

        }
    }
}