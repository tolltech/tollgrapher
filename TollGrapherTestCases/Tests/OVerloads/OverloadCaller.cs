﻿namespace Tolltech.TollGrapherTestCases.Tests.OVerloads
{
    public class OverloadCaller : IOverloadCaller
    {
        private readonly IOverloader _overloader;

        public OverloadCaller(IOverloader overloader)
        {
            _overloader = overloader;
        }

        public void Call11()
        {
            var i = 42;
            _overloader.Call(i, (long?)null);
        }

        public void Call12()
        {
            var i = 42;
            _overloader.Call(i, 42L);
        }

        public void Call21()
        {
            _overloader.Call(42, (int?)42);
        }

        public void Call22()
        {
            _overloader.Call(42, (int?)null);
        }

        public void Call31()
        {
            _overloader.Call(42, 42L, "s");
        }

        public void Call32()
        {
            _overloader.Call(42, 42L, new string[] {});
        }

        public void Call33()
        {
            _overloader.Call(42, 42L, null);
        }
    }
}