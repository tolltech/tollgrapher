﻿using System;

namespace Tolltech.TollGrapherTestCases.Tests.OVerloads
{
    public class Overloader : IOverloader
    {
        private readonly IDummy _dummy;

        public Overloader(IDummy dummy)
        {
            _dummy = dummy;
        }

        public void Call()
        {
        }

        public void Call(int i, long? l)
        {
            _dummy.DummyCall();
        }

        public void Call(int i, int? i2)
        {
            _dummy.DummyCall();
            _dummy.DummyCall(1,1);
        }

        public void Call(int i, long? l, params string[] s)
        {
            _dummy.DummyCall();
            _dummy.DummyCall(1, 1);
            _dummy.DummyCall(1, 1, new DateTime(2010, 10, 10));
        }
    }
}