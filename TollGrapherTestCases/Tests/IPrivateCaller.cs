﻿namespace Tolltech.TollGrapherTestCases.Tests
{
    public interface IPrivateCaller
    {
        void Call();
    }
}