﻿using System;

namespace Tolltech.TollGrapherTestCases.Tests
{
    public interface IDummy
    {
        void DummyCall();
        void DummyCall(int i, long l, params string[] s);
        void DummyCall(int i, long l, DateTime dateTime, params string[] s);
        void DummyCallParameters(int i, long l, params string[] s);
        void DummyCall2();
        void DummyCallWithAccessToPropertyWithDeepBody();
    }
}