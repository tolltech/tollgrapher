﻿namespace Tolltech.TollGrapherTestCases.Tests.Networks
{
    public interface INetworkClient
    {
        void Call();
        void CallGateway();
        void CallField();
        void CallExpressionBody();
        void CallWithRpc();

        void CallClusterClientWithHttpCLientProvider();
        void CallClusterClientWithHttpClientFactory();
    }
}