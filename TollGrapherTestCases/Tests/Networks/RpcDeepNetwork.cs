namespace Tolltech.TollGrapherTestCases.Tests.Networks
{
    public class RpcDeepNetwork : IRpcDeepNetwork
    {
        private readonly IRPCNetwork rpcNetwork;
        private readonly IDummy dummy;

        public RpcDeepNetwork(IRPCNetwork rpcNetwork, IDummy dummy)
        {
            this.rpcNetwork = rpcNetwork;
            this.dummy = dummy;
        }

        public void CallNetworkQueue()
        {
            dummy.DummyCall();
            rpcNetwork.Call();
            dummy.DummyCall2();
        }
    }
}