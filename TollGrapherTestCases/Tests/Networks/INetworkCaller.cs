﻿namespace Tolltech.TollGrapherTestCases.Tests.Networks
{
    public interface INetworkCaller
    {
        void CallRpc();
        void CallNetwork();
        void CallGateway();
        void CallField();
        void CallExpressionBody();
    }
}