﻿using Tolltech.TollGrapherTestCases.Infra;

namespace Tolltech.TollGrapherTestCases.Tests.Networks
{
    [NetworkService]
    public interface IRPCNetwork
    {
        void Call();
        void CallProperty();
    }
}