﻿using System;
using System.Collections.Specialized;
using SKBKontur.Billy.Core;
using Tolltech.TollGrapherTestCases.Infra;

namespace Tolltech.TollGrapherTestCases.Tests.Networks
{
    public class NetworkClient : INetworkClient
    {
        private readonly IDomain domain;
        private readonly IProxyCommandFactory proxyCommandFactory;

        private readonly IHttpClient provideredServiceClient;
        private readonly IHttpClient factoredServiceClient;

        public NetworkClient(IDomain domain, IProxyCommandFactory proxyCommandFactory, IHttpClientProvider httpClientProvider, IHttpClientFactory httpClientFactory)
        {
            this.domain = domain;
            this.proxyCommandFactory = proxyCommandFactory;
            this.factoredServiceClient = httpClientFactory.Create("NetworkGateway");
            provideredServiceClient = httpClientProvider.Get(ConstServiceName);
        }

        public const string ConstServiceName = "Network";
        public string ServiceName { get { return "Network"; } }
        public string ServiceNameExpressionBody => "Network";
        private readonly string ServiceNameField = "Network";

        public void Call()
        {
            var command = proxyCommandFactory.CreateGet("Call", new NameValueCollection ());
            domain.RunOnRandomReplicaAsync(ServiceName, command);
        }

        public void CallClusterClientWithHttpCLientProvider()
        {
            var request = Request
                .Post("Call")
                .WithContent(new DateTime(2010,10,10), SerializationType.Grobuf)
                .WithAdditionalQueryParameter("key1", Guid.NewGuid());

            provideredServiceClient.Send(request);
        }

        public void CallClusterClientWithHttpClientFactory()
        {
            var request = Request
                .Post("Call")
                .WithContent(new DateTime(2010, 10, 10), SerializationType.Grobuf)
                .WithAdditionalQueryParameter("key1", Guid.NewGuid());

            factoredServiceClient.Send(request);
        }

        public void CallExpressionBody()
        {
            var command = proxyCommandFactory.CreateGet("Call", new NameValueCollection ());
            domain.RunOnRandomReplicaAsync(ServiceNameExpressionBody, command);
        }

        public void CallWithRpc()
        {
            var command = proxyCommandFactory.CreateGet("CallWithRpc", new NameValueCollection());
            domain.RunOnRandomReplicaAsync(ServiceName, command);
        }

        public void CallField()
        {
            var command = proxyCommandFactory.CreateGet("Call", new NameValueCollection());
            domain.RunOnRandomReplicaAsync(ServiceNameField, command);
        }

        public void CallGateway()
        {
            var command = proxyCommandFactory.CreateGet("Call", new NameValueCollection ());
            domain.RunOnRandomReplicaAsync("NetworkGateway", command);
        }
    }
}