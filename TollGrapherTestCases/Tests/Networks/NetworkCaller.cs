﻿namespace Tolltech.TollGrapherTestCases.Tests.Networks
{
    public class NetworkCaller : INetworkCaller
    {
        private readonly IRPCNetwork rpcNetwork;
        private readonly INetworkClient networkClient;

        public NetworkCaller(IRPCNetwork rpcNetwork, INetworkClient networkClient)
        {
            this.rpcNetwork = rpcNetwork;
            this.networkClient = networkClient;
        }

        public void CallRpc()
        {
            rpcNetwork.Call();
        }

        public void CallNetwork()
        {
            networkClient.Call();
        }

        public void CallGateway()
        {
            networkClient.CallGateway();
        }

        public void CallField()
        {
            networkClient.CallField();
        }

        public void CallExpressionBody()
        {
            networkClient.CallExpressionBody();
        }

        public void CallClusterClientWithHttpClientFactory()
        {
            networkClient.CallClusterClientWithHttpClientFactory();
        }

        public void CallClusterClientWithHttpCLientProvider()
        {
            networkClient.CallClusterClientWithHttpCLientProvider();
        }
    }
}