﻿using Tolltech.TollGrapherTestCases.Infra;

namespace Tolltech.TollGrapherTestCases.Tests.Networks
{
    [NetworkService]
    public interface IRpcDeepNetwork
    {
        void CallNetworkQueue();
    }
}