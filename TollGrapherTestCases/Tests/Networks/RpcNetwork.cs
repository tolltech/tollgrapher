﻿using Tolltech.TollGrapherTestCases.Tests.Models;

namespace Tolltech.TollGrapherTestCases.Tests.Networks
{
    public class RpcNetwork : IRPCNetwork
    {
        private readonly IDummy dummy;

        public RpcNetwork(IDummy dummy)
        {
            this.dummy = dummy;
        }

        public void Call()
        {
            dummy.DummyCall(42, 42, new[] {"", "."});
        }


        public void CallProperty()
        {
            var e = new Entity();
            var s = e.PropertyWithBody;
        }
    }
}