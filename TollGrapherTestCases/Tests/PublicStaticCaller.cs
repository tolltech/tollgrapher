﻿namespace Tolltech.TollGrapherTestCases.Tests
{
    public class PublicStaticCaller : IPublicStaticCaller
    {
        public void Call()
        {
            StaticClass.Call();
        }
    }
}