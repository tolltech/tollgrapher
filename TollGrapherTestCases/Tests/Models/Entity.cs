﻿namespace Tolltech.TollGrapherTestCases.Tests.Models
{
    public class VeryBaseEntity
    {
        protected void ProtectedVeryBaseMethod()
        {

        }

        public void VeryBaseMethod()
        {

        }
    }

    public class Entity : BaseEntity
    {
        private readonly IDummy dummy;
        private Entity2 entity = new Entity2();

        public EntityEnum EntityEnum { get; set; }

        public Entity(IDummy dummy = null)
        {
            this.dummy = dummy;
        }

        public string Property { get; set; }

        public string PropertyWithBody {
            get
            {
                var s = "BlobBlob";

                dummy.DummyCall();

                return s;
            }
            set
            {
                var s = "dsd";
                s = value;
            }
        }

        public string PropertyWithBodyFromEntity
        {
            get
            {
                var s = "BlobBlob";

                dummy.DummyCall();

                return entity.Property;
            }
            set
            {
                var s = "dsd";
                s = value;
                entity.Property = value;
            }
        }
    }

    public class Entity2
    {
        public string Property { get; set; }
    }

    public enum EntityEnum
    {
        One,
        Two
    }
}