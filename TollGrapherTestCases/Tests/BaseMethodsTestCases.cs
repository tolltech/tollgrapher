﻿using Tolltech.TollGrapherTestCases.Tests.Models;

namespace Tolltech.TollGrapherTestCases.Tests
{
    public class BaseMethodsTestCases
    {
        public void VeryBaseMethod()
        {
            var e = new Entity();
            e.VeryBaseMethod();
        }

        public void BaseMethod()
        {
            var e = new Entity();
            e.BaseMethod();
        }

        public void BaseMethodWithProtectedCall()
        {
            var e = new Entity();
            e.BaseMethodWithProtectedCall();
        }
    }
}