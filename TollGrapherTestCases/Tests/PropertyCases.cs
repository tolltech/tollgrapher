﻿using System.Linq;
using Tolltech.TollGrapherTestCases.Tests.Models;

namespace Tolltech.TollGrapherTestCases.Tests
{
    public class PropertyCases
    {
        public void AnonymusTypeInLinq()
        {
            var entity = new Entity();
            var s = Enumerable.Range(0, 10).Select(x => new {Property2 = entity.Property});
            var d = s.Select(x => x.Property2);
        }

        public void AnonymusType()
        {
            var entity = new Entity();
            var s = new { Property2 = entity.Property };
            var d = s.Property2;
        }

        public void NullableCall()
        {
            var entity = new Entity();
            var s = entity?.Property;
        }

        public void CallSet()
        {
            var entity = new Entity();
            entity.Property = "prop2";
        }

        public void CallCtorSet()
        {
            var entity = new Entity { Property = "prop" };
        }

        public void CallCtorGetSet()
        {
            var enttity2 = new Entity2();

            var entity = new Entity
            {
                Property = enttity2.Property,
            };
        }

        public void NullableCallWithinLinq()
        {
            var entity = new Entity();
            var s = new[] {entity}.FirstOrDefault(x => x.Property == "p2")?.Property;
        }

        public void CallWithinLinq()
        {
            var entity = new Entity();
            var s = new[] {entity}.FirstOrDefault(x => x.Property == "p2");
        }

        public void CallSetWithinLinq()
        {
            var entity = new Entity();
            var s = new[] {entity}.Select(x => x.Property = "p2").ToArray();
        }
    }

    public class EnumCases
    {
        public void Enum()
        {
            var entity =  EntityEnum.Two;
        }
    }
}