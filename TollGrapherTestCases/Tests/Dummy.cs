﻿using System;
using Tolltech.TollGrapherTestCases.Tests.Models;

namespace Tolltech.TollGrapherTestCases.Tests
{
    public class Dummy : IDummy
    {
        public void DummyCall()
        {
        }

        public void DummyCall(int i, long l, DateTime dateTime, params string[] s)
        {

        }

        public void DummyCallParameters(int i, long l, params string[] s)
        {

        }

        public void DummyCall2()
        {

        }

        public void DummyCall(int i, long l)
        {
        }

        public void DummyCall(int i, int i2)
        {
        }

        public void DummyCall(int i, long l, params string[] s)
        {
        }

        public void DummyCallWithAccessToProperty()
        {
            var entity = new Entity();
            var s = entity.Property;
        }

        public void DummyCallWithAccessToPropertyWithBody()
        {
            var entity = new Entity();
            var s = entity.PropertyWithBody;
        }

        public void DummyCallWithAccessToPropertyWithDeepBody()
        {
            var entity = new Entity();
            var s = entity.PropertyWithBodyFromEntity;
        }

        public void DummyCallWithAccessToPropertyWithinCtorInvocation()
        {
            var entity = new Entity().Property;
        }

        public void DummyCallWithAccessToPropertyWithinInvocation()
        {
            var entity = GetEntity().Property;
        }

        private static Entity GetEntity()
        {
            return new Entity();
        }
    }
}