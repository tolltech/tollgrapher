﻿namespace Tolltech.TollGrapherTestCases.Tests
{
    public static class StaticClass
    {
        public static string StaticProperty { get; set; }
        public static string StaticField;

        public static void Call()
        {
            var dummy = new Dummy();
            dummy.DummyCall();
        }
    }
}