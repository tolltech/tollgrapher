﻿namespace Tolltech.TollGrapherTestCases.Tests
{
    public class NamespaceAccessingCases
    {
        public void AccessToMethodWithNamespace()
        {
            //System.Text.Encoding.UTF8.GetString(null);
            var s = Tolltech.TollGrapherTestCases.Tests.ClassWithStaticClass.AccessToClassWithNamespace.AccessToMethodWithNamespace();
        }

        public void AccessToStaticMethodWithNamespace()
        {
            //System.Text.Encoding.UTF8.GetString(null);
            var s = Tolltech.TollGrapherTestCases.Tests.ClassForStaticClass.AccessToStaticMethodWithNamespace();
        }

        public void AccessToClassWithNamespace()
        {
            //System.Web.HttpContext.Current
            var s = Tolltech.TollGrapherTestCases.Tests.ClassWithStaticClass.AccessToClassWithNamespace;
        }
    }

    public class ClassWithStaticClass
    {
        public static ClassForStaticClass AccessToClassWithNamespace { get; set; }
    }

    public class ClassForStaticClass
    {
        public string AccessToMethodWithNamespace()
        {
            return "23";
        }

        public static string AccessToStaticMethodWithNamespace()
        {
            return "23";
        }
    }
}