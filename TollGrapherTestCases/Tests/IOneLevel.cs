﻿namespace Tolltech.TollGrapherTestCases.Tests
{
    public interface IOneLevel
    {
        void Call();
        void CallProperty();
        void CallWithNetwork();
    }
}