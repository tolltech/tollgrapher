﻿using System.Linq;
using Tolltech.TollGrapherTestCases.Infra.Tasks;

namespace Tolltech.TollGrapherTestCases.Tests.Tasks
{
    public class TaskCases
    {
        private readonly ITaskRegistrator _taskRegistrator;
        private readonly ITaskQueueClient _taskQueueClient;

        public TaskCases(ITaskRegistrator taskRegistrator, ITaskQueueClient taskQueueClient)
        {
            _taskRegistrator = taskRegistrator;
            _taskQueueClient = taskQueueClient;
        }

        public void CallEchelonTask()
        {
            var task = new EchelonTask();
            _taskQueueClient.Enqueue(new[] {task});
        }


        public void CallEchelonTaskExplicitArray()
        {
            var task = new EchelonTask();
            var echelonTasks = new[] { task };
            _taskQueueClient.Enqueue(echelonTasks);
        }

        public void CallEchelonTaskExplicitArrayFromLinq()
        {
            var echelonTasks = Enumerable.Range(0, 1).Select(x => new EchelonTask()).ToArray();
            _taskQueueClient.Enqueue(echelonTasks);
        }

        public void CallEchelonTaskAsyncParams()
        {
            var task = new EchelonTask();
            _taskQueueClient.EnqueueAsync(task);
        }

        public void CallSqlTask()
        {
            var task = new SqlTask();
            _taskRegistrator.Register(task);
        }
    }
}