﻿namespace Tolltech.TollGrapherTestCases.Tests
{
    public class PrivateCaller : IPrivateCaller
    {
        private readonly IDummy _dummy;

        public PrivateCaller(IDummy dummy)
        {
            _dummy = dummy;
        }

        public void Call()
        {
            PrivateCall();
        }

        private void PrivateCall()
        {
            _dummy.DummyCall();
        }
    }
}