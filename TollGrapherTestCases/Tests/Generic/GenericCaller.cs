﻿using System;
namespace Tolltech.TollGrapherTestCases.Tests.Generic
{
    public class GenericCaller
    {
        private readonly GenericChildClass<Guid> genericChildClass;
        private readonly NonGenericChildClass nonGenericChildClass;
        private readonly GenericClass<DateTime> genericClass;

        public GenericCaller(GenericChildClass<Guid> genericChildClass, NonGenericChildClass nonGenericChildClass, GenericClass<DateTime> genericClass)
        {
            this.genericChildClass = genericChildClass;
            this.nonGenericChildClass = nonGenericChildClass;
            this.genericClass = genericClass;
        }

        public void CallGenericProperty()
        {
            var e = new GenericEntity<DateTime>();
            var s = e.GenericProperty;
        }

        public void CallGenericChildProperty()
        {
            var e = new GenericChildEntity<DateTime>();
            var s = e.GenericProperty;
        }

        public void CallNonGenericChildProperty()
        {
            var e = new NonGenericChildEntity();
            var s = e.GenericProperty;
        }

        public void CallGenericMethod()
        {
            genericClass.Call(new DateTime(2010, 10, 10));
        }

        public void CallGenericChildMethod()
        {
            genericChildClass.ChildCall(Guid.NewGuid());
        }

        public void CallGenericChildBaseMethod()
        {
            genericChildClass.Call(Guid.NewGuid());
        }

        public void CallNonGenericChildMethod()
        {
            nonGenericChildClass.Call(Guid.NewGuid());
        }
    }
}