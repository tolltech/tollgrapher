﻿namespace Tolltech.TollGrapherTestCases.Tests.Generic
{
    public class GenericEntity<T>
    {
        public T GenericProperty { get; set; }
        public string Property { get; set; }
    }
}