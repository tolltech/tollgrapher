﻿namespace Tolltech.TollGrapherTestCases.Tests
{
    public interface ITwoLevels
    {
        int Call();
        void CallWithNetwork();
    }
}