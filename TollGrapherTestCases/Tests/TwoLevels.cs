﻿namespace Tolltech.TollGrapherTestCases.Tests
{
    public class TwoLevels : ITwoLevels
    {
        private readonly IOneLevel oneLevel;
        private readonly IDummy dummy;

        public TwoLevels(IOneLevel oneLevel, IDummy dummy)
        {
            this.oneLevel = oneLevel;
            this.dummy = dummy;
        }

        public int Call()
        {
            dummy.DummyCall();
            oneLevel.Call();
            return 42;
        }

        public void CallWithNetwork()
        {
            oneLevel.CallWithNetwork();
        }
    }
}