﻿namespace Tolltech.TollGrapherTestCases.Tests.Composites
{
    public class Concrete2 : IConcrete
    {
        private readonly IDummy _dummy;

        public Concrete2(IDummy dummy)
        {
            _dummy = dummy;
        }

        public bool Call()
        {
            _dummy.DummyCall();
            _dummy.DummyCall();
            return true;
        }
    }
}