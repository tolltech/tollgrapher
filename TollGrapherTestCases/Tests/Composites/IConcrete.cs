﻿namespace Tolltech.TollGrapherTestCases.Tests.Composites
{
    public interface IConcrete
    {
        bool Call();
    }
}