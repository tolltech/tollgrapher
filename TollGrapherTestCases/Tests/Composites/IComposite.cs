﻿namespace Tolltech.TollGrapherTestCases.Tests.Composites
{
    public interface IComposite
    {
        void Call();
    }
}