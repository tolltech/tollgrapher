﻿namespace Tolltech.TollGrapherTestCases.Tests.Composites
{
    public class Concrete1 : IConcrete
    {
        private readonly IDummy _dummy;

        public Concrete1(IDummy dummy)
        {
            _dummy = dummy;
        }

        public bool Call()
        {
            _dummy.DummyCall();
            return true;
        }
    }
}