﻿using System.Linq;

namespace Tolltech.TollGrapherTestCases.Tests.Composites
{
    public class Composite : IComposite
    {
        private readonly IConcrete[] _concretes;

        public Composite(IConcrete[] concretes)
        {
            _concretes = concretes;
        }

        public void Call()
        {
            foreach (var concrete in _concretes)
            {
                concrete.Call();
            }
        }

        public void CallLambda()
        {
            var s = _concretes.Where(x => x.Call()).ToArray();
        }
    }
}