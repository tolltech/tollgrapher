﻿using Tolltech.TollGrapherTestCases.Infra;
using Tolltech.TollGrapherTestCases.Tests;

namespace NetworkGateway
{
    public class NetworkGatewayHttpHandler : HttpHandlerBase
    {
        private readonly IDummy dummy;

        public NetworkGatewayHttpHandler(IDummy dummy)
        {
            this.dummy = dummy;
        }


        [HttpMethod]
        public void Call()
        {
            dummy.DummyCall();
        }
    }
}